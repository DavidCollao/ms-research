#include<stdbool.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include"SUtil.h"
#include"Vector.h"
#include"IO_vtk.h"
#include"GRID_IO.h"
#include"amesh_obj.h"
#include"2DSpacing_Obj.h"
#include"ANALYTIC_FUNCTIONS.h"
#include"2DSPACING_FUNCTIONS.h"

#include"SUtil.h"

#define MIN( x, y ) ( ( x ) <= ( y ) ? ( x ) : ( y ) )
#define MAX( x, y ) ( ( x ) >= ( y ) ? ( x ) : ( y ) )


/* 2DSpacing */
int main( int argcs, char * pArgs[] )
{

  /* Testing if input is present */
  if( argcs < 2 ) {
    Print_Info();
    printf( "Usage: exe meshfile\n" );
    exit( EXIT_FAILURE );
  }

  /* Opening file and testing pointer */
  FILE * fp;
  Print_Info();
  printf( "Opening input mesh file.\n" );
  if( ( fp = fopen( pArgs[1], "r" ) ) == 0 ) {
    Print_Info();
    printf( "Could not open mesh file <%s>\n", pArgs[1] );
    exit( 0 );
  }

  /* Reading Data */
  Print_Info();
  printf( "Reading file...\n" );
  int nNodes, nBlocks, nTri;
  int nQuads, nBs, nConst, nVar;
  int * nSeg = NULL, *segType = NULL;
  int ** tri = NULL, ** quads = NULL, *** bs = NULL;
  double * x = NULL, * y = NULL, * var = NULL, * constant = NULL;
  Read_Generic_Meshfile_Var( fp, &nNodes, &nBlocks, &nTri, &nQuads,
                             &nBs, &nConst, &nVar, &nSeg, &segType,
                             &tri, &quads, &bs, &x, &y, &var, &constant );
  fclose( fp );
  free( segType );
  if( constant != NULL )  free( constant );
  Print_Done();

  /* Interchanging boundary segment pointers to order them consecutively */
  Print_Info();
  printf( "Reordering boundaries...\n" );
  Reorder_Boundaries( nBs, nSeg, bs );
  Print_Done();

  /* Finding root bounds */
  Print_Info();
  printf( "calculating geometry characteristics...\n" );
  int maxSpaceLevel;
  double ds, dsGlobal;
  double root_lo[2], root_hi[2];
  root_lo[0] = root_lo[1] =  1.0e20;
  root_hi[0] = root_hi[1] = -1.0e20;
  Get_Root_Bounds( nNodes, maxSpaceLevel, &ds, &dsGlobal,
                   root_lo, root_hi, x, y );
  Print_Done();

  /* Generating a node to node hash-table using Linked_List */
  Print_Info();
  printf( "Generating node to node connectivities...\n" );
  int i;
  Linked_List ** node_to_node = NULL;
  node_to_node = new Linked_List*[nNodes];
  for( i = 0; i < nNodes; i++ )
    node_to_node[i] = new Linked_List();
  Get_Node_To_Node( nTri, nQuads, tri, quads, node_to_node );
  Print_Done();

  /* Calculating analytic functions */
  double * f;
  Get_Analytical_Function( nVar, nNodes, ds, root_lo, root_hi, x, var, &f );
  if( var != NULL ) {
    free( var );
  }

  /* Storing solution data option */
  int j, storeSolution = 1;
  Print_Input();
  printf( "Select what information to store ( 0 -> Spacing Info, 1 -> Solution Data ): " );
  scanf( "%d", &storeSolution );
  Print_Info();
  if( storeSolution ) {
    printf( "Storing solution data.\n" );
    Store_Solution_Data( nNodes, nTri, nQuads, nBs, nSeg, tri,
                         quads, bs, x, y, f );

    /*** Freeing memory ***/
    for( i = 0; i < nTri; i++ )
      free( tri[i] );
    free( tri );
    for( i = 0; i < nQuads; i++ )
      free( quads[i] );
    free( quads );
    for( i = 0; i < nBs; i++ ) {
      for( j = 0; j < nSeg[i]; j++ )
        free( bs[i][j] );
      free( bs[i] );
    }
    free( bs );
    if( constant != NULL )  free( constant );
    for( i = 0; i < nNodes; i++ ) {
      delete node_to_node[i];
    }
    delete[] node_to_node;
    free( nSeg );
    free( x );
    free( y );
    free( f );

    return( 0 );
  }
  /* If not storing solution data, continue with program */
  printf( "Storing spacing info.\n" );

  /* Getting Rieman metrics and previous processes */
  Vector * gradient;
  double * avg_node_dist, ** riemann_t;
  Get_Metrics( nNodes, nTri, nQuads, tri, quads, x, y, f, dsGlobal,
               node_to_node, &avg_node_dist, &riemann_t, &gradient );
  for( i = 0; i < nNodes; i++ ) {
    delete node_to_node[i];
  }
  delete[] node_to_node;

  /* Getting options to be used when storing spacing info */
  int eBased, mode;
  Storing_Spacing_Options( &eBased, &mode, &dsGlobal );

  /* Storing tensors */
  if( eBased ) {
    /* Storing at cells */
    Element_Based_Storing( nNodes, nTri, nQuads, tri, quads, x,
                           y, riemann_t, mode, dsGlobal, f );
  }
  else {
    /* Storing at nodes */
    Node_Based_Storing( nNodes, nTri, nQuads, nBs, nSeg, tri, quads,
                        bs, x, y, avg_node_dist, riemann_t, mode,
                        dsGlobal, gradient );
  }

  /*** TESTING ONLY ***/
  free( avg_node_dist );
  delete[] gradient;
  /*** Freeing memory ***/
  for( i = 0; i < nNodes; i++ ) {
    free( riemann_t[i] );
  }
  free( riemann_t );
  free( x );
  free( y );
  for( i = 0; i < nTri; i++ ) {
    free( tri[i] );
  }
  free( tri );
  for( i = 0; i < nQuads; i++ ) {
    free( quads[i] );
  }
  free( quads );
  for( i = 0; i < nBs; i++ ) {
    for( j = 0; j < nSeg[i]; j++ ) {
      free( bs[i][j] );
    }
    free( bs[i] );
  }
  free( bs );
  free( nSeg );
  free( f );

  return( 0 );

}

  /* Testing new procedures */
//  double * desired, vector[2], tensor[2][2];
//  double mlmean, mlmin, mlmax, mlstd;
//  FILE * test;
//  desired = ( double* ) malloc ( numberTens * sizeof( double ) );
//  vector[0] = 1.0;
//  vector[1] = 0.0;
//  vector[0] = 0.0;
//  vector[1] = 1.0;
//  mlmin =  1.0e20;
//  mlmax = -1.0e20;
//  mlstd = mlmean = 0.0;
//  test = fopen( "test.out" , "w" );
//  for( i = 0; i < numberTens; i++ ) {
//    tensor[0][0] = a_t->space[i].rmt[0];
//    tensor[0][1] = a_t->space[i].rmt[1];
//    tensor[1][0] = a_t->space[i].rmt[1];
//    tensor[1][1] = a_t->space[i].rmt[2];
//    desired[i] = 1.0 / a_t->Metric_Length( vector, tensor );
//    fprintf( test, "%4d %24.14e\n", i, desired[i] );
//    mlmin = MIN( mlmin, desired[i] );
//    if( fabs ( mlmin - desired[i] ) < 1.0e-12 ) j = i;
//    mlmax = MAX( mlmax, desired[i] );
//    if( fabs ( mlmax - desired[i] ) < 1.0e-12 ) n = i;
//    mlmean += desired[i];
//  }
//  mlmean /= numberTens;
//  for( i = 0; i < numberTens; i++ ) {
//    mlstd += ( desired[i] - mlmean )*( desired[i] - mlmean );
//  }
//  mlstd = sqrt( mlstd / ( (double)numberTens - 1.0 ) );
//  fprintf( test, "mean    = %.14e\n", j, mlmean );
//  fprintf( test, "min(%d) = %.14e\n", j, mlmin );
//  fprintf( test, "max(%d) = %.14e\n", n, mlmax );
//  fprintf( test, "std     = %.14e\n", mlstd );
//  fclose( test );
//  free( desired );

