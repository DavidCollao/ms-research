#include<stdbool.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include<sys/time.h>
#include"Vector.h"
#include"SUtil.h"
#include"2DSpacing_Obj.h"
#include"GRID_IO.h"
#include"ANALYTIC_FUNCTIONS.h"
#include"IO_vtk.h"
#include"Spacing_Field.h"

#define MIN( x, y ) ( ( x ) <= ( y ) ? ( x ) : ( y ) )
#define MAX( x, y ) ( ( x ) >= ( y ) ? ( x ) : ( y ) )


void print_SF( ) {
  printf( "SPACING FIELD: " );
}

void print_SFDONE( ) {
  printf( "SPACING FIELD: DONE\n" );
}

int main( int argcs, char * pArgs[] )
{

  /* Variables for spacing handling spacing information */
  int n, nvrt, action = -1, type = 0;
  double p[2], pt1[2], pt2[2], scalar;
  double totalTime, deltaTime;
  double rmt[2][2], vert[100000][2], lam[2];
  double left[2][2], right[2][2], RT[2][2];
  double e1[2], e2[2], h1, h2;
  struct timeval startTime;
  struct timeval endTime;
//  Size_Obj * mysize;

  /* Variables for handling other operations */
  FILE * fp;
  int i, j, k;
  int nNodes, nBlocks, nTri, nQuads, nBs, nConst;
  int * nSeg = NULL, * segType = NULL, ** tri = NULL;
  int ** quads = NULL, *** bs = NULL;
  double * x = NULL, * y = NULL;

  /* Testing if input is present */
  if( argcs < 4 ) {
    print_SF();
    printf( "Usage: exe meshfile tensor-no-merged-file tensor-merged-file\n" );
    exit( EXIT_FAILURE );
  }

  /* Opening file and testing pointer */
  print_SF();
  printf( "Opening input mesh file <%s>.\n", pArgs[1] );
  if( ( fp = fopen( pArgs[1], "r" ) ) == NULL ) {
    print_SF();
    printf( "Could not open mesh file <%s>\n", pArgs[1] );
    exit( EXIT_FAILURE );
  }
  print_SFDONE();
  /* Reading Data */
  print_SF();
  printf( "Reading mesh file...\n" );
  Read_Generic_Meshfile( fp, &nNodes, &nBlocks, &nTri, &nQuads, &nBs,
                         &nSeg, &segType, &tri, &quads, &bs, &x, &y );
  fclose( fp );
  /* Freeing unnecessary data */
  if( nTri != 0 ) {
    for( i = 0; i < nTri; i++ ) {
      free( tri[i] );
    }
    free( tri );
  }
  if( nQuads != 0 ) {
    for( i = 0; i < nQuads; i++ ) {
      free( quads[i] );
    }
    free( quads );
  }
  for( i = 0; i < nBs; i++ ) {
    for( j = 0; j < nSeg[i]; j++ ) {
      free( bs[i][j] );
    }
    free( bs[i] );
  }
  free( bs );
  free( nSeg );
  free( segType );
  print_SF();
  printf( "Got points!\n" );
  print_SFDONE();

  /* Initializing quadtree ( Spacing field pointer is global! ) */
  print_SF();
  printf( "Importing spacing info from <%s> and storing in quadtree.\n",
          pArgs[2] );
  SF_Initialize( pArgs[2] );
  print_SFDONE();

  /* Testing brute search timing */
  SF_Brute_Search_Timing( nNodes, x, y );

  /* Testing search on unoptimized field */
  print_SF();
  printf( "Opening unoptimized search timing log file...\n" );
  if( ( fp = fopen( "unoptimized.time", "w" ) ) == NULL ) {
    printf( "Could not open <unoptimized.time> file! Exiting!\n" );
    exit( EXIT_FAILURE );
  }
  /* Starting search */
  print_SF();
  printf( "Starting unoptimized search test...\n" );
  totalTime = 0.0;
  /* Looping over unstructured mesh points */
  for( i = 0; i < nNodes; i++ ) {
    /* Getting coordinates */
    p[0] = x[i];
    p[1] = y[i];
    RT[0][0]  = 0.0;
    RT[0][1]  = 0.0;
    RT[1][0]  = 0.0;
    RT[1][1]  = 0.0;
    /* Getting start time, calling retrieve function, getting end time */
    deltaTime = 0.0;
    gettimeofday( &startTime, NULL );
    SF_Retrieve_Tensor( p, RT );
    gettimeofday( &endTime, NULL );
    /* If information is not valid, point was not found! */
    if( RT[0][0] < 1.0e-12 ) {
      print_SF();
      printf( "Test failed! Point %d( %.5e, %.5e ) not located.\n",
              i, p[0], p[1] );
      fprintf( fp, "Test failed! Point %d( %.5e, %.5e ) not located.\n",
              i, p[0], p[1] );
      fclose( fp );
      exit( EXIT_FAILURE );
    }
    /* Getting delta time, adding toward total time, keeping log */
    deltaTime = (double)( endTime.tv_sec - startTime.tv_sec );
    deltaTime += (double)( endTime.tv_usec - startTime.tv_usec ) / 1.0e+6;
    totalTime += deltaTime;
    fprintf( fp, "%4d %18.14lf %d %22.14e\n", i, deltaTime, 0, RT[0][0] );
  }
  /* Adding last time to log */
  print_SF();
  printf( "Unoptimized search timing finished\n" );
  fprintf( fp, "Total time: %18.14lf\n", totalTime );
  fclose( fp );
  print_SF();
  printf( "Total time: %18.14lf\n", totalTime );
  print_SFDONE();

  /* Closing program */
  print_SF();
  printf( "Freeing quadtree for file <%s>.\n", pArgs[2] );
  SF_Finalize();
  print_SFDONE();

  /* Initializing quadtree second file( Spacing field pointer is global! ) */
  print_SF();
  printf( "Importing spacing info from <%s> and storing in quadtree.\n",
          pArgs[3] );
  SF_Initialize( pArgs[3] );
  print_SFDONE();

  /* Testing search on unoptimized field */
  print_SF();
  printf( "Opening optimized search timing log file...\n" );
  if( ( fp = fopen( "optimized.time", "w" ) ) == NULL ) {
    printf( "Could not open <optimized.time> file! Exiting!\n" );
    exit( EXIT_FAILURE );
  }
  /* Starting search */
  print_SF();
  printf( "Starting optimized search test...\n" );
  totalTime = 0.0;
  /* Looping over unstructured mesh points */
  for( i = 0; i < nNodes; i++ ) {
    /* Getting coordinates */
    p[0] = x[i];
    p[1] = y[i];
    RT[0][0]  = 0.0;
    RT[0][1]  = 0.0;
    RT[1][0]  = 0.0;
    RT[1][1]  = 0.0;
    /* Getting start time, calling retrieve function, getting end time */
    deltaTime = 0.0;
    gettimeofday( &startTime, NULL );
    SF_Retrieve_Tensor( p, RT );
    gettimeofday( &endTime, NULL );
    /* If information is not valid, point was not found! */
    if( RT[0][0] < 1.0e-12 ) {
      print_SF();
      printf( "Test failed! Point %d( %.5e, %.5e ) not located.\n",
              i, p[0], p[1] );
      fprintf( fp, "Test failed! Point %d( %.5e, %.5e ) not located.\n",
              i, p[0], p[1] );
      fclose( fp );
      exit( EXIT_FAILURE );
    }
    /* Getting delta time, adding toward total time, keeping log */
    deltaTime = (double)( endTime.tv_sec - startTime.tv_sec );
    deltaTime += (double)( endTime.tv_usec - startTime.tv_usec ) / 1.0e+6;
    totalTime += deltaTime;
    fprintf( fp, "%4d %18.14lf %d %22.14e\n", i, deltaTime, 0, RT[0][0] );
  }
  /* Adding last time to log */
  print_SF();
  printf( "Optimized search timing finished\n" );
  fprintf( fp, "Total time: %18.14lf\n", totalTime );
  fclose( fp );
  print_SF();
  printf( "Total time: %18.14lf\n", totalTime );
  print_SFDONE();

  /* Closing program */
  print_SF();
  printf( "Freeing quadtree for file <%s>.\n", pArgs[3] );
  SF_Finalize();
  print_SFDONE();

  /* Freeing array of coordinates */
  free( x );
  free( y );

  return( 0 );

}

