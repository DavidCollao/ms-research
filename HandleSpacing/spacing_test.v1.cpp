#include<stdbool.h>
#include<string.h>
#include<stdlib.h>
#include<stdio.h>
#include"Vector.h"
#include"SUtil.h"
#include"2DSpacing_Obj.h"
#include"GRID_IO.h"
#include"ANALYTIC_FUNCTIONS.h"
#include"IO_vtk.h"
#include"Spacing_Field.h"

#define MIN( x, y ) ( ( x ) <= ( y ) ? ( x ) : ( y ) )
#define MAX( x, y ) ( ( x ) >= ( y ) ? ( x ) : ( y ) )

void print_SF( ) {
  printf( "SPACING FIELD: " );
}

void print_SFDONE( ) {
  printf( "SPACING FIELD: DONE\n" );
}

int main( int argcs, char * pArgs[] )
{

  int n, nvrt, action = -1, type = 0;
  double pt[2], pt1[2], pt2[2], scalar;
  double rmt[2][2], vert[4][2], lam[2];
  double left[2][2], right[2][2], RT[2][2];
  double e1[2], e2[2], h1, h2;

  /* Testing if input is present */
  if( argcs < 2 ) {
    print_SF();
    printf( "Usage: exe meshfile\n" );
    exit( EXIT_FAILURE );
  }

  /* Initializing quadtree ( Spacing field pointer is global! ) */
  print_SF();
  printf( "Importing spacing info from <%s> and storing in quadtree.\n",
          pArgs[1] );
  SF_Initialize( pArgs[1] );
  print_SFDONE();

  /* Presenting user with options from library */
  while( action != 0 ) {

    print_SF();
    printf( "Select action:\n" );
    printf( "\t(1)\tRetrieve number of entries.\n" );
    printf( "\t(2)\tRetrieve spacing at a node.\n" );
    printf( "\t(3)\tRetrieve edge size given two nodes.\n" );
    printf( "\t(4)\tRetrieve tensor at a node.\n" );
    printf( "\t(5)\tRetrieve a tensor given two nodes.\n" );
    printf( "\t(6)\tRetrieve a specific tensor by index.\n" );
    printf( "\t(7)\tCalculate edge metric length given two nodes.\n" );
    printf( "\t(8)\tCompute metric length given two nodes.\n" );
    printf( "\t(9)\tDecompose a given tensor.\n" );
    printf( "\t(10)\tCompute riemanian metric given tensor components.\n" );
    printf( "\t(0)\tExit.\n" );
    print_SF();
    printf( "Your entry: " );
    scanf( "%d", &action );

    switch( action ) {

      case 1:
        printf( "\tNumber of entries in spacing file: %d\n",
                SF_Number_of_Entries() );
        break;
      case 2:
        printf( "\tRetrieving spacing at node.\n" );
        printf( "\tEnter coordinates of node: " );
        scanf( "%lf %lf", &pt[0], &pt[1] );
        printf( "\tSpacing at node ( %4.2e, %4.2e ): %24.16e\n",
                pt[0], pt[1], SF_Retrieve_Size( pt ) );
        break;
      case 3:
        printf( "\tRetrieving edge size.\n" );
        printf( "\tEnter coordinates of first node: " );
        scanf( "%lf %lf", &pt1[0], &pt1[1] );
        printf( "\tEnter coordinates of second node: " );
        scanf( "%lf %lf", &pt2[0], &pt2[1] );
        printf( "\tEdge size from ( %4.2e, %4.2e ) to ( %4.2e, %4.2e ) :\n\t\t%24.16e\n",
                pt1[0], pt1[1], pt2[0], pt2[1],
                SF_Retrieve_Edge_Size( pt1, pt2 ) );
        break;
      case 4:
        printf( "\tRetrieving tensor at node.\n" );
        printf( "\tEnter coordinates of node: " );
        scanf( "%lf %lf", &pt[0], &pt[1] );
        rmt[0][0] = 0.0;
        rmt[0][1] = 0.0;
        rmt[1][0] = 0.0;
        rmt[1][1] = 0.0;
        SF_Retrieve_Tensor( pt, rmt );
        printf( "\tComponents of tensor at node( %4.2e, %4.2e ):\n\t\t%24.16e %24.16e %24.16e\n",
                pt[0], pt[1], rmt[0][0], rmt[0][1], rmt[1][1] );
        break;
      case 5:
        printf( "\tRetrieving edge tensor.\n" );
        printf( "\tEnter coordinates of first node: " );
        scanf( "%lf %lf", &pt1[0], &pt1[1] );
        printf( "\tEnter coordinates of second node: " );
        scanf( "%lf %lf", &pt2[0], &pt2[1] );
        rmt[0][0] = 0.0;
        rmt[0][1] = 0.0;
        rmt[1][0] = 0.0;
        rmt[1][1] = 0.0;
        SF_Retrieve_Edge_Tensor( pt1, pt2, rmt );
        printf( "\tEdge tensor from ( %4.2e, %4.2e ) to ( %4.2e, %4.2e ) :\n\t\t%24.16e %24.16e %24.16e\n",
                pt1[0], pt1[1], pt2[0], pt2[1],
                rmt[0][0], rmt[0][1], rmt[1][1] );
        break;
      case 6:
        printf( "\tRetrieving a specific tensor item.\n" );
        printf( "\tEnter tensor index: " );
        scanf( "%d", &n );
        nvrt = -1;
        rmt[0][0] = 0.0;
        rmt[0][1] = 0.0;
        rmt[1][0] = 0.0;
        rmt[1][1] = 0.0;
        vert[0][0] = vert[0][1] = 0.0;
        vert[1][0] = vert[1][1] = 0.0;
        vert[2][0] = vert[2][1] = 0.0;
        vert[3][0] = vert[3][1] = 0.0;
        SF_Retrieve_Tensor_Item( n-1, rmt, nvrt, vert );
        printf( "\tComponents for tensor %d.\n", n );
        printf( "\t\t%22.16e %24.16e %24.16e\n",
                rmt[0][0], rmt[0][1], rmt[1][1] );
        printf( "\t\tNumber of vertices: %d\n", nvrt );
        for( n = 0; n < nvrt; n++ ) {
          printf( "\t\t%7.3e %7.3e\n", vert[n][0], vert[n][1] );
        }
        break;
      case 7:
        printf( "\tComputing edge metric length.\n" );
        printf( "\tEnter edge type[ 0: max partial, 1: full ]:" );
        scanf( "%d", &type );
        printf( "\tEnter coordinates of first node: " );
        scanf( "%lf %lf", &pt1[0], &pt1[1] );
        printf( "\tEnter coordinates of second node: " );
        scanf( "%lf %lf", &pt2[0], &pt2[1] );
        rmt[0][0] = 0.0;
        rmt[0][1] = 0.0;
        rmt[1][0] = 0.0;
        rmt[1][1] = 0.0;
        scalar = SF_Edge_Metric_Length( pt1, pt2, type );
        printf( "\tEdge metric length from ( %4.2e, %4.2e ) to ( %4.2e, %4.2e ):\n",
                pt1[0], pt1[1], pt2[0], pt2[1] );
        printf( "\t\t%24.16e\n", scalar );
        break;
      case 8:
        printf( "\tComputing metric length.\n" );
        printf( "\tEnter coordinates of first node: " );
        scanf( "%lf %lf", &pt1[0], &pt1[1] );
        printf( "\tEnter coordinates of second node: " );
        scanf( "%lf %lf", &pt2[0], &pt2[1] );
        rmt[0][0] = 0.0;
        rmt[0][1] = 0.0;
        rmt[1][0] = 0.0;
        rmt[1][1] = 0.0;
        SF_Retrieve_Edge_Tensor( pt1, pt2, rmt );
        printf( "\tMetric length from ( %4.2e, %4.2e ) to ( %4.2e, %4.2e ) :\n\t\t%24.16e\n",
                pt1[0], pt1[1], pt2[0], pt2[1],
                SF_Compute_Metric_Length( pt1, pt2, rmt ) );
        break;
      case 9:
        printf( "\tDecomposing tensor.\n" );
        printf( "\tEnter three components of tensor: " );
        scanf( "%lf %lf %lf", &RT[0][0], &RT[0][1], &RT[1][1] );
        RT[1][0] = RT[0][1];
        left[0][0] = right[0][0] = 0.0;
        left[0][1] = right[0][1] = 0.0;
        left[1][0] = right[1][0] = 0.0;
        left[1][1] = right[1][1] = 0.0;
        lam[0] = lam[1] = 0.0;
        SF_Decompose_Tensor( RT, left, right, lam );
        printf( "\tFor tensor:\n" );
        printf( "\t\t%24.16e %24.16e\n", RT[0][0], RT[0][1] );
        printf( "\t\t%24.16e %24.16e\n", RT[1][0], RT[1][1] );
        printf( "\tDirection unit vectors:\n" );
        printf( "\t\t%24.16e %24.16e\n", left[0][0], left[1][0] );
        printf( "\t\t%24.16e %24.16e\n", left[0][1], left[1][1] );
        printf( "\tSpacing in each direction:\n" );
        printf( "\t\t%24.16e %24.16e\n",
                sqrt( 1.0 / lam[0] ), sqrt( 1.0 / lam[1] ) );
        break;
      case 10:
        printf( "\tComputing Riemannian metric.\n" );
        printf( "\tEnter components of one of direction vectors: " );
        scanf( "%lf %lf", &e1[0], &e1[1] );
        e2[0] =  e1[1];
        e2[1] = -e1[0];

        printf( "\tEnter desired spacing in each direction: " );
        scanf( "%lf %lf", &h1, &h2 );
        RT[0][0] = 0.0;
        RT[0][1] = 0.0;
        RT[1][0] = 0.0;
        RT[1][1] = 0.0;
        SF_Compute_Riemannian_Metric( e1, e2, h1, h2, RT );
        printf( "\tFor given information, tensor is:\n" );
        printf( "\t\t%24.16e %24.16e\n", RT[0][0], RT[0][1] );
        printf( "\t\t%24.16e %24.16e\n", RT[1][0], RT[1][1] );
        break;
      case 0:
        printf( "\tExiting.\n" );
        break;
      default:
        printf( "\tThat is not an option. Please, enter a valid option\n" );
        break;

    }

    print_SFDONE();

  }

  /* Closing program */
  print_SF();
  printf( "Freeing quadtree.\n" );
  SF_Finalize();
  print_SFDONE();

  return( 0 );

}
