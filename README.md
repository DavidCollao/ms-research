# README #

**This repository is not ready to get up and running.** The code needs to get reorganized, compiled, and tested.

The main purpose of this repository is to save and showcase code that I have developed while working on my master's research using the C and C++ programming languages. My master thesis topic was "Generation and Optimization of Spacing Fields."

### What is this repository for? ###

* Source code for 'create_spacing' program and 'spacing' library
* Version 1.0

### How do I get set up? ###

* Not ready to set up.

### Contribution guidelines ###

* Not ready to test.

### Who do I talk to? ###

* David Collao