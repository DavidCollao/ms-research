#include"2DSPACING_FUNCTIONS.h"
#include"IO_vtk.h"

/* Functions used for info output purposes */
void Print_Info( ) {
  printf( "INFO:\t" );
}
void Print_Done( ) {
  printf( "INFO:\tDone\n" );
}
void Print_Input( ) {
  printf( "INPUT:\t" );
}

/* Fuction to calculate root bounds and global spacing */
void Print_RB( ) {
  printf( "Root Bounds:\t" );
}
void Get_Root_Bounds( int nNodes,
                      int maxSpaceLevel,
                      double * ds,
                      double * dsGlobal,
                      double root_lo[],
                      double root_hi[],
                      double * x,
                      double * y ) {

  int i;
  for( i = 0; i < nNodes; i++ ) {
    root_lo[0] = MIN( root_lo[0], x[i] );
    root_lo[1] = MIN( root_lo[1], y[i] );
    root_hi[0] = MAX( root_hi[0], x[i] );
    root_hi[1] = MAX( root_hi[1], y[i] );
  }

  /* Making bound of geometry greater than lowest and highest points and
   * initializing the root, and initializing max level */
  *dsGlobal = ( root_hi[0] - root_lo[0] ) / 10.0;
  *dsGlobal = MAX( *dsGlobal, ( root_hi[1] - root_lo[1] ) / 10.0 );
  *ds = *dsGlobal * 1.0e-5;
  Print_RB();
  printf( "Geometry extension:\t( %.6f, %.6f ), ( %.6f, %.6f )\n",
          root_lo[0], root_lo[1], root_hi[0], root_hi[1] );
  root_lo[0] -= *ds;
  root_lo[1] -= *ds;
  root_hi[0] += *ds;
  root_hi[1] += *ds;
  maxSpaceLevel = 0;
  Print_RB();
  printf( "dsGlobal = %.6f\n", *dsGlobal );
  Print_RB();
  printf( "ds       = %.6f\n", *ds );
  Print_RB();
  printf( "Root extension:\t( %.6f, %.6f ), ( %.6f, %.6f )\n",
          root_lo[0], root_lo[1], root_hi[0], root_hi[1] );

}

/* Generate a node-to-node list */
void Get_Node_To_Node( int nTri,
                       int nQuads,
                       int ** tri,
                       int ** quads,
                       Linked_List ** n2n ) {

  int i, n0, n1, n2, n3;

  for( i = 0; i < nTri; i++ ) {
    n0 = tri[i][0];
    n1 = tri[i][1];
    n2 = tri[i][2];
    if( !n2n[n0]->In_list( n1 ) )  n2n[n0]->Insert( n1 );
    if( !n2n[n0]->In_list( n2 ) )  n2n[n0]->Insert( n2 );
    if( !n2n[n1]->In_list( n0 ) )  n2n[n1]->Insert( n0 );
    if( !n2n[n1]->In_list( n2 ) )  n2n[n1]->Insert( n2 );
    if( !n2n[n2]->In_list( n0 ) )  n2n[n2]->Insert( n0 );
    if( !n2n[n2]->In_list( n1 ) )  n2n[n2]->Insert( n1 );
  }
  for( i = 0; i < nQuads; i++ ) {
    n0 = quads[i][0];
    n1 = quads[i][1];
    n2 = quads[i][2];
    n3 = quads[i][3];
    if( !n2n[n0]->In_list( n1 ) )  n2n[n0]->Insert( n1 );
    if( !n2n[n0]->In_list( n3 ) )  n2n[n0]->Insert( n3 );
    if( !n2n[n1]->In_list( n0 ) )  n2n[n1]->Insert( n0 );
    if( !n2n[n1]->In_list( n2 ) )  n2n[n1]->Insert( n2 );
    if( !n2n[n2]->In_list( n1 ) )  n2n[n2]->Insert( n1 );
    if( !n2n[n2]->In_list( n3 ) )  n2n[n2]->Insert( n3 );
    if( !n2n[n3]->In_list( n0 ) )  n2n[n3]->Insert( n0 );
    if( !n2n[n3]->In_list( n2 ) )  n2n[n3]->Insert( n2 );
  }

}

/* Set analytical function */
void Analytical_Function( int nNodes,
                          double ds,
                          double root_lo[2],
                          double root_hi[2],
                          double * x,
                          double ** t_f ) {


  /* Variables */
  int i;
  double first, second, *f;

  /* Defining thirds */
  first  = root_lo[0] + ( root_hi[0] - root_lo[0] - 2.0 * ds ) / 3.0;
  second = root_lo[0] + ( root_hi[0] - root_lo[0] - 2.0 * ds ) * 2.0 / 3.0;

  /* Memory allocation and setting functions at nodes */
  f = ( double* ) malloc ( nNodes * sizeof( double ) );
  for( i = 0; i < nNodes; i++ ) {
    if( x[i] < first ) {
      f[i] = 0.0;
    }
    else if( x[i] > second) {
      f[i] = 1.0;
    }
    else {
      f[i] = ( x[i] - first ) / ( second - first );
    }
  }

  /* Interchanging pointers */
  *t_f = f;
  f = NULL;

}

/* Picking and obtaining analytical function */
void Get_Analytical_Function( int nVar,
                              int nNodes,
                              double ds,
                              double root_lo[],
                              double root_hi[],
                              double * x,
                              double * var,
                              double ** fOUT ) {

  /* Variables used here */
  int i;
  double * f;

  /* If no solution variables, use mesh */
  if( nVar == 0 ) {
    Print_Info();
    printf( "No variables in input meshfile!\n" );
    Print_Info();
    printf( "Setting an analytical function from previously defined routine...\n" );
    Analytical_Function( nNodes, ds, root_lo, root_hi, x, &f );
    Print_Done();
  }
  else { // If solution available, pick scalar function
    Print_Info();
    printf( "Calculating the scalar function...\n" );
    i = -1;
    while( i < 0 || i > 3 ) {
      Print_Input();
      printf( "Select solution data for scalar function:\n" );
      printf( "\t0 -> Velocity Magnitude\n" );
      printf( "\t1 -> Pressure\n" );
      printf( "\t2 -> Mach Number\n" );
      printf( "\t3 -> Density\n" );
      Print_Input();
      printf( "Your input: " );
      scanf( "%d", &i );
      if( i < 0 || i > 3 ) {
        Print_Info();
        printf( "That is not an option.\n" );
      }
    }
    /* Memory for scalar function */
    f = ( double* ) malloc ( nNodes * sizeof( double ) );
    if( i == 0 ) {
      Print_Info();
      printf( "The scalar function is velocity magnitude.\n" );
      Velocity_Magnitude( nNodes, var, f );
    }
    else if( i == 1 ) {
      Print_Info();
      printf( "The scalar function is pressure.\n" );
      Pressure( nNodes, var, f );
    }
    else if( i == 2 ) {
      Print_Info();
      printf( "The scalar function is mach number.\n" );
      Mach( nNodes, var, f );
    }
    else {
      Print_Info();
      printf( "The scalar function is density.\n" );
      Mach( nNodes, var, f );
    }
    Print_Done();
  }

  /* Switching pointers */
  *fOUT = f;

}

/* Function for storing solution data */
void Store_Solution_Data( int nNodes,
                          int nTri,
                          int nQuads,
                          int nBs,
                          int * nSeg,
                          int ** tri,
                          int ** quads,
                          int *** bs,
                          double * x,
                          double * y,
                          double * f ) {

  /* Getting grid in node tensor extent version */
  int i, j, n, nCents;
  int * n2c_count, ** node_to_cent;
  double * x_cent, * y_cent;
  double vert[100000][2];
  char fname[] = "test.tensor";

  printf( "Getting grid of centroidal duals...\n" );
  Get_Node_Version_Grid( nNodes, nTri, nQuads, nBs, &nCents, nSeg, tri,
                         quads, &n2c_count, &node_to_cent, bs, x, y,
                         &x_cent, &y_cent );
  Print_Done();

  /* Storing solution data in space array */
  Spacing_Obj * a_t;
  a_t = new Spacing_Obj();
  for( i = 0; i < nNodes; i++ ) {

    /* Building the new extents for node tensors */ //<- <- <- <- <- change vert fixed size
    for( j = 0; j < n2c_count[i]; j++ ) {
      n = node_to_cent[i][j];
      vert[j][0] = x_cent[n];
      vert[j][1] = y_cent[n];
    }

    /* Storing scalar */
    a_t->Store_Scalar( f[i], n2c_count[i], vert );

  }

  /* Plotting duals */
  Print_Info();
  printf( "Creating node based VTK file...\n" );
  Print_Info();
  printf( "Enter name for VTK file( something.vtu ): " );
  scanf( "%s", &fname );
  Print_Info();
  printf( "filename <%s>\n", fname );

  Write_VTK_2D_General( fname, nCents, x_cent, y_cent, nNodes,
                        n2c_count, node_to_cent, 0, NULL, 1, f );
  Print_Done();

  /* Exporting VTK file */
  Print_Info();
  printf( "Creating cell based VTK file...\n" );
  Print_Info();
  printf( "Enter name for VTK file( something.vtu ): " );
  scanf( "%s", &fname );
  Print_Info();
  printf( "filename <%s>\n", fname );
  free( n2c_count );
  n2c_count = ( int* ) malloc ( ( nTri + nQuads ) * sizeof( int ) );
  int ** node_to_cell;
  node_to_cell = ( int** ) malloc ( ( nTri + nQuads ) * sizeof( int* ) );
  for( i = 0; i < nTri; i++ ) {
    n2c_count[i] = 3;
    node_to_cell[i] = ( int* ) malloc ( 3 * sizeof( int ) );
    node_to_cell[i][0] = tri[i][0];
    node_to_cell[i][1] = tri[i][1];
    node_to_cell[i][2] = tri[i][2];
  }
  for( i = 0; i < nQuads; i++ ) {
    n2c_count[i+nTri] = 4;
    node_to_cell[i+nTri] = ( int* ) malloc ( 4 * sizeof( int ) );
    node_to_cell[i+nTri][0] = quads[i][0];
    node_to_cell[i+nTri][1] = quads[i][1];
    node_to_cell[i+nTri][2] = quads[i][2];
    node_to_cell[i+nTri][3] = quads[i][3];
  }
  Write_VTK_2D_General( fname, nNodes, x, y, nQuads+nTri, n2c_count,
                        node_to_cell, 1, f, 0, NULL );
  Print_Done();

  /* Exporting file */
  Print_Info();
  printf( "Creating tensor file...\n" );
  Print_Info();
  printf( "Enter name for tensor file( something.tensor ): " );
  scanf( "%s", &fname );
  Print_Info();
  printf( "filename <%s>\n", fname );
  a_t->Export_Spacing( fname );
  Print_Done();

  /*** Freeing memory ***/
  for( i = 0; i < nTri+nQuads; i++ )
    free( node_to_cell[i] );
  free( node_to_cell );
  for( i = 0; i < nNodes; i++ ) {
    free( node_to_cent[i] );
  }
  free( node_to_cent );
  free( n2c_count );
  free( x_cent );
  free( y_cent );
  delete a_t;

}

/* Calculate cell gradients using solution data */
void Get_Cell_Gradients( int nNodes,
                         int nTri,
                         int nQuads,
                         int invDistWeight,
                         int ** tri,
                         int ** quads,
                         double * x,
                         double * y,
                         double * f,
                         Vector * gradient ) {

  int i, n0, n1, n2, n3;
  double * weight, ds, area;
  Vector cell_grad;
  Point p0, p1, p2, p3, cg;

  /* Memory allocation and initializing arrays */
  weight = ( double* ) malloc ( nNodes * sizeof( double ) );
  for( i = 0; i < nNodes; i++ ) {
    weight[i]   = 0.0;
    gradient[i] = Vector( 0.0, 0.0 );
  }

  /* Getting cell gradients going through triangular cells */
  for( i = 0; i < nTri; i++ ) {
    n0 = tri[i][0];
    n1 = tri[i][1];
    n2 = tri[i][2];
    p0 = Point( x[n0], y[n0] );
    p1 = Point( x[n1], y[n1] );
    p2 = Point( x[n2], y[n2] );

    cell_grad = triangle_gradient( p0, p1, p2, f[n0], f[n1], f[n2] );

    if( invDistWeight ) {// Use inverse distance
      cg = ( p0 + p1 + p2 ) / 3.0;

      ds            = 1.0 / distance( p0, cg );
      gradient[n0] += cell_grad * ds;
      weight[n0]   += ds;

      ds            = 1.0 / distance( p1, cg );
      gradient[n1] += cell_grad * ds;
      weight[n1]   += ds;

      ds            = 1.0 / distance( p2, cg );
      gradient[n2] += cell_grad * ds;
      weight[n2]   += ds;
    }
    else {// Use area
      area = triangle_area( p0, p1, p2 );
      cell_grad *= area;

      gradient[n0] += cell_grad;
      weight[n0]   += area;

      gradient[n1] += cell_grad;
      weight[n1]   += area;

      gradient[n2] += cell_grad;
      weight[n2]   += area;
    }

  }

  /* Getting cell gradients going through quadrilateral cells */
  for( i = 0; i < nQuads; i++ ) {
    n0 = quads[i][0];
    n1 = quads[i][1];
    n2 = quads[i][2];
    n3 = quads[i][3];
    p0 = Point( x[n0], y[n0] );
    p1 = Point( x[n1], y[n1] );
    p2 = Point( x[n2], y[n2] );
    p3 = Point( x[n3], y[n3] );

    cell_grad = quadrilateral_gradient( p0, p1, p2, p3, f[n0], f[n1], f[n2], f[n3] );

    if( invDistWeight ) {// Use inverse distance
      cg = ( p0 + p1 + p2 + p3 ) / 4.0;

      ds            = 1.0 / distance( p0, cg );
      gradient[n0] += cell_grad * ds;
      weight[n0]   += ds;

      ds            = 1.0 / distance( p1, cg );
      gradient[n1] += cell_grad * ds;
      weight[n1]   += ds;

      ds            = 1.0 / distance( p2, cg );
      gradient[n2] += cell_grad * ds;
      weight[n2]   += ds;

      ds            = 1.0 / distance( p3, cg );
      gradient[n3] += cell_grad * ds;
      weight[n3]   += ds;
    }
    else {// Use area
      area = quadrilateral_area( p0, p1, p2, p3 );
      cell_grad *= area;

      gradient[n0] += cell_grad;
      weight[n0]   += area;

      gradient[n1] += cell_grad;
      weight[n1]   += area;

      gradient[n2] += cell_grad;
      weight[n2]   += area;

      gradient[n3] += cell_grad;
      weight[n3]   += area;
    }

  }

  /* Dividing gradients by weights */
  for( i = 0; i < nNodes; i++ ) {
    gradient[i] /= weight[i];
  }

  /*** Freeing memory ***/
  free( weight );

}

/* Calculating df statistics */
void Print_dfS( ) {
  printf( "df Stats:\t" );
}
void Get_df_Stats( int nNodes,
                   int * avg_node_count,
                   double power,
                   double * dfmin,
                   double * dfmax,
                   double * dfmean,
                   double * dfsd,
                   double * avg_node_dist,
                   double * x,
                   double * y,
                   Vector * gradient,
                   Linked_List ** n2n ) {

  int count, n, i;
  double mag, ff;
  Linked_Node * hd;
  Vector local;

  /* Calculating dfmin, dfmax, and dfmean; and initializing avg node distance
   * and count */
  count = 0;
  for( i = 0; i < nNodes; i++ ) {
    avg_node_dist[i] = 0.0;
    avg_node_count[i] = 0;
    hd = n2n[i]->head;// Get first node
    while( hd ) {// while point is not NULL
      n       = hd->data;// Get connected node index
      local   = Vector( x[n] - x[i], y[n] - y[i] );
      mag     = local.magnitude();
      local.normalize();
      /* Calculate gradient of function */
      ff      = fabs( gradient[i] * local ) * pow( mag, power );
      *dfmin   = MIN( *dfmin, ff );
      *dfmax   = MAX( *dfmax, ff );
      *dfmean += ff;
      count++;
      hd      = hd->next;// Get next point
    }
  }
  *dfmean /= MAX( 1, count );

  /* Calculate dfsd */
  for( i = 0; i < nNodes; i++ ) {
    hd = n2n[i]->head;// Get first node
    while( hd ) {// while next point is not NULL
      n     = hd->data;// Get next connected node index
      local = Vector( x[n] - x[i], y[n] - y[i] );
      mag   = local.magnitude();
      local.normalize();
      /* Calculate gradient of function minus mean */
      ff    = fabs( gradient[i] * local ) * pow( mag, power ) - *dfmean;
      *dfsd += ff*ff;
      hd    = hd->next;// Get next point
      /* Calculate average distance of nodes around node */
      avg_node_dist[i] += mag;
      avg_node_count[i]++;
    }
  }
  *dfsd = sqrt( *dfsd / MAX( 1, count ) );
  for( i = 0; i < nNodes; i++ ) {
    avg_node_dist[i] /= avg_node_count[i];
  }

  /* Printing df stats */
  Print_dfS();
  printf( "Printing function statistics:\n" );
  Print_dfS();
  printf( "dfmin    = %.15e\n",   *dfmin );
  Print_dfS();
  printf( "dfmax    = %.15e\n",   *dfmax );
  Print_dfS();
  printf( "dfmean   = %.15e\n",   *dfmean );
  Print_dfS();
  printf( "dfsd     = %.15e\n", *dfsd );

}

/* Calculating Riemann metrics */
void Get_Riemann_Metrics( int nNodes,
                          double power,
                          double limit,
                          double dsGlobal,
                          double * avg_node_dist,
                          double ** riemann_t,
                          Vector * gradient,
                          Spacing_Obj * a_t ) {

  int i;
  double ff, ds, h1, h2, h3;
  double e1[2], e2[2], e3[2];
  double rmt1[2][2], rmt2[2][2], rmt3[2][2];
  double right[2][2], left[2][2], lam[2];
  Vector v1, v2;
  FILE * myfp;
  double vert[2][2];

  for( i = 0; i < nNodes; i++ ) {

    /* Generating tensor #1 */
    /* Getting units of node gradient and its orthogonal */
    v1 = gradient[i];
    if( v1.magnitude() < 1.0e-10 ) {
      v1 = Vector( 1.0, 0.0 );
    }
    v1.normalize();
    v2 = Vector( v1[1], -v1[0] );
    /* Calculating h1 and h2 (?) */
    ff = fabs( v1 * gradient[i] );
    ds = pow( MAX( 0.0, limit ) / MAX( 1.0e-10, ff ), 1.0 / power );
    h1 = MIN( dsGlobal, ds );
    h2 = 2.0 * avg_node_dist[i];

//    /* Saving first tensor rmt1 */
//    if( fabs( h1 - h2 ) < 1.0e-12 ) {
//      /* Storing as uniform */
//      rmt1[0][0] = 1.0 / ( h1*h1 );
//      rmt1[0][1] = 0.0;
//      rmt1[1][0] = 0.0;
//      rmt1[1][1] = 1.0 / ( h2*h2 );
//    }
//    else {
//      e1[0] = v1[0];
//      e1[1] = v1[1];
//      e2[0] = v2[0];
//      e2[1] = v2[1];
//      a_t->Compute_Riemannian_Metric( e1, e2, e3, h1, h2, h3, rmt1 );
//    }

    /* Copying straight from result to tensor */
    e1[0] = v1[0];
    e1[1] = v1[1];
    e2[0] = v2[0];
    e2[1] = v2[1];
    a_t->Compute_Riemannian_Metric( e1, e2, e3, h1, h2, h3, rmt1 );
    riemann_t[i][0] = rmt1[0][0];
    riemann_t[i][1] = rmt1[0][1];
    riemann_t[i][2] = rmt1[1][1];

    /* Generating tensor #2 */
    /* Creating full tensors of existing tensor components */
//    if( fabs( riemann_t[i][1] ) < 1.0e-12 ) {
//      a_t->Scalar_To_Tensor( riemann_t[i][0], rmt2 );
//    }
//    else {
//      a_t->Full_Tensor( riemann_t[i], rmt2 );
//    }

    /* Comparing rmt1 and rmt2 to decide whether to store as scalar or tensor */
//    if( a_t->Compare_Tensors( rmt1, rmt2 ) > 0.05 ) {
//      a_t->Combine_Tensors( rmt1, rmt2, rmt3 );
//      a_t->Decompose_Tensor( rmt3, left, right, lam );//left, right, lambda
//      h1 = sqrt( 1.0 / lam[0] );
//      h2 = sqrt( 1.0 / lam[1] );
//      if( fabs( h1 - h2 ) < 1.0e-12 ) {
//        /* Store as scalar */
//        riemann_t[i][0] = h1;
//        riemann_t[i][1] = 0.0;
//        riemann_t[i][2] = 0.0;
//      }
//      else {
//        /* Store as 3 components of symmetric tensor */
//        riemann_t[i][0] = rmt3[0][0];
//        riemann_t[i][1] = rmt3[0][1];
//        riemann_t[i][2] = rmt3[1][1];
//      }
//
//    }
  }

}

/* Get Metrics */
void Get_Metrics( int nNodes,
                  int nTri,
                  int nQuads,
                  int ** tri,
                  int ** quads,
                  double * x,
                  double * y,
                  double * f,
                  double dsGlobal,
                  Linked_List ** node_to_node,
                  double ** avg_node_distOUT,
                  double *** riemann_tOUT,
                  Vector ** gradientTEMP ) {

  /* Setting weighting function */
  Print_Info();
  printf( "Calculating node gradients...\n" );
  int invDistWeight;
  //invDistWeight = 0;//0:Area - 1:Inverse distance
  Print_Input();
  printf( "Enter weighting function ( 1 -> Inverse Distance, 0 -> Area ): " );
  scanf( "%d", &invDistWeight );
  Print_Info();
  if( invDistWeight ) 
    printf( "Inverse distance weighting used.\n" );
  else
    printf( "Area weighting used.\n" );

  /* Getting cell gradients */
  Print_Info();
  printf( "Calculating cell gradients...\n" );
  Vector * gradient;
  gradient = new Vector[nNodes];
  Get_Cell_Gradients( nNodes, nTri, nQuads, invDistWeight, tri,
                      quads, x, y, f, gradient );
  Print_Done();

  /* Setting length exponent */
  Print_Info();
  printf( "Calculating statistics of function derivative...\n" );
  //power = 1.2;//Power > 1
  double power;
  Print_Input();
  printf( "Enter exponent of length ( power > 1 ): " );
  scanf( "%lf", &power );
  Print_Info();
  printf( "Power = %lf\n", power );

  /* Setting initial values of dfmin, dfmax, and dfmean; and getting df stats */
  double dfmin, dfmax, dfmean, dfsd;
  dfmin  =  1.0e20;
  dfmax  = -1.0e20;
  dfmean =  0.0;
  dfsd   =  0.0;
  int * avg_node_count;
  double * avg_node_dist;
  avg_node_dist  = ( double* ) calloc ( nNodes, sizeof( double ) );
  avg_node_count = ( int* ) calloc ( nNodes, sizeof( int ) );
  Get_df_Stats( nNodes, avg_node_count, power, &dfmin, &dfmax, &dfmean,
                &dfsd, avg_node_dist, x, y, gradient, node_to_node );
  Print_Done();

  /*** Freeing memory ***/
  free( avg_node_count );

  /* Calculating Riemann metrics */
  Print_Info();
  printf( "Calculating Riemannian metrics...\n" );
  double limit;
  //limit = 0.002;//Threshold
  Print_Input();
  printf( "Enter limit ( threshold ): " );
  scanf( "%lf", &limit );
  Print_Info();
  printf( "Limit: %lf\n", limit );

  /* Assigning memory for arrays and calculating Riemann metrics */
  int i;
  Spacing_Obj * a_t;
  a_t = new Spacing_Obj();
  double ** riemann_t;
  riemann_t = ( double** ) malloc ( nNodes * sizeof( double* ) );
  for( i = 0; i < nNodes; i++ )
    riemann_t[i] = ( double* ) malloc ( 3 * sizeof( double ) );
  Get_Riemann_Metrics( nNodes, power, limit, dsGlobal, avg_node_dist,
                       riemann_t, gradient, a_t );
  Print_Done();
  delete a_t;

  /* Interchanging pointers */
  *avg_node_distOUT = avg_node_dist;
  *riemann_tOUT = riemann_t;
  *gradientTEMP = gradient;

}

/* Boundary reordering */
void Reorder_Boundaries( int nBs,
                         int * nSeg,
                         int *** bs ) {

  int i, j, node, tNumberSeg;
  int ** bpointer;

  /* Interchanging boundary segment pointers to order them consecutively */
  for( i = 0; i < nBs-1; i++ ) {
    node = bs[i][nSeg[i]-1][0];
    bpointer = bs[i+1];
    tNumberSeg = nSeg[i+1];
    for( j = i+1; j < nBs; j++ ) {
      if( bs[j][0][1] == node ) {
        bs[i+1] = bs[j];
        nSeg[i+1] = nSeg[j];
        bs[j] = bpointer;
        nSeg[j] = tNumberSeg;
        break;
      }
    }
  }

}

/* Generation node version of grid */
/* WARNING: For proper functioning of this function the boundaries in the input
 * file (.mesh) must meet the following requirements.
 * 1.- The edges forming the domains in the mesh must have been created in
 *     clockwise order.
 * 2.- If the mesh is hybrid (tris+quads), and if different kinds of cells have
 *     edges in the mesh boundaries, such boundary segments must be declared
 *     separately in the input file. */
void Print_NVG( ) {
  printf( "Node Version Grid:\t" );
}
void Get_Node_Version_Grid( int nNodes,
                            int nTri,
                            int nQuads,
                            int nBs,
                            int * nExtents,
                            int * nSeg,
                            int ** tri,
                            int ** quads,
                            int ** t_n2c_count,
                            int *** t_node_to_cent,
                            int *** bs,
                            double * x,
                            double * y,
                            double ** t_x,
                            double ** t_y ) {

  int i, j, n, n0, n1, n2, n3, node, newNode;
  int cell, count, index, centArraySize;

  /* Getting count of node-to-cell for later memory allocation */
  int * n2c_count = NULL;
  n2c_count = ( int* ) calloc ( nNodes, sizeof( int ) );
  for( i = 0; i < nTri; i++ ) {
    n2c_count[tri[i][0]]++;
    n2c_count[tri[i][1]]++;
    n2c_count[tri[i][2]]++;
  }
  for( i = 0; i < nQuads; i++ ) {
    n2c_count[quads[i][0]]++;
    n2c_count[quads[i][1]]++;
    n2c_count[quads[i][2]]++;
    n2c_count[quads[i][3]]++;
  }

  /* Creating node to cell connectivity */
  int ** n2c = NULL;
  n2c = ( int** ) malloc ( nNodes * sizeof( int* ) );
  for( i = 0; i < nNodes; i++ ) {
    n2c[i] = ( int* ) malloc ( n2c_count[i] * sizeof( int ) );
    n2c_count[i] = 0;
  }
  for( i = 0; i < nTri; i++ ) {
    n0 = tri[i][0];
    n1 = tri[i][1];
    n2 = tri[i][2];
    n2c[n0][n2c_count[n0]] = i;
    n2c[n1][n2c_count[n1]] = i;
    n2c[n2][n2c_count[n2]] = i;
    n2c_count[n0]++;
    n2c_count[n1]++;
    n2c_count[n2]++;
  }
  for( i = 0; i < nQuads; i++ ) {
    n0 = quads[i][0];
    n1 = quads[i][1];
    n2 = quads[i][2];
    n3 = quads[i][3];
    n2c[n0][n2c_count[n0]] = i + nTri;
    n2c[n1][n2c_count[n1]] = i + nTri;
    n2c[n2][n2c_count[n2]] = i + nTri;
    n2c[n3][n2c_count[n3]] = i + nTri;
    n2c_count[n0]++;
    n2c_count[n1]++;
    n2c_count[n2]++;
    n2c_count[n3]++;
  }

  /* Calculating size of centroidals array */
  centArraySize = nTri + nQuads;
  for( i = 0; i < nBs; i++ ) {
    centArraySize += 2 * nSeg[i];
  }
  *nExtents = centArraySize;

  /* Creating centroidal nodes from inside elements and storing */
  double * x_cent = NULL;
  double * y_cent = NULL;
  x_cent = ( double* ) malloc ( centArraySize * sizeof( double ) );
  y_cent = ( double* ) malloc ( centArraySize * sizeof( double ) );
  for( i = 0; i < nTri; i++ ) {
    n0 = tri[i][0];
    n1 = tri[i][1];
    n2 = tri[i][2];
    x_cent[i] = ( x[n0] + x[n1] + x[n2] ) / 3.0;
    y_cent[i] = ( y[n0] + y[n1] + y[n2] ) / 3.0;
  }
  for( i = 0; i < nQuads; i++ ) {
    n0 = quads[i][0];
    n1 = quads[i][1];
    n2 = quads[i][2];
    n3 = quads[i][3];
    x_cent[i+nTri] = 0.25 * ( x[n0] + x[n1] + x[n2] + x[n3] );
    y_cent[i+nTri] = 0.25 * ( y[n0] + y[n1] + y[n2] + y[n3] );
  }

  /* Creating centroidal nodes from boundary edges, storing and mapping them */
  int *** bs_map = NULL;
  bs_map = ( int*** ) calloc ( nBs, sizeof( int** ) );
  count = nTri + nQuads;
  for( i = 0; i < nBs; i++ ) {
    bs_map[i] = ( int** ) calloc ( nSeg[i], sizeof( int* ) );
    for( j = 0; j < nSeg[i]; j++ ) {
      bs_map[i][j] = ( int* ) malloc ( 2 * sizeof( int ) );
      n0 = bs[i][j][0];
      n1 = bs[i][j][1];
      x_cent[count] = x[n0];
      y_cent[count] = y[n0];
      bs_map[i][j][0] = count;
      count++;
      x_cent[count] = 0.5 * ( x[n0] + x[n1] );
      y_cent[count] = 0.5 * ( y[n0] + y[n1] );
      bs_map[i][j][1] = count;
      count++;
    }
  }

  /* Checking process went well */
  if( count != centArraySize ) {
    Print_NVG();
    printf( "ERROR: Count of centroidal nodes does not match size of array!\n" );
    Print_NVG();
    printf( "count = %d, centArraySize = %d. Exiting!\n", count, centArraySize );
    exit( EXIT_FAILURE );
  }

  /* Tagging boundary nodes */
  bool * boundary = NULL;
  boundary = ( bool* ) calloc ( nNodes, sizeof( bool ) );
  for( i = 0; i < nBs; i++ ) {
    for( j = 0; j < nSeg[i]; j++ ) {
      boundary[bs[i][j][0]] = true;
    }
  }

  /* Going around nodes, n2c_count turns from node-to-cell count to
   * node-to-centroid count */
  int ** node_to_cent = NULL;
  node_to_cent = ( int** ) malloc ( nNodes * sizeof( int* ) );
  for( i = 0; i < nNodes; i++ ) {
    if( boundary[i] )  n2c_count[i] += 3;
    node_to_cent[i] = ( int* ) malloc ( n2c_count[i] * sizeof( int ) );
  }

  /* Going around nodes creating node to centroid connectivity */
  for( i = 0; i < nNodes; i++ ) {

    node = -1;

    /* Boundary nodes */
    if( boundary[i] ) {
      /* Going around boundaries */
      for( j = 0; j < nBs; j++ ) {
        /* Going around segments */
        for( n = 0; n < nSeg[j]; n++ ) {
          /* Checking boundary node position */
          if( bs[j][n][0] == i ) {
            /* Getting next node */
            node = bs[j][n][1];
            break;
          }
        }
        if( node != -1 )  break;
      }
      if( node == -1 )  {
        Print_NVG();
        printf( "ERROR: node = -1 in boundary. Node %d. Exiting!\n", i );
        exit( EXIT_FAILURE );
      }
      /* Inserting in list first three nodes: middle node from next edge,
       * node from current edge, and middle node from current edge */
      /* First, inserting second and third node in list */
      node_to_cent[i][1] = bs_map[j][n][0];
      node_to_cent[i][2] = bs_map[j][n][1];
      /* Increasing n by one to switch to "next edge" */
      n++;
      /* If n equal to number of edges in that boundary, increase j by one to
       * switch to "next boundary", and set n to zero to switcht to first
       * edge in "next boundary" */
      if( n == nSeg[j] ) {

        n = 0;
        j++;
        /* If j equal to number of boundaries, set j to zero to switch to
         * first boundary */
        if( j == nBs ) {
          j = 0;
        }

      }
      node_to_cent[i][0] = bs_map[j][n][1];
      /* Setting count */
      count = 3;
    }
    /* Inside nodes ( non-boundary ) */
    else {

      /* Picking first cell in node to cell conn */
      cell = n2c[i][0];

      if( cell < nTri ) {
        /* It's a triangle. Looking for node i, and saving next node */
        if( tri[cell][0] == i )  node = tri[cell][1];
        if( tri[cell][1] == i )  node = tri[cell][2];
        if( tri[cell][2] == i )  node = tri[cell][0];
        if( node == -1 )  {
          Print_NVG();
          printf( "ERROR: node = -1. Node %d, cell %d. Exiting!\n", i, cell );
          exit( EXIT_FAILURE );
        }
      }
      else {
        /* It's a quad. Looking for node i, and saving next node */
        if( quads[cell-nTri][0] == i )  node = quads[cell-nTri][1];
        if( quads[cell-nTri][1] == i )  node = quads[cell-nTri][2];
        if( quads[cell-nTri][2] == i )  node = quads[cell-nTri][3];
        if( quads[cell-nTri][3] == i )  node = quads[cell-nTri][0];
        if( node == -1 )  {
          Print_NVG();
          printf( "ERROR: node = -1. Node %d, cell %d. Exiting!\n", i, cell );
          exit( EXIT_FAILURE );
        }
      }
      /* Setting count */
      count = 0;
    }

    /* Creating node to centroids connectivity */
    while( count != n2c_count[i] ) {
      /* Looking for next node */
      newNode = -1;
      /* Going through cells around nodes */
      for( j = 0; j < n2c_count[i]; j++ ) {
        index = -1;
        cell = n2c[i][j];
        if( cell < nTri ) {
          /* It's a triangle, finding index of next node */
          if( tri[cell][0] == node )  index = 0;
          if( tri[cell][1] == node )  index = 1;
          if( tri[cell][2] == node )  index = 2;
          if( index == -1 )  continue;
          if( tri[cell][(index+2)%3] == i ){
            newNode = tri[cell][(index+1)%3];
            break;
          }
        }
        else {
          /* It's a quad, finding index of second next node */
          if( quads[cell-nTri][0] == node )  index = 0;
          if( quads[cell-nTri][1] == node )  index = 1;
          if( quads[cell-nTri][2] == node )  index = 2;
          if( quads[cell-nTri][3] == node )  index = 3;
          if( index == -1 )  continue;
          if( quads[cell-nTri][(index+3)%4] == i ){
            newNode = quads[cell-nTri][(index+2)%4];
            break;
          }
        }
      }
      if( newNode == -1 ) {
        Print_NVG();
        printf( "ERROR: newNode = -1.\n" );
        Print_NVG();
        printf( "Cell %d, Insert node %d, central node %d. Exiting!\n", cell, node, i );
        exit( EXIT_FAILURE );
      }
      /* Saving node for new round, inserting centroidal index
       * in list, and increasing count */
      node = newNode;
      //node_to_cent[i][count] = node;
      node_to_cent[i][count] = cell;
      count++;
    }

  }

  /*** Freeing memory ***/
  for( i = 0; i < nBs; i++ ) {
    for( j = 0; j < nSeg[i]; j++ ) {
      free( bs_map[i][j] );
    }
    free( bs_map[i] );
  }
  free( bs_map );
  free( boundary );
  for( i = 0; i < nNodes; i++ ) {
    free( n2c[i] );
  }
  free( n2c );

  /*** Interchanging pointers ***/
  *t_n2c_count = n2c_count;
  *t_node_to_cent = node_to_cent;
  *t_x = x_cent;
  *t_y = y_cent;

}

/* Getting simple squares grid */
void Get_Simple_Squares_Grid( int nNodes,
                              int * t_nNewNodes,
                              int ** t_n2c_count,
                              int *** t_node_to_cent,
                              double * avg_node_dist,
                              double * x,
                              double * y,
                              double ** t_x,
                              double ** t_y ) {

  /* Variables */
  int i, nNewNodes, * n2c_count, ** node_to_cent;
  double * new_x, * new_y, x_hi, x_lo, y_hi, y_lo;

  /* Gettint coung of new nodes and allocating memory */
  *t_nNewNodes = nNewNodes = 4 * nNodes;
  new_x = ( double* ) malloc ( nNewNodes * sizeof( double ) );
  new_y = ( double* ) malloc ( nNewNodes * sizeof( double ) );
  n2c_count    = ( int*  ) malloc ( nNodes * sizeof( int ) );
  node_to_cent = ( int** ) malloc ( nNodes * sizeof( int* ) );

  /* Generating new nodes and connectivities */
  for( i = 0; i < nNodes; i++ ) {

    x_hi = x[i] + avg_node_dist[i];
    y_hi = y[i] + avg_node_dist[i];
    x_lo = x[i] - avg_node_dist[i];
    y_lo = y[i] - avg_node_dist[i];

    new_x[4*i+0] = x_lo;
    new_x[4*i+1] = x_hi;
    new_x[4*i+2] = x_hi;
    new_x[4*i+3] = x_lo;

    new_y[4*i+0] = y_lo;
    new_y[4*i+1] = y_lo;
    new_y[4*i+2] = y_hi;
    new_y[4*i+3] = y_hi;

    n2c_count[i] = 4;
    node_to_cent[i] = ( int* ) malloc ( 4 * sizeof( int ) );
    node_to_cent[i][0] = 4*i+0;
    node_to_cent[i][1] = 4*i+1;
    node_to_cent[i][2] = 4*i+2;
    node_to_cent[i][3] = 4*i+3;

  }

  /* Interchanging pointers */
  *t_x = new_x;
  *t_y = new_y;
  *t_n2c_count = n2c_count;
  *t_node_to_cent = node_to_cent;

}

/* Getting storing options */
void Storing_Spacing_Options( int * eBasedOUT,
                              int * modeOUT,
                              double * dsGlobalOUT ) {

  /* Setting where to store tensors */
  Print_Info();
  printf( "Storing Tensors in file\n" );
  int eBased;
  //eBased = 0;//store at 0:Node - 1:Element
  Print_Input();
  printf( "Select where to store ( 0 -> at node, 1 -> at element ): " );
  scanf( "%d", &eBased );
  Print_Info();
  if( eBased )  printf( "Storing at elements.\n" );
  else  printf( "Storing at nodes.\n" );
  *eBasedOUT = eBased;

  /* Setting form of tensor to store */
  //mode = 2;//0:Scalar - 1:Tensor - 2:Mixed
  int mode;
  Print_Input();
  printf( "Select which form to store ( 0 -> in scalar, 1 -> tensor, 2 -> mixed ): " );
  scanf( "%d", &mode );
  Print_Info();
  if( mode == 0 )  printf( "Storing scalars.\n" );
  else if( mode == 1 )  printf( "Storing tensors.\n" );
  else  printf( "Storing mixed.\n" );
  *modeOUT = mode;

  /* Setting whether or not to use dsGlobal */
  int limiter;
  Print_Input();
  printf( "Use spacing limiting threshold? ( 0 -> NO, 1 -> YES ): " );
  scanf( "%d", &limiter );
  if( limiter ) {
    Print_Info();
    printf( "Using spacing limiting threshold: %14.e\n", *dsGlobalOUT );
  }
  else {
    *dsGlobalOUT = 1.0e15;
    Print_Info();
    printf( "Using no spacing limiting threshold.\n" );
  }

}

/* Storing spacing information at elements (cells) */
void Element_Based_Storing( int nNodes,
                            int nTri,
                            int nQuads,
                            int ** tri,
                            int ** quads,
                            double * x,
                            double * y,
                            double ** riemann_t,
                            int mode,
                            double dsGlobal,
                            double * f ) {

  /* Generating spacing object list */
  Spacing_Obj * a_t;
  a_t = new Spacing_Obj();
  bool * marked_n = NULL;// For marking nodes
  bool * marked_t = NULL;// For marking tensors
  int i, n0, n1, n2, n3;
  int nScal, nTen, count;
  double h1, h2, ds, vert[100000][2];
  double rmt1[2][2], rmt2[2][2], rmt3[2][2];
  double left[2][2], right[2][2], lam[2];
  marked_t = ( bool* ) calloc ( nTri + nQuads, sizeof( bool ) );
  marked_n = ( bool* ) calloc ( nNodes, sizeof( bool ) );
  count = nScal = nTen = 0;
  /* Over triangles */
  for( i = 0; i < nTri; i++ ) {
    /* Getting triangle node indices */
    n0 = tri[i][0];
    n1 = tri[i][1];
    n2 = tri[i][2];

    /* Retrieving information from spacing-object list for 1st node */
    if( fabs( riemann_t[n0][1] ) < 1.0e-12 && fabs( riemann_t[n0][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[n0][0], rmt1 );
    else
      a_t->Full_Tensor( riemann_t[n0], rmt1 );
    /* Retrieving information from spacing-object list for 2nd node */
    if( fabs( riemann_t[n1][1] ) < 1.0e-12 && fabs( riemann_t[n1][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[n1][0], rmt2 );
    else
      a_t->Full_Tensor( riemann_t[n1], rmt2 );
    /* Combining tensors from first two nodes */
    a_t->Combine_Tensors( rmt1, rmt2, rmt3 );
    /* Retrieving information from spacing-object list for 3rd node */
    if( fabs( riemann_t[n2][1] ) < 1.0e-12 && fabs( riemann_t[n2][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[n2][0], rmt1 );
    else
      a_t->Full_Tensor( riemann_t[n2], rmt1 );
    /* Combining tensor from third node with combination of the first two */
    a_t->Combine_Tensors( rmt1, rmt3, rmt2 );

    /* Decomposing combined tensor into eigenvalues and eigenvectors */
    a_t->Decompose_Tensor( rmt2, left, right, lam );

    /* Getting spacing distance in principal directions */
    h1 = sqrt( 1.0 / lam[0] );
    h2 = sqrt( 1.0 / lam[1] );

    if( h1 < dsGlobal || h2 < dsGlobal ) {
      /* STORE SCALARS or mixed if big h is bigger than small h by 50% or
       * more */
      if( mode == 0 || ( mode == 2 && MAX( h1, h2 ) / MIN( h1, h2 ) < 1.5 ) ) {
        ds = MIN( h1, h2 );
        if( ds < dsGlobal ) {
          vert[0][0] = x[n0];
          vert[0][1] = y[n0];
          vert[1][0] = x[n1];
          vert[1][1] = y[n1];
          vert[2][0] = x[n2];
          vert[2][1] = y[n2];
          a_t->Store_Scalar( ds, 3, vert );
          marked_t[i]  = true;// Since the process is element based, i is a tensor
          marked_n[n0] = true;
          marked_n[n1] = true;
          marked_n[n2] = true;
          count++;
          nScal++;
        }
      }
      else {
        vert[0][0] = x[n0];
        vert[0][1] = y[n0];
        vert[1][0] = x[n1];
        vert[1][1] = y[n1];
        vert[2][0] = x[n2];
        vert[2][1] = y[n2];
        a_t->Store_Tensor( rmt2, 3, vert );
        marked_t[i]  = true;// Since the process is element based, i is a tensor
        marked_n[n0] = true;
        marked_n[n1] = true;
        marked_n[n2] = true;
        count++;
        nTen++;
      }
    }
  }

  /* Over quads */
  for( i = 0; i < nQuads; i++ ) {
    /* Getting triangle node indices */
    n0 = quads[i][0];
    n1 = quads[i][1];
    n2 = quads[i][2];
    n3 = quads[i][3];

    /* Retrieving information from spacing-object list for 1st node */
    if( fabs( riemann_t[n0][1] ) < 1.0e-12 && fabs( riemann_t[n0][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[n0][0], rmt1 );
    else
      a_t->Full_Tensor( riemann_t[n0], rmt1 );
    /* Retrieving information from spacing-object list for 2nd node */
    if( fabs( riemann_t[n1][1] ) < 1.0e-12 && fabs( riemann_t[n1][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[n1][0], rmt2 );
    else
      a_t->Full_Tensor( riemann_t[n1], rmt2 );
    /* Combining tensors from first two nodes */
    a_t->Combine_Tensors( rmt1, rmt2, rmt3 );
    /* Retrieving information from spacing-object list for 3rd node */
    if( fabs( riemann_t[n2][1] ) < 1.0e-12 && fabs( riemann_t[n2][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[n2][0], rmt1 );
    else
      a_t->Full_Tensor( riemann_t[n2], rmt1 );
    /* Combining tensor from first three nodes */
    a_t->Combine_Tensors( rmt1, rmt3, rmt2 );
    /* Retrieving information from spacing-object list for 4th node */
    if( fabs( riemann_t[n3][1] ) < 1.0e-12 && fabs( riemann_t[n3][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[n3][0], rmt1 );
    else
      a_t->Full_Tensor( riemann_t[n3], rmt1 );
    /* Combining tensor information for all four nodes */
    a_t->Combine_Tensors( rmt1, rmt2, rmt3 );

    /* Decomposing tensor in eigenvalues and eigenvectors */
    a_t->Decompose_Tensor( rmt3, left, right, lam );

    /* Getting spacing distance in principal directions */
    h1 = sqrt( 1.0 / lam[0] );
    h2 = sqrt( 1.0 / lam[1] );

    if( h1 < dsGlobal || h2 < dsGlobal ) {
      /* STORE SCALARS or mixed if big h is bigger than small h by 50% or
       * more */
      if( mode == 0 || ( mode == 2 && MAX( h1, h2 ) / MIN( h1, h2 ) < 1.5 ) ) {
        ds = MIN( h1, h2 );
        if( ds < dsGlobal ) {
          vert[0][0] = x[n0];
          vert[0][1] = y[n0];
          vert[1][0] = x[n1];
          vert[1][1] = y[n1];
          vert[2][0] = x[n2];
          vert[2][1] = y[n2];
          vert[3][0] = x[n3];
          vert[3][1] = y[n3];
          a_t->Store_Scalar( ds, 4, vert );
          marked_t[nTri+i] = true;// Since the process is element based, nTri+i is a tensor
          marked_n[n0]     = true;
          marked_n[n1]     = true;
          marked_n[n2]     = true;
          marked_n[n3]     = true;
          count++;
          nScal++;
        }
      }
      else {
        vert[0][0] = x[n0];
        vert[0][1] = y[n0];
        vert[1][0] = x[n1];
        vert[1][1] = y[n1];
        vert[2][0] = x[n2];
        vert[2][1] = y[n2];
        vert[3][0] = x[n3];
        vert[3][1] = y[n3];
        a_t->Store_Tensor( rmt3, 4, vert );
        marked_t[nTri+i] = true;// Since the process is element based, nTri+i is a tensor
        marked_n[n0]     = true;
        marked_n[n1]     = true;
        marked_n[n2]     = true;
        marked_n[n3]     = true;
        count++;
        nTen++;
      }
    }
  }
  Print_Info();
  printf( "Number of items stored   %d\n", count );
  Print_Info();
  printf( "Number of scalars stored %d\n", nScal );
  Print_Info();
  printf( "Number of tensors stored %d\n", nTen );

  /* Creating array of new coordinates and mapping old to new coordinates */
  int j, * new_coord = NULL;
  double * new_x  = NULL;
  double * new_y  = NULL;
  new_coord = ( int* ) malloc ( nNodes *  sizeof( int ) );
  new_x =  ( double* ) malloc ( nNodes *  sizeof( double ) );
  new_y =  ( double* ) malloc ( nNodes *  sizeof( double ) );
  count = 0;
  for( i = 0; i < nNodes; i++ ) {
    new_coord[i] = -1;
    if( marked_n[i] ) {
      new_coord[i] = count;// Map from old node indices to new ones
      new_x[count] = x[i];
      new_y[count] = y[i];
      count++;
    }
  }
  free( marked_n );
  int newNumberNodes = count;
  new_x = ( double* ) realloc ( new_x, newNumberNodes * sizeof( double ) );
  new_y = ( double* ) realloc ( new_y, newNumberNodes * sizeof( double ) );

  /* Creating old to new tensor index map, and reorganizing connectivity arrays */
  int ** cell_conn = NULL;
  int * conn_count = NULL;
  cell_conn  =( int** ) calloc ( ( nTri + nQuads ), sizeof( int* ) );
  conn_count = ( int* ) calloc ( ( nTri + nQuads ), sizeof( int ) );
  count = 0;
  for( i = 0; i < nTri; i++ ) {
    if( marked_t[i] ) {
      conn_count[count] = 3;
      cell_conn[count] = ( int* ) malloc ( 3 * sizeof( int ) );
      for( j = 0; j < 3; j++ )
        cell_conn[count][j] = new_coord[tri[i][j]];
      count++;
    }
  }
  for( i = 0; i < nQuads; i++ ) {
    if( marked_t[nTri+i] ) {
      conn_count[count] = 4;
      cell_conn[count] = ( int* ) malloc ( 4 * sizeof( int ) );
      for( j = 0; j < 4; j++ ) {
        cell_conn[count][j] = new_coord[quads[i][j]];
      }
      count++;
    }
  }
  free( marked_t );
  free( new_coord );
  int numberTens = count;
  conn_count = ( int* ) realloc ( conn_count, numberTens * sizeof( int ) );
  cell_conn = ( int** ) realloc ( cell_conn, numberTens * sizeof( int* ) );

  /* Optimizing option */
  int optimize;
  Print_Input();
  printf( "Optimize field? ( 0 -> NO, 1 -> YES ): " );
  scanf( "%d", &optimize );
  double mergeThresh, * mi_matriz = NULL;
  mi_matriz = ( double* ) calloc ( numberTens, sizeof( double ) );
  if( optimize ) { 
    Print_Input();
    printf( "Enter tensor merging threshold ( in % ) less than 50%: " );
    scanf( "%lf", &mergeThresh );
    Print_Info();
    printf( "Merging threshold for combining tensors is %.2lf\n", mergeThresh );
    mergeThresh /= 100.0;
    Print_Info();
    printf( "Optimizing field\n" );
    a_t->Optimize_Field( mode, &numberTens, &conn_count, &cell_conn,
                         &newNumberNodes, &new_x, &new_y, mergeThresh,
                         &mi_matriz, &a_t );
    Print_Done();
  }
  else {
    Print_Info();
    printf( "No field optimization.\n" );
  }

  /* Checking correct number of tensors */
  if( numberTens != a_t->Number_of_Entries() ) {
    Print_Info();
    printf( "ERROR: Counts of entries in tensor object array do not match.\n" );
    Print_Info();
    printf( "numberTens: %d, Number_of_Entries: %d. Exiting!\n",
            numberTens,
            a_t->Number_of_Entries() );
    exit( EXIT_FAILURE );
  }

  /* Storing spacing information in arrays */
  int nCellVars = 8;// <- <- <- <- This number is being modified constantly
  double * cell_vars = NULL;
  cell_vars = ( double* ) malloc ( nCellVars * numberTens * sizeof( double ) );
  for( i = 0; i < numberTens; i++ ) {
    /* Retrieving  tensor */
    a_t->Retrieve_Tensor_Item( i, rmt1, j, vert );
//    printf( "* * * * * * segfault! -> %d\n", i );
    cell_vars[nCellVars*i+0] = rmt1[0][0];
    cell_vars[nCellVars*i+1] = rmt1[1][0];
    cell_vars[nCellVars*i+2] = rmt1[0][1];
    cell_vars[nCellVars*i+3] = rmt1[1][1];

    /* Decomposing tensor and calculating components of direction vectors,
     * spacings, and spacing vectors */
    //a_t->Decompose_Tensor( rmt1, left, right, lam );
    //cell_vars[nCellVars*i+4] = left[0][0] / sqrt( lam[0] );
    //cell_vars[nCellVars*i+5] = left[1][0] / sqrt( lam[0] );
    //cell_vars[nCellVars*i+6] = left[0][1] / sqrt( lam[1] );
    //cell_vars[nCellVars*i+7] = left[1][1] / sqrt( lam[1] );
    cell_vars[nCellVars*i+4] = 0.0;
    cell_vars[nCellVars*i+5] = 0.0;
    cell_vars[nCellVars*i+6] = 0.0;
    cell_vars[nCellVars*i+7] = 0.0;
    cell_vars[nCellVars*i+7] = mi_matriz[i];
  }

  /* Used for testing, TO BE DELETED */
  if( mi_matriz != NULL )
    free( mi_matriz );

  Print_Info();
  printf( "Creating Visualization Tool Kit file...\n" );
  char cellTensName[] = "cellTensorTest.vtu";
  Print_Input();
  printf( "Enter name for VTK file( something.vtu ): " );
  scanf( "%s", &cellTensName );
  Print_Info();
  printf( "filename<%s>\n", cellTensName );
  Write_VTK_2D_General( cellTensName, newNumberNodes, new_x, new_y,
                        numberTens, conn_count, cell_conn, 1, f,
                        nCellVars, cell_vars );
  Print_Done();

  /* Freeing memory */
  free( new_x );
  free( new_y );
  free( cell_vars );
  free( conn_count );
  for( i = 0; i < numberTens; i++ ) {
    free( cell_conn[i] );
  }
  free( cell_conn );

  /* Exporting tensor file */
  char fname[] = "test.tensor";
  Print_Info();
  printf( "Creating tensor file...\n" );
  Print_Info();
  printf( "Enter name for tensor file( something.tensor ): " );
  scanf( "%s", &fname );
  Print_Info();
  printf( "filename <%s>\n", fname );
  a_t->Export_Spacing( fname );
  Print_Done();
  delete a_t;

}

/* Storing spacing information at nodes (centroidal duals) */
void Node_Based_Storing( int nNodes,
                         int nTri,
                         int nQuads,
                         int nBs,
                         int * nSeg,
                         int ** tri,
                         int ** quads,
                         int *** bs,
                         double * x,
                         double * y,
                         double * avg_node_dist,
                         double ** riemann_t,
                         int mode,
                         double dsGlobal,
                         Vector * gradient ) {

  /* Getting grid in node tensor extent version */
  int dual, centArraySize;
  int * n2c_count, ** node_to_cent;
  double * x_cent, * y_cent;
  Print_Info();
  printf( "Getting extents for information at nodes...\n" );
  Print_Input();
  printf( "Select type of extents( 0 -> simple extents, 1 -> duals ): " );
  scanf( "%d", &dual );
  Print_Info();
  if( dual ) {
    printf( "Getting grid of centroidal duals...\n" );
    Get_Node_Version_Grid( nNodes, nTri, nQuads, nBs, &centArraySize, nSeg,
                           tri, quads, &n2c_count, &node_to_cent, bs, x, y,
                           &x_cent, &y_cent );
  }
  else {
    printf( "Getting grid of simple extents...\n" );
    Get_Simple_Squares_Grid( nNodes, &centArraySize, &n2c_count,
                             &node_to_cent, avg_node_dist, x, y, &x_cent,
                             &y_cent );
  }
  Print_Done();

  /* Going around nodes adding tensors to list and marking used nodes */
  Spacing_Obj * a_t;
  a_t = new Spacing_Obj();
  bool * marked_c = NULL;
  bool * marked_t = NULL;
  int i, j, n, nScal, nTen, count;
  double ds, h1, h2, vert[100000][2];
  double left[2][2], right[2][2], lam[2];
  double rmt1[2][2], rmt2[2][2], rmt3[2][2];
//  centArraySize = nTri + nQuads;
//  for( i = 0; i < nBs; i++ ) {
//    centArraySize += 2 * nSeg[i];
//  }
  marked_t = ( bool* ) calloc ( nNodes, sizeof( bool ) );
  marked_c = ( bool* ) calloc ( centArraySize, sizeof( bool ) );
  nScal = nTen = count = 0;
  for( i = 0; i < nNodes; i++ ) {

    /* Retrieve tensor from spacing object list */
    if( fabs( riemann_t[i][1] ) < 1.0e-12 && fabs( riemann_t[i][2] ) < 1.0e-12 )
      a_t->Scalar_To_Tensor( riemann_t[i][0], rmt2 );
    else
      a_t->Full_Tensor( riemann_t[i], rmt2 );

    /* Decomposing tensor into eigenvalues and eigenvectors */
    a_t->Decompose_Tensor( rmt2, left, right, lam );

    /* Getting spacing distance in principal directions */
    h1 = sqrt( 1.0 / lam[0] );
    h2 = sqrt( 1.0 / lam[1] );

    /* STORE TENSORS or mixed if big h is bigger than small h by 50% or more */
    if( mode == 1 || ( mode == 2 && MAX( h1, h2 ) / MIN( h1, h2 ) > 1.5 ) ) {

      /* Desired spacing too big */
      if( h1 > dsGlobal && h2 > dsGlobal ) {
        continue;
      }

      // I think there's no reason for this, it's repetitive
      // Before I commented this out, there used to be only one conditional
      // like this right after the beginning of the loop. But I had to add a
      // second one because of a bug.
      //if( fabs( riemann_t[i][1] ) < 1.0e-12 )
      //  a_t->Scalar_To_Tensor( riemann_t[i][0], rmt2 );
      //else
      //  a_t->Full_Tensor( riemann_t[i], rmt2 );

      /* Building the new extents for node tensors */
      if( dual ) {
        /* Going around nodes of duals */
        for( j = 0; j < n2c_count[i]; j++ ) {
          n = node_to_cent[i][j];
          vert[j][0] = x_cent[n];
          vert[j][1] = y_cent[n];
          marked_c[n] = true;
        }

        /* Storing tensor */
        a_t->Store_Tensor( rmt2, n2c_count[i], vert );
        marked_t[i] = true;
        count++;
        nTen++;
      }
      else {
        /* Going around extents of simple box */
        n = node_to_cent[i][0];
        vert[0][0] = x_cent[n];
        vert[0][1] = y_cent[n];
        marked_c[n] = true;

        n = node_to_cent[i][1];
        marked_c[n] = true;

        n = node_to_cent[i][2];
        vert[1][0] = x_cent[n];
        vert[1][1] = y_cent[n];
        marked_c[n] = true;

        n = node_to_cent[i][3];
        marked_c[n] = true;

        /* Storing tensor */
        a_t->Store_Tensor( rmt2, vert[0], vert[1] );
        marked_t[i] = true;
        count++;
        nTen++;
      }

    }
    /* STORE SCALARS */
    else {
      ds = MIN( h1, h2 );
      if( ds > dsGlobal ) {
        continue;
      }

      /* Building the new extents for node tensors */
      if( dual ) {
        /* Going around nodes of duals */
        for( j = 0; j < n2c_count[i]; j++ ) {
          n = node_to_cent[i][j];
          vert[j][0] = x_cent[n];
          vert[j][1] = y_cent[n];
          marked_c[n] = true;
        }

        /* Storing scalar */
        a_t->Store_Scalar( ds, n2c_count[i], vert );
        marked_t[i] = true;
        count++;
        nScal++;
      }
      else {
        /* Going around extents of simple box */
        n = node_to_cent[i][0];
        vert[0][0] = x_cent[n];
        vert[0][1] = y_cent[n];
        marked_c[n] = true;

        n = node_to_cent[i][1];
        marked_c[n] = true;

        n = node_to_cent[i][2];
        vert[1][0] = x_cent[n];
        vert[1][1] = y_cent[n];
        marked_c[n] = true;

        n = node_to_cent[i][3];
        marked_c[n] = true;

        /* Storing scalar */
        a_t->Store_Scalar( ds, vert[0], vert[1] );
        marked_t[i] = true;
        count++;
        nScal++;
      }

      /* Storing scalar */
      a_t->Store_Scalar( ds, n2c_count[i], vert );
      marked_t[i] = true;
      count++;
      nScal++;
    }
  }
  Print_Info();
  printf( "Number of items stored %d\n", count );
  Print_Info();
  printf( "Number of scalars stored %d\n", nScal );
  Print_Info();
  printf( "Number of tensors stored %d\n", nTen );

//  FILE * myfp;
//  if( ( myfp = fopen( "debug2.out", "w" ) ) == NULL ) {
//    printf( "Couldn't open debug2.out\n" );
//    exit( 0 );
//  }
//  for( i = 0; i < nNodes; i++ ) {
//    fprintf( myfp, "%4d: %20.14e, %20.14e, %20.14e\n", i,
//             riemann_t[i][0], riemann_t[i][1], riemann_t[i][2] );
//  }
//  fclose( myfp );
  /* Creating old to new centroid index map, and reorganizing array of
   * coordinates */
  int * new_cent = NULL;
  new_cent = ( int* ) malloc ( centArraySize * sizeof( int ) );
  count = 0;
  for( i = 0; i < centArraySize; i++ ) {
    new_cent[i] = -1;
    if( marked_c[i] ) {
      new_cent[i] = count;
      x_cent[count] = x_cent[i];
      y_cent[count] = y_cent[i];
      count++;
    }
  }

  /*** Freeing memory ***/
  free( marked_c );

  int newCAS = count;
  x_cent = ( double* ) realloc ( x_cent, newCAS * sizeof( double ) );
  y_cent = ( double* ) realloc ( y_cent, newCAS * sizeof( double ) );

  /* Creating old to new tensor index map, and reorganizing array of tensors */
  count = 0;
  int * new_to_old;
  new_to_old = ( int* ) malloc ( nNodes * sizeof( int ) );
  for( i = 0; i < nNodes; i++ ) {
    new_to_old[i] = -1;
    if( marked_t[i] ) {
      new_to_old[count] = i;
      n2c_count[count] = n2c_count[i];
      node_to_cent[count] = ( int* ) realloc ( node_to_cent[count], n2c_count[count] * sizeof( int ) );
      for( n = 0; n < n2c_count[count]; n++ ) {
        node_to_cent[count][n] = new_cent[node_to_cent[i][n]];
      }
      count++;
    }
  }
  free( marked_t );
  free( new_cent );
  int numberTens = count;
  for( i = numberTens; i < nNodes; i++ ) {
    free( node_to_cent[i] );
  }
  n2c_count    = ( int* ) realloc ( n2c_count, numberTens * sizeof( int ) );
  node_to_cent = ( int** ) realloc ( node_to_cent, numberTens * sizeof( int* ) );

  /* Used for testing, TO BE DELETED */
  double * mi_matriz;
  mi_matriz = ( double* ) calloc ( numberTens, sizeof( double ) );

  /* Optimizing function */
  int optimize;
  double mergeThresh;
  if( dual ) {
    Print_Input();
    printf( "Optimize field? ( 0 -> NO, 1 -> YES ): " );
    scanf( "%d", &optimize );
    if( optimize ) {
      Print_Input();
      printf( "Enter tensor merging threshold ( in % ) less than 50%: " );
      scanf( "%lf", &mergeThresh );
      Print_Info();
      printf( "Merging threshold for combining tensors is %.2lf\n", mergeThresh );
      mergeThresh /= 100.0;

      a_t->Optimize_Field( mode, &numberTens, &n2c_count, &node_to_cent, &newCAS,
                           &x_cent, &y_cent, mergeThresh, &mi_matriz, &a_t );
      Print_Done();
    }
    else {
      Print_Info();
      printf( "No field optimization.\n" );
    }
  }
  else {
    Print_Info();
    printf( "No field optimization option for simple squares.\n" );
  }

  /* Checking correct number of tensors */
  if( numberTens != a_t->Number_of_Entries() ) {
    Print_Info();
    printf( "ERROR: Counts of entries in tensor object array do not match.\n" );
    Print_Info();
    printf( "numberTens: %d, Number_of_Entries: %d. Exiting!\n",
            numberTens, a_t->Number_of_Entries() );
    exit( EXIT_FAILURE );
  }

  /* Storing spacing information in arrays */
  int nCellVars = 8;//************ <- <- <- <- This number is being modified
  double * cell_vars = NULL;
  cell_vars = ( double* ) malloc ( nCellVars * numberTens * sizeof( double ) );
  for( i = 0; i < numberTens; i++ ) {
    /* Retrieving  tensor */
//    printf( "Segfault?2\n" );
    a_t->Retrieve_Tensor_Item( i, rmt1, j, vert );
//    fprintf( fp2, "%4d: %20.14e, %20.14e, %20.14e\n", i,
//             rmt1[0][0], rmt1[1][0], rmt1[1][1] );
//    rmt1[0][0] = riemann_t[i][0];
//    rmt1[1][0] = riemann_t[i][1];
//    rmt1[0][1] = riemann_t[i][1];
//    rmt1[1][1] = riemann_t[i][2];
    cell_vars[nCellVars*i+0] = rmt1[0][0];
    cell_vars[nCellVars*i+1] = rmt1[1][0];
    cell_vars[nCellVars*i+2] = rmt1[0][1];
    cell_vars[nCellVars*i+3] = rmt1[1][1];

    /* Decomposing tensor and calculating components of direction vectors,
     * spacings, and spacing vectors */
    a_t->Decompose_Tensor( rmt1, left, right, lam );
    //cell_vars[nCellVars*i+4] = left[0][0] / sqrt( lam[0] );
    //cell_vars[nCellVars*i+5] = left[1][0] / sqrt( lam[0] );
    //cell_vars[nCellVars*i+6] = left[0][1] / sqrt( lam[1] );
    //cell_vars[nCellVars*i+7] = left[1][1] / sqrt( lam[1] );
    cell_vars[nCellVars*i+4] = gradient[new_to_old[i]][0];
    cell_vars[nCellVars*i+5] = gradient[new_to_old[i]][1];
    cell_vars[nCellVars*i+6] = sqrt( 1.0 / lam[0] );
    cell_vars[nCellVars*i+7] = mi_matriz[i];
  }
  free( new_to_old );

  /* Used for testing, TO BE DELETED */
  free( mi_matriz );

  /* Generating extent vtk file */
  Print_Info();
  printf( "Creating Visualization Tool Kit file...\n" );
  char nodeTensName[] = "nodeTensorTest.vtu";
  Print_Input();
  printf( "Enter name for VTK file( something.vtu ): " );
  scanf( "%s", &nodeTensName );
  Print_Info();
  printf( "filename<%s>\n", nodeTensName );
  Write_VTK_2D_General( nodeTensName, newCAS, x_cent, y_cent, numberTens,
                        n2c_count, node_to_cent, 0, NULL, nCellVars,
                        cell_vars );
  Print_Done();

  /*** Freeing memory ***/
  for( i = 0; i < numberTens; i++ ) {
    free( node_to_cent[i] );
  }
  free( node_to_cent );
  free( n2c_count );
  free( cell_vars );
  free( x_cent );
  free( y_cent );

  /* Exporting tensor file */
  char fname[] = "test.tensor";
  Print_Info();
  printf( "Creating tensor file...\n" );
  Print_Info();
  printf( "Enter name for tensor file( something.tensor ): " );
  scanf( "%s", &fname );
  Print_Info();
  printf( "filename <%s>\n", fname );
  a_t->Export_Spacing( fname );
  Print_Done();
  delete a_t;

}

