#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include"Space.h"
#include"Point.h"
#include"SUtil.h"
#include"PList.h"
#include"Vector.h"
#include"Linked_List.h"
#include"2DSpacing_Obj.h"
#include"ANALYTIC_FUNCTIONS.h"

#ifndef TWODSPACING_FUNCTIONS_H
#define TWODSPACING_FUNCTIONS_H

#define MIN( x, y ) ( ( x ) <= ( y ) ? ( x ) : ( y ) )
#define MAX( x, y ) ( ( x ) >= ( y ) ? ( x ) : ( y ) )

void Print_Info();

void Print_Done();

void Print_Input();

void Print_RB();

void Get_Root_Bounds( int nNodes,
                      int maxSpaceLevel,
                      double * ds,
                      double * dsGlobal,
                      double root_lo[],
                      double root_hi[],
                      double * x,
                      double * y );

void Get_Node_To_Node( int nTri,
                       int nQuads,
                       int ** tri,
                       int ** quads,
                       Linked_List ** n2n );

void Analytical_Function( int nNodes,
                          double ds,
                          double root_lo[2],
                          double root_hi[2],
                          double * x,
                          double ** t_f );

void Get_Analytical_Function( int nVar,
                              int nNodes,
                              double ds,
                              double root_lo[],
                              double root_hi[],
                              double * x,
                              double * var,
                              double ** fOUT );

void Store_Solution_Data( int nNodes,
                          int nTri,
                          int nQuads,
                          int nBs,
                          int * nSeg,
                          int ** tri,
                          int ** quads,
                          int *** bs,
                          double * x,
                          double * y,
                          double * f );

void Get_Cell_Gradients( int nNodes,
                         int nTri,
                         int nQuads,
                         int invDistWeight,
                         int ** tri,
                         int ** quads,
                         double * x,
                         double * y,
                         double * f,
                         Vector * gradient );

void Print_dfS();

void Get_df_Stats( int nNodes,
                   int * avg_node_count,
                   double power,
                   double * dfmin,
                   double * dfmax,
                   double * dfmean,
                   double * dfsd,
                   double * avg_node_dist,
                   double * x,
                   double * y,
                   Vector * gradient,
                   Linked_List ** n2n );

void Get_Riemann_Metrics( int nNodes,
                          double power,
                          double limit,
                          double dsGlobal,
                          double * avg_node_dist,
                          double ** riemann_t,
                          Vector * gradient,
                          Spacing_Obj * a_t );

void Get_Metrics( int nNodes,
                  int nTri,
                  int nQuads,
                  int ** tri,
                  int ** quads,
                  double * x,
                  double * y,
                  double * f,
                  double dsGlobal,
                  Linked_List ** node_to_node,
                  double ** avg_node_distOUT,
                  double *** riemann_tOUT,
                  Vector ** gradientTEMP );

void Reorder_Boundaries( int nBs,
                         int * nSeg,
                         int *** bs );

void Get_Node_Version_Grid( int nNodes,
                            int nTri,
                            int nQuads,
                            int nBs,
                            int * nExtents,
                            int * nSeg,
                            int ** tri,
                            int ** quads,
                            int ** t_n2c_count,
                            int *** t_node_to_cent,
                            int *** bs,
                            double * x,
                            double * y,
                            double ** t_x,
                            double ** t_y );

void Get_Simple_Squares_Grid( int nNodes,
                              int * t_nNewNodes,
                              int ** t_n2c_count,
                              int *** t_node_to_cent,
                              double * avg_node_dist,
                              double * x,
                              double * y,
                              double ** t_x,
                              double ** t_y );

void Storing_Spacing_Options( int * eBasedOUT,
                              int * modeOUT,
                              double * dsGlobalOUT );

void Element_Based_Storing( int nNodes,
                            int nTri,
                            int nQuads,
                            int ** tri,
                            int ** quads,
                            double * x,
                            double * y,
                            double ** riemann_t,
                            int mode,
                            double dsGlobal,
                            double * f );

void Node_Based_Storing( int nNodes,
                         int nTri,
                         int nQuads,
                         int nBs,
                         int * nSeg,
                         int ** tri,
                         int ** quads,
                         int *** bs,
                         double * x,
                         double * y,
                         double * avg_node_dist,
                         double ** riemann_t,
                         int mode,
                         double dsGlobal,
                         Vector * gradient );

#endif

