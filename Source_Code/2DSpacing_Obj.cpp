/* THIS IS THE 2D FRIENDLY VERSION OF Spacing_Obj.cpp */
#include<stdio.h>
#include<math.h>
#include<time.h>
#include<string.h>
#include<stdbool.h>
#include"Space.h"
#include"2DSpacing_Obj.h"
#include"svdcmp.h"

#define MIN(x,y) ((x) <= (y) ? (x) : (y))
#define MAX(x,y) ((x) >= (y) ? (x) : (y))

double tol = 1.0e-14;

void Top_Ordering( const int max,
                   int * item,
                   int * function ) {


  int i, j, index, maxIndex, safeIndex, maxFunc, safeFunc;

  for( i = 0; i < max; i++ ) {
    maxFunc = safeFunc = function[i];
    maxIndex = safeIndex = item[i];
    index = i;
    for( j = i+1; j < max; j++ ) {
      if( function[j] > maxFunc ) {
        maxFunc = function[j];
        maxIndex = item[j];
        index = j;
      }
    }
    function[i] = maxFunc;
    function[index] = safeFunc;
    item[i] = maxIndex;
    item[index] = safeIndex;
  }

}

#if SPACE == 2
/* 2D vector cross product, a double is returned */
double vector_cross_product( double v1[2], double v2[2] ) {
  return( v1[0]*v2[1]-v1[1]*v2[0] );
}

/* Normalizing 2D vector */
void vector_normalize(double v[2]) {
  double mag = MAX( tol, sqrt( v[0]*v[0] + v[1]*v[1] ) );
  v[0] /= mag;
  v[1] /= mag;
}

/* Dot product for 2D */
double vector_dot_product(double v1[2], double v2[2])
{
  return( v1[0]*v2[0] + v1[1]*v2[1] );
}
#else
/* 3D vector cross product, a vector is modified */
void vector_cross_product( double v1[3], double v2[3], double v3[3] ) {
  v3[0] = v1[1]*v2[2]-v1[2]*v2[1];
  v3[1] = v1[2]*v2[0]-v1[0]*v2[2];
  v3[2] = v1[0]*v2[1]-v1[1]*v2[0];
}

/* Normalizing 3D vector */
void vector_normalize(double v[3]) {
  double mag = MAX( tol, sqrt( v[0]*v[0] + v[1]*v[1] + v[2]*v[2] ) );
  v[0] /= mag;
  v[1] /= mag;
  v[2] /= mag;
}

/* Dot product for 3D */
double vector_dot_product(double v1[3], double v2[3])
{
  return( v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2] );
}
#endif

/* Initialize root using lo, hi, global space, level, smn? */
void Spacing_Obj::Initialize_Root(double lo[SD], double hi[SD],  double gsp, int lvl=0, double smn=0.0)
{

#if SPACE == 2
  Quadtree_Storage *dummy=0;
  space_root = new Quadtree_Storage((Quadtree_Storage*)dummy,lo,hi,lvl,smn); // Create root octant using extents
#else
  Octree_Storage *dummy=0;
  space_root = new Octree_Storage((Octree_Storage*)dummy,lo,hi,lvl,smn); // Create root octant using extents
#endif
  global_space = gsp;
  max_level = lvl;
}

/* Look for pt inside an element */
int Spacing_Obj::Point_In_Element(double pt[SD],
                                  Size_Obj *ptr) {

  int nVert;
  int i, j, k, s, i0, i1, j0, j1;
  double totalAngle, dumb;
  Point p1, p2, p3;

#if SPACE == 2
  double normal[2], vector[2], mid[2], lo[2], hi[2];
  nVert = ptr->nv;
  p1 = Point( pt[0], pt[1] );

  /* For polygons */
  if( nVert > 2 ) {
    /* Summation of angles test for point in element */
    k = 0;
    totalAngle = 0.0;
    for( s = 0; s < nVert; s++ ) {
      i0 = s;
      i1 = ( s + 1 ) % nVert;
      p2 = Point( ptr->vert[i0][0], ptr->vert[i0][1] );
      p3 = Point( ptr->vert[i1][0], ptr->vert[i1][1] );
      dumb = angle( p1, p2, p3 );
      if( fabs( dumb - M_PI ) < 1.0e-12 ) {
        return( 1 );
      }
      totalAngle += dumb;
//      printf( "%d: angle+ %.2lf\n", s+1, dumb );
    }
    if( totalAngle > 1.0e-5 )  k = 1;
//    printf( "angle %.2lf\n", totalAngle );
//    /* Switch to each face ( edge ) of element, calculate face vector,
//     * calculate vector from middle of face to point looked for and calculate
//     * dot product. If dot product is negative ACROSS ALL FACES OF ELEMENT,
//     * then point is inside element */
//    k = 1;
//    for( s = 0; s < nVert && k; s++ ) {
//      i0 = s;
//      i1 = ( s + 1 ) % nVert;
//      normal[0] =    ptr->vert[i1][1] - ptr->vert[i0][1];
//      normal[1] = -( ptr->vert[i1][0] - ptr->vert[i0][0] );
//      vector_normalize( normal );
//      mid[0] = 0.5 * ( ptr->vert[i1][0] + ptr->vert[i0][0] );
//      mid[1] = 0.5 * ( ptr->vert[i1][1] + ptr->vert[i0][1] );
//      vector[0] = pt[0] - mid[0];
//      vector[1] = pt[1] - mid[1];
//      if( vector_dot_product( normal, vector ) > tol )
//        k = 0;
//    }
  }
  /* For simple extents */
  else {
    k = 1;
    /* extent test */
    lo[0] = lo[1] =  1.0e20;
    hi[0] = hi[1] = -1.0e20;
    for( j = 0; j < ptr->nv; j++ ) {
      for( k = 0; k < 2; k++ ) {
        lo[k] = MIN( lo[k], ptr->vert[j][k] );
        hi[k] = MAX( hi[k], ptr->vert[j][k] );
      }
    }

    if( pt[0] < lo[0] || pt[0] > hi[0] ||
        pt[1] < lo[1] || pt[1] > hi[1] )
      k = 0;
  }


#else
  double v1[3], v2[3], v3[3], lo[3], hi[3];
  /* Depending on number of vertices, switch to type of element */
  switch (ptr->nv) {

    /* Switch to three consecutive vertices to get two adjacent vectors,
     * calculate cross product, and calculate dot product between resulting
     * vector and vector from middle vertex and point looked for. If dot product
     * is negative ACROSS ALL FACES OF ELEMENT, then point is inside element */

    // NOTE: element logic assumes the faces are planar
    case 4: // Tetrahedron element
      k=1;
      for (s=0; s < 4 && k; s++) {
        switch(s) {
          case 0: i0 = 0; i1 = 2; j0 = 0; j1 = 1; break;
          case 1: i0 = 0; i1 = 1; j0 = 0; j1 = 3; break;
          case 2: i0 = 1; i1 = 2; j0 = 1; j1 = 3; break;
          case 3: i0 = 2; i1 = 0; j0 = 2; j1 = 3; break;
        }
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        v1[0] = pt[0]-ptr->vert[i0][0];
        v1[1] = pt[1]-ptr->vert[i0][1];
        v1[2] = pt[2]-ptr->vert[i0][2];
        if (vector_dot_product(v1,v3) > tol)
          k=0;
      }
      break;
    case 5: // Pyramid element
      k=1;
      for (s=0; s < 5 && k; s++)
      {
        switch(s)
        {
          case 0: i0 = 0; i1 = 2; j0 = 3; j1 = 1; break;
          case 1: i0 = 0; i1 = 1; j0 = 0; j1 = 4; break;
          case 2: i0 = 1; i1 = 2; j0 = 1; j1 = 4; break;
          case 3: i0 = 2; i1 = 3; j0 = 2; j1 = 4; break;
          case 4: i0 = 3; i1 = 0; j0 = 3; j1 = 4; break;
        }
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        v1[0] = pt[0]-ptr->vert[i0][0];
        v1[1] = pt[1]-ptr->vert[i0][1];
        v1[2] = pt[2]-ptr->vert[i0][2];
        if (vector_dot_product(v1,v3) > tol)
          k=0;
      }
      break;
    case 6: // Prism element
      k=1;
      for (s=0; s < 5 && k; s++)
      {
        switch(s)
        {
          case 0: i0 = 0; i1 = 4; j0 = 1; j1 = 3; break;
          case 1: i0 = 1; i1 = 5; j0 = 2; j1 = 4; break;
          case 2: i0 = 2; i1 = 3; j0 = 0; j1 = 5; break;
          case 3: i0 = 0; i1 = 2; j0 = 0; j1 = 1; break;
          case 4: i0 = 3; i1 = 4; j0 = 3; j1 = 5; break;
        }
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        v1[0] = pt[0]-ptr->vert[i0][0];
        v1[1] = pt[1]-ptr->vert[i0][1];
        v1[2] = pt[2]-ptr->vert[i0][2];
        if (vector_dot_product(v1,v3) > tol)
          k=0;
      }
      break;
    case 8: // Hex element
      k=1;
      for (s=0; s < 6 && k; s++)
      {
        switch(s)
        {
          case 0: i0 = 0; i1 = 2; j0 = 3; j1 = 1; break;
          case 1: i0 = 0; i1 = 5; j0 = 1; j1 = 4; break;
          case 2: i0 = 1; i1 = 6; j0 = 2; j1 = 5; break;
          case 3: i0 = 2; i1 = 7; j0 = 3; j1 = 6; break;
          case 4: i0 = 3; i1 = 4; j0 = 0; j1 = 7; break;
          case 5: i0 = 4; i1 = 6; j0 = 5; j1 = 7; break;
        }
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        v1[0] = pt[0]-ptr->vert[i0][0];
        v1[1] = pt[1]-ptr->vert[i0][1];
        v1[2] = pt[2]-ptr->vert[i0][2];
        if (vector_dot_product(v1,v3) > tol)
          k=0;
      }
      break;
    default: // Entire extent as default
      k=1;
      // extent test
      lo[0] = lo[1] = lo[2] = 1.0e20;
      hi[0] = hi[1] = hi[2] = -1.0e20;
      for (j=0; j < ptr->nv; j++)
      {
        for (k=0; k < 3; k++)
        {
          lo[k] = MIN(lo[k],ptr->vert[j][k]);
          hi[k] = MAX(hi[k],ptr->vert[j][k]);
        }
      }

      if (pt[0] < lo[0] || pt[0] > hi[0] ||
          pt[1] < lo[1] || pt[1] > hi[1] ||
          pt[2] < lo[2] || pt[2] > hi[2])
        k=0;
      break;
  }
#endif

  return(k);
}

/* Compute portion of edge inside element */
double Spacing_Obj::Edge_In_Element(double node1[SD],
                                    double node2[SD],
                                    Size_Obj *ptr) {

  int i0, i1, j0, j1, s, dumb;
  double dc, ds, dot0, dot1;

#if SPACE == 2

  int nVert;
  nVert = ptr->nv;
  double p1[2], p2[2];
  p1[0] = node1[0];
  p1[1] = node1[1];
  p2[0] = node2[0];
  p2[1] = node2[1];

  double normal[2], mid[2], v1[2], v2[2];
  // set clipped coordinates to original points
  v1[0] = p1[0] - p2[0];
  v1[1] = p1[1] - p2[1];
  /* Both ds and dc set to full edge length */
  ds = dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );

  if( nVert > 2 ) {
    // NOTE: element logic assumes the faces are planar
    /* Go through all vertices */
    /* Conditions for part of edge to be inside of element have to happen
     * ACROSS ALL FACES */
    for( s = 0; s < nVert && dc > tol; s++ ) {
      /* Get two consecutive nodes of element ( face ) */
      i0 = s;
      i1 = ( s + 1 ) % nVert;
      normal[0] =    ptr->vert[i1][1] - ptr->vert[i0][1];
      normal[1] = -( ptr->vert[i1][0] - ptr->vert[i0][0] );
      vector_normalize( normal );
      mid[0] = 0.5 * ( ptr->vert[i1][0] + ptr->vert[i0][0] );
      mid[1] = 0.5 * ( ptr->vert[i1][1] + ptr->vert[i0][1] );
      v1[0] = p1[0] - mid[0];
      v1[1] = p1[1] - mid[1];
      v2[0] = p2[0] - mid[0];
      v2[1] = p2[1] - mid[1];
      dot0 = vector_dot_product( v1, normal );
      dot1 = vector_dot_product( v2, normal );
      /* The following algorithm considers only the case of one dot
       * product positive and one dot product negative ( edge crosses that
       * face ), and the case of both dot products positive in which dc is
       * set to zero ( edge has no portion inside element ). The case of
       * both dot products negative is not considered since the edge would
       * later be treated in the case when it intersects another face (
       * edge does not cross current face ). */

      /* If both nodes of edge are out of element ( both dot products
       * greater than zero ), set dc to zero ( no portion of edge in
       * element ) */
      if( dot0 > tol && dot1 > tol ) {
        dc = 0.0;
      }
      /* If first node is out and second node is in, calculate magnitude of
       * vector from edge point at face to first edge node. Store in dc */
      else if( dot0 > tol && dot1 <= 0.0 ) {
        dc = fabs( dot1 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
        p1[0] = p2[0] + dc * ( p1[0] - p2[0] );
        p1[1] = p2[1] + dc * ( p1[1] - p2[1] );
        v1[0] = p1[0] - p2[0];
        v1[1] = p1[1] - p2[1];
        dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
      }
      /* If first node is in and second node is out, calculate magnitude of
       * vector from edge point at face to second edge node. Store in dc */
      else if( dot0 <= 0.0 && dot1 > tol ) {
        dc = fabs( dot0 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
        p2[0] = p1[0] + dc * ( p2[0] - p1[0] );
        p2[1] = p1[1] + dc * ( p2[1] - p1[1] );
        v1[0] = p2[0] - p1[0];
        v1[1] = p2[1] - p1[1];
        dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
      }
    }
  }
  else {
    dumb = 0;
    for( s = 0; s < 4 && dc > tol; s++ ) {
      switch( s ) {
        case 0:
          normal[0] = -1.0; normal[1] =  0.0; i0 = 0; break;
        case 1:
          normal[0] =  1.0; normal[1] =  0.0; i0 = 1; break;
        case 2:
          normal[0] =  0.0; normal[1] = -1.0; i0 = 0; break;
        case 3:
          normal[0] =  0.0; normal[1] =  1.0; i0 = 1; break;
      }
      v1[0] = p1[0] - ptr->vert[i0][0];
      v1[1] = p1[1] - ptr->vert[i0][1];
      v2[0] = p2[0] - ptr->vert[i0][0];
      v2[1] = p2[1] - ptr->vert[i0][1];
      dot0 = vector_dot_product( v1, normal );
      dot1 = vector_dot_product( v2, normal );
      /* The following algorithm considers only the case of one dot
       * product positive and one dot product negative ( edge crosses that
       * face ), and the case of both dot products positive in which dc is
       * set to zero ( edge has no portion inside element ). The case of
       * both dot products negative is not considered since the edge would
       * later be treated in the case when it intersects another face (
       * edge does not cross current face ). */

      /* If both nodes of edge are out of element ( both dot products
       * greater than zero ), set dc to zero ( no portion of edge in
       * element ) */
      if( dot0 > tol && dot1 > tol ) {
        dc = 0.0;
      }
      /* If first node is out and second node is in, calculate magnitude of
       * vector from edge point at face to first edge node. Store in dc */
      else if( dot0 > tol && dot1 <= 0.0 ) {
        dc = fabs( dot1 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
        p1[0] = p2[0] + dc * ( p1[0] - p2[0] );
        p1[1] = p2[1] + dc * ( p1[1] - p2[1] );
        v1[0] = p1[0] - p2[0];
        v1[1] = p1[1] - p2[1];
        dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
      }
      /* If first node is in and second node is out, calculate magnitude of
       * vector from edge point at face to second edge node. Store in dc */
      else if( dot0 <= 0.0 && dot1 > tol ) {
        dc = fabs( dot0 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
        p2[0] = p1[0] + dc * ( p2[0] - p1[0] );
        p2[1] = p1[1] + dc * ( p2[1] - p1[1] );
        v1[0] = p2[0] - p1[0];
        v1[1] = p2[1] - p1[1];
        dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
      }

    }
  }

  /* Find element type according to number of vertices */
//  switch( ptr->nv ) {
//    // NOTE: element logic assumes the faces are planar
//    /* Go through all vertices */
//    /* Conditions for part of edge to be inside of element have to happen
//     * ACROSS ALL FACES */
//    case 3: // Triangular element
//      for( s = 0; s < 3 && dc > tol; s++ ) {
//        /* Get two consecutive nodes of element ( face ) */
//        i0 = s;
//        i1 = ( s + 1 ) % 3;
//        normal[0] =    ptr->vert[i1][1] - ptr->vert[i0][1];
//        normal[1] = -( ptr->vert[i1][0] - ptr->vert[i0][0] );
//        vector_normalize( normal );
//        mid[0] = 0.5 * ( ptr->vert[i1][0] + ptr->vert[i0][0] );
//        mid[1] = 0.5 * ( ptr->vert[i1][1] + ptr->vert[i0][1] );
//        v1[0] = p1[0] - mid[0];
//        v1[1] = p1[1] - mid[1];
//        v2[0] = p2[0] - mid[0];
//        v2[1] = p2[1] - mid[1];
//        dot0 = vector_dot_product( v1, normal );
//        dot1 = vector_dot_product( v2, normal );
//        /* The following algorithm considers only the case of one dot
//         * product positive and one dot product negative ( edge crosses that
//         * face ), and the case of both dot products positive in which dc is
//         * set to zero ( edge has no portion inside element ). The case of
//         * both dot products negative is not considered since the edge would
//         * later be treated in the case when it intersects another face (
//         * edge does not cross current face ). */
//
//        /* If both nodes of edge are out of element ( both dot products
//         * greater than zero ), set dc to zero ( no portion of edge in
//         * element ) */
//        if( dot0 > tol && dot1 > tol ) {
//          dc = 0.0;
//        }
//        /* If first node is out and second node is in, calculate magnitude of
//         * vector from edge point at face to first edge node. Store in dc */
//        else if( dot0 > tol && dot1 <= 0.0 ) {
//          dc = fabs( dot1 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
//          p1[0] = p2[0] + dc * ( p1[0] - p2[0] );
//          p1[1] = p2[1] + dc * ( p1[1] - p2[1] );
//          v1[0] = p1[0] - p2[0];
//          v1[1] = p1[1] - p2[1];
//          dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
//        }
//        /* If first node is in and second node is out, calculate magnitude of
//         * vector from edge point at face to second edge node. Store in dc */
//        else if( dot0 <= 0.0 && dot1 > tol ) {
//          dc = fabs( dot0 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
//          p2[0] = p1[0] + dc * ( p2[0] - p1[0] );
//          p2[1] = p1[1] + dc * ( p2[1] - p1[1] );
//          v1[0] = p2[0] - p1[0];
//          v1[1] = p2[1] - p1[1];
//          dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
//        }
//      }
//      break;
//    case 4: // Quadrilateral element
//      for( s = 0; s < 4 && dc > tol; s++ ) {
//        /* Get two consecutive nodes of element ( face ) */
//        i0 = s;
//        i1 = ( s + 1 ) % 4;
//        normal[0] =    ptr->vert[i1][1] - ptr->vert[i0][1];
//        normal[1] = -( ptr->vert[i1][0] - ptr->vert[i0][0] );
//        vector_normalize( normal );
//        mid[0] = 0.5 * ( ptr->vert[i1][0] + ptr->vert[i0][0] );
//        mid[1] = 0.5 * ( ptr->vert[i1][1] + ptr->vert[i0][1] );
//        v1[0] = p1[0] - mid[0];
//        v1[1] = p1[1] - mid[1];
//        v2[0] = p2[0] - mid[0];
//        v2[1] = p2[1] - mid[1];
//        dot0 = vector_dot_product( v1, normal );
//        dot1 = vector_dot_product( v2, normal );
//        /* The following algorithm considers only the case of one dot
//         * product positive and one dot product negative ( edge crosses that
//         * face ), and the case of both dot products positive in which dc is
//         * set to zero ( edge has no portion inside element ). The case of
//         * both dot products negative is not considered since the edge would
//         * later be treated in the case when it intersects another face (
//         * edge does not cross current face ). */
//
//        /* If both nodes of edge are out of element ( both dot products
//         * greater than zero ), set dc to zero ( no portion of edge in
//         * element ) */
//        if( dot0 > tol && dot1 > tol ) {
//          dc = 0.0;
//        }
//        /* If first node is out and second node is in, calculate magnitude of
//         * vector from edge point at face to first edge node. Store in dc */
//        else if( dot0 > tol && dot1 <= 0.0 ) {
//          dc = fabs( dot1 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
//          p1[0] = p2[0] + dc * ( p1[0] - p2[0] );
//          p1[1] = p2[1] + dc * ( p1[1] - p2[1] );
//          v1[0] = p1[0] - p2[0];
//          v1[1] = p1[1] - p2[1];
//          dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
//        }
//        /* If first node is in and second node is out, calculate magnitude of
//         * vector from edge point at face to second edge node. Store in dc */
//        else if( dot0 <= 0.0 && dot1 > tol ) {
//          dc = fabs( dot0 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
//          p2[0] = p1[0] + dc * ( p2[0] - p1[0] );
//          p2[1] = p1[1] + dc * ( p2[1] - p1[1] );
//          v1[0] = p2[0] - p1[0];
//          v1[1] = p2[1] - p1[1];
//          dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
//        }
//      }
//      break;
//    default: // Extent square of a node tensor
//      dumb = 0;
////      printf( "\tRETRIEVE EDGE SCALAR: DEBUGGING\n" );
////      printf( "\t\t%7.3e %7.3e\n", ptr->vert[0][0], ptr->vert[0][1] );
////      printf( "\t\t%7.3e %7.3e\n", ptr->vert[1][0], ptr->vert[1][1] );
//      for( s = 0; s < 4 && dc > tol; s++ ) {
//        switch( s ) {
//          case 0:
//            normal[0] = -1.0; normal[1] =  0.0; i0 = 0; break;
//          case 1:
//            normal[0] =  1.0; normal[1] =  0.0; i0 = 1; break;
//          case 2:
//            normal[0] =  0.0; normal[1] = -1.0; i0 = 0; break;
//          case 3:
//            normal[0] =  0.0; normal[1] =  1.0; i0 = 1; break;
//        }
//        v1[0] = p1[0] - ptr->vert[i0][0];
//        v1[1] = p1[1] - ptr->vert[i0][1];
//        v2[0] = p2[0] - ptr->vert[i0][0];
//        v2[1] = p2[1] - ptr->vert[i0][1];
//        dot0 = vector_dot_product( v1, normal );
//        dot1 = vector_dot_product( v2, normal );
//        /* The following algorithm considers only the case of one dot
//         * product positive and one dot product negative ( edge crosses that
//         * face ), and the case of both dot products positive in which dc is
//         * set to zero ( edge has no portion inside element ). The case of
//         * both dot products negative is not considered since the edge would
//         * later be treated in the case when it intersects another face (
//         * edge does not cross current face ). */
//
//        /* If both nodes of edge are out of element ( both dot products
//         * greater than zero ), set dc to zero ( no portion of edge in
//         * element ) */
//        if( dot0 > tol && dot1 > tol ) {
////          printf( "\t* * * Outside element! %d\n", ++dumb );
//          dc = 0.0;
//        }
//        /* If first node is out and second node is in, calculate magnitude of
//         * vector from edge point at face to first edge node. Store in dc */
//        else if( dot0 > tol && dot1 <= 0.0 ) {
//          dc = fabs( dot1 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
//          p1[0] = p2[0] + dc * ( p1[0] - p2[0] );
//          p1[1] = p2[1] + dc * ( p1[1] - p2[1] );
//          v1[0] = p1[0] - p2[0];
//          v1[1] = p1[1] - p2[1];
//          dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
////          printf( "\t* * * -> %d ( %7.3e, %7.3e ) Crossed a face! %d( %7.3e, %7.3e )\n",
////                  s+1, dot0, dot1, ++dumb, p1[0], p1[1] );
//        }
//        /* If first node is in and second node is out, calculate magnitude of
//         * vector from edge point at face to second edge node. Store in dc */
//        else if( dot0 <= 0.0 && dot1 > tol ) {
//          dc = fabs( dot0 ) / MAX( tol, fabs( dot0 ) + fabs( dot1 ) );
//          p2[0] = p1[0] + dc * ( p2[0] - p1[0] );
//          p2[1] = p1[1] + dc * ( p2[1] - p1[1] );
//          v1[0] = p2[0] - p1[0];
//          v1[1] = p2[1] - p1[1];
//          dc = sqrt( v1[0]*v1[0] + v1[1]*v1[1] );
////          printf( "\t* * * -> %d ( %7.3e, %7.3e ) Crossed a face! %d( %7.3e, %7.3e )\n",
////                  s+1, dot0, dot1, ++dumb, p2[0], p2[1] );
//        }
//
//        //if( dumb == 3 ) {
//        //  printf( "\tRETRIEVE EDGE SCALAR: DEBUGGING\n" );
//        //  printf( "\t\t%7.3e %7.3e\n", ptr->vert[0][0], ptr->vert[0][1] );
//        //  printf( "\t\t%7.3e %7.3e\n", ptr->vert[1][0], ptr->vert[1][1] );
//        //}
//      }
//      break;
//  }

#else

  double clip[2][3], v1[3], v2[3], v3[3];

  // set clipped coordinates to original points
  clip[0][0]=p1[0];
  clip[0][1]=p1[1];
  clip[0][2]=p1[2];
  clip[1][0]=p2[0];
  clip[1][1]=p2[1];
  clip[1][2]=p2[2];
  v1[0] = clip[0][0]-clip[1][0];
  v1[1] = clip[0][1]-clip[1][1];
  v1[2] = clip[0][2]-clip[1][2];
  /* Both ds and dc set to full edge length */
  ds = dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);

  /* Find element type according to number of vertices */
  switch (ptr->nv) {
    // NOTE: element logic assumes the faces are planar
    /* Go through all vertices */
    /* Conditions for part of edge to be inside of element have to happen
     * ACROSS ALL FACES */
    case 4: // Tetrahedron element
      for (s=0; s < 4 && dc > tol; s++) {
        /* Get three consecutive nodes on a face */
        switch(s) {
          case 0: i0 = 0; i1 = 2; j0 = 0; j1 = 1; break;
          case 1: i0 = 0; i1 = 1; j0 = 0; j1 = 3; break;
          case 2: i0 = 1; i1 = 2; j0 = 1; j1 = 3; break;
          case 3: i0 = 2; i1 = 0; j0 = 2; j1 = 3; break;
        }
        /* Calculate normal vector to face */
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        /* Dot product between face vector and vectors from middle node to
         * nodes of edge */
        v1[0] = clip[0][0] - ptr->vert[i0][0];
        v1[1] = clip[0][1] - ptr->vert[i0][1];
        v1[2] = clip[0][2] - ptr->vert[i0][2];
        dot0 = vector_dot_product(v1,v3);
        v1[0] = clip[1][0] - ptr->vert[i0][0];
        v1[1] = clip[1][1] - ptr->vert[i0][1];
        v1[2] = clip[1][2] - ptr->vert[i0][2];
        dot1 = vector_dot_product(v1,v3);
        /* The following algorithm considers only the case of one dot
         * product positive and one dot product negative ( edge crosses that
         * face ), and the case of both dot products positive in which dc is
         * set to zero ( edge has no portion inside element ). The case of
         * both dot products negative is not considered since the edge would
         * later be treated in the case when it intersects another face (
         * edge does not cross current face ). */

        /* If both nodes of edge are out of element ( both dot products
         * greater than zero ), set dc to zero ( no portion of edge in
         * element ) */
        if ((dot0 > -tol && dot1 > tol) ||
            (dot0 > tol && dot1 > -tol)) {
          dc = 0.0;
        }
        /* If first node is out and second node is in, calculate magnitude of
         * vector from edge point at face to first edge node. Store in dc */
        else if (dot0 > tol && dot1 <= 0.0) {
          dc = fabs(dot1)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[0][0] = clip[1][0] + dc*(clip[0][0]-clip[1][0]);
          clip[0][1] = clip[1][1] + dc*(clip[0][1]-clip[1][1]);
          clip[0][2] = clip[1][2] + dc*(clip[0][2]-clip[1][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        }
        /* If first node is in and second node is out, calculate magnitude of
         * vector from edge point at face to second edge node. Store in dc */
        else if (dot0 <= 0.0 && dot1 > tol) {
          dc = fabs(dot0)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[1][0] = clip[0][0] + dc*(clip[1][0]-clip[0][0]);
          clip[1][1] = clip[0][1] + dc*(clip[1][1]-clip[0][1]);
          clip[1][2] = clip[0][2] + dc*(clip[1][2]-clip[0][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        }
      }
      break;
    case 5: // Pyramid element
      for (s=0; s < 5 && dc > tol; s++)
      {
        switch(s)
        {
          case 0: i0 = 0; i1 = 2; j0 = 3; j1 = 1; break;
          case 1: i0 = 0; i1 = 1; j0 = 0; j1 = 4; break;
          case 2: i0 = 1; i1 = 2; j0 = 1; j1 = 4; break;
          case 3: i0 = 2; i1 = 3; j0 = 2; j1 = 4; break;
          case 4: i0 = 3; i1 = 0; j0 = 3; j1 = 4; break;
        }
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        v1[0] = clip[0][0]-ptr->vert[i0][0];
        v1[1] = clip[0][1]-ptr->vert[i0][1];
        v1[2] = clip[0][2]-ptr->vert[i0][2];
        dot0 = vector_dot_product(v1,v3);
        v1[0] = clip[1][0]-ptr->vert[i0][0];
        v1[1] = clip[1][1]-ptr->vert[i0][1];
        v1[2] = clip[1][2]-ptr->vert[i0][2];
        dot1 = vector_dot_product(v1,v3);
        /* The following algorithm considers only the case of one dot
         * product positive and one dot product negative ( edge crosses that
         * face ), and the case of both dot products positive in which dc is
         * set to zero ( edge has no portion inside element ). The case of
         * both dot products negative is not considered since the edge would
         * later be treated in the case when it intersects another face (
         * edge does not cross current face ). */

        if ((dot0 > -tol && dot1 > tol) ||
            (dot0 > tol && dot1 > -tol)) {
          dc = 0.0;
        }
        else if (dot0 > tol && dot1 <= 0.0)
        {
          dc = fabs(dot1)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[0][0] = clip[1][0] + dc*(clip[0][0]-clip[1][0]);
          clip[0][1] = clip[1][1] + dc*(clip[0][1]-clip[1][1]);
          clip[0][2] = clip[1][2] + dc*(clip[0][2]-clip[1][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        } else if (dot0 <= 0.0 && dot1 > tol)
        {
          dc = fabs(dot0)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[1][0] = clip[0][0] + dc*(clip[1][0]-clip[0][0]);
          clip[1][1] = clip[0][1] + dc*(clip[1][1]-clip[0][1]);
          clip[1][2] = clip[0][2] + dc*(clip[1][2]-clip[0][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        }
      }
      break;
    case 6: // Prism element
      for (s=0; s < 5 && dc > tol; s++)
      {
        switch(s)
        {
          case 0: i0 = 0; i1 = 4; j0 = 1; j1 = 3; break;
          case 1: i0 = 1; i1 = 5; j0 = 2; j1 = 4; break;
          case 2: i0 = 2; i1 = 3; j0 = 0; j1 = 5; break;
          case 3: i0 = 0; i1 = 2; j0 = 0; j1 = 1; break;
          case 4: i0 = 3; i1 = 4; j0 = 3; j1 = 5; break;
        }
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        v1[0] = clip[0][0]-ptr->vert[i0][0];
        v1[1] = clip[0][1]-ptr->vert[i0][1];
        v1[2] = clip[0][2]-ptr->vert[i0][2];
        dot0 = vector_dot_product(v1,v3);
        v1[0] = clip[1][0]-ptr->vert[i0][0];
        v1[1] = clip[1][1]-ptr->vert[i0][1];
        v1[2] = clip[1][2]-ptr->vert[i0][2];
        dot1 = vector_dot_product(v1,v3);
        /* The following algorithm considers only the case of one dot
         * product positive and one dot product negative ( edge crosses that
         * face ), and the case of both dot products positive in which dc is
         * set to zero ( edge has no portion inside element ). The case of
         * both dot products negative is not considered since the edge would
         * later be treated in the case when it intersects another face (
         * edge does not cross current face ). */

        if ((dot0 > -tol && dot1 > tol) ||
            (dot0 > tol && dot1 > -tol))
        {
          dc = 0.0;
        } else if (dot0 > tol && dot1 <= 0.0)
        {
          dc = fabs(dot1)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[0][0] = clip[1][0] + dc*(clip[0][0]-clip[1][0]);
          clip[0][1] = clip[1][1] + dc*(clip[0][1]-clip[1][1]);
          clip[0][2] = clip[1][2] + dc*(clip[0][2]-clip[1][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        } else if (dot0 <= 0.0 && dot1 > tol)
        {
          dc = fabs(dot0)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[1][0] = clip[0][0] + dc*(clip[1][0]-clip[0][0]);
          clip[1][1] = clip[0][1] + dc*(clip[1][1]-clip[0][1]);
          clip[1][2] = clip[0][2] + dc*(clip[1][2]-clip[0][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        }
      }
      break;
    case 8: // Hex element
      for (s=0; s < 6 && dc > tol; s++)
      {
        switch(s)
        {
          case 0: i0 = 0; i1 = 2; j0 = 3; j1 = 1; break;
          case 1: i0 = 0; i1 = 5; j0 = 1; j1 = 4; break;
          case 2: i0 = 1; i1 = 6; j0 = 2; j1 = 5; break;
          case 3: i0 = 2; i1 = 7; j0 = 3; j1 = 6; break;
          case 4: i0 = 3; i1 = 4; j0 = 0; j1 = 7; break;
          case 5: i0 = 4; i1 = 6; j0 = 5; j1 = 7; break;
        }
        v1[0] = ptr->vert[i1][0] - ptr->vert[i0][0];
        v1[1] = ptr->vert[i1][1] - ptr->vert[i0][1];
        v1[2] = ptr->vert[i1][2] - ptr->vert[i0][2];
        v2[0] = ptr->vert[j1][0] - ptr->vert[j0][0];
        v2[1] = ptr->vert[j1][1] - ptr->vert[j0][1];
        v2[2] = ptr->vert[j1][2] - ptr->vert[j0][2];
        vector_cross_product(v1,v2,v3);
        vector_normalize(v3);
        v1[0] = clip[0][0]-ptr->vert[i0][0];
        v1[1] = clip[0][1]-ptr->vert[i0][1];
        v1[2] = clip[0][2]-ptr->vert[i0][2];
        dot0 = vector_dot_product(v1,v3);
        v1[0] = clip[1][0]-ptr->vert[i0][0];
        v1[1] = clip[1][1]-ptr->vert[i0][1];
        v1[2] = clip[1][2]-ptr->vert[i0][2];
        dot1 = vector_dot_product(v1,v3);
        /* The following algorithm considers only the case of one dot
         * product positive and one dot product negative ( edge crosses that
         * face ), and the case of both dot products positive in which dc is
         * set to zero ( edge has no portion inside element ). The case of
         * both dot products negative is not considered since the edge would
         * later be treated in the case when it intersects another face (
         * edge does not cross current face ). */

        if ((dot0 > -tol && dot1 > tol) ||
            (dot0 > tol && dot1 > -tol))
        {
          dc = 0.0;
        } else if (dot0 > tol && dot1 <= 0.0)
        {
          dc = fabs(dot1)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[0][0] = clip[1][0] + dc*(clip[0][0]-clip[1][0]);
          clip[0][1] = clip[1][1] + dc*(clip[0][1]-clip[1][1]);
          clip[0][2] = clip[1][2] + dc*(clip[0][2]-clip[1][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        } else if (dot0 <= 0.0 && dot1 > tol)
        {
          dc = fabs(dot0)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[1][0] = clip[0][0] + dc*(clip[1][0]-clip[0][0]);
          clip[1][1] = clip[0][1] + dc*(clip[1][1]-clip[0][1]);
          clip[1][2] = clip[0][2] + dc*(clip[1][2]-clip[0][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        }
      }
      break;
      /* This default option is used when the Size_Obj only has the higher
       * and lower points ( cube ) i.e. node tensor */
    default:
      for (s=0; s < 6 && dc > tol; s++)
      {
        switch(s)
        {
          case 0:
            v3[0] = -1.0; v3[1] = 0.0; v3[2] = 0.0; i0 = 0; break;
          case 1:
            v3[0] = 1.0; v3[1] = 0.0; v3[2] = 0.0; i0 = 1; break;
          case 2:
            v3[0] = 0.0; v3[1] = -1.0; v3[2] = 0.0; i0 = 0; break;
          case 3:
            v3[0] = 0.0; v3[1] = 1.0; v3[2] = 0.0; i0 = 1; break;
          case 4:
            v3[0] = 0.0; v3[1] = 0.0; v3[2] = -1.0; i0 = 0; break;
          case 5:
            v3[0] = 0.0; v3[1] = 0.0; v3[2] = 1.0; i0 = 1; break;
        }
        v1[0] = clip[0][0]-ptr->vert[i0][0];
        v1[1] = clip[0][1]-ptr->vert[i0][1];
        v1[2] = clip[0][2]-ptr->vert[i0][2];
        dot0 = vector_dot_product(v1,v3);
        v1[0] = clip[1][0]-ptr->vert[i0][0];
        v1[1] = clip[1][1]-ptr->vert[i0][1];
        v1[2] = clip[1][2]-ptr->vert[i0][2];
        dot1 = vector_dot_product(v1,v3);
        /* The following algorithm considers only the case of one dot
         * product positive and one dot product negative ( edge crosses that
         * face ), and the case of both dot products positive in which dc is
         * set to zero ( edge has no portion inside element ). The case of
         * both dot products negative is not considered since the edge would
         * later be treated in the case when it intersects another face (
         * edge does not cross current face ). */

        if ((dot0 > -tol && dot1 > tol) ||
            (dot0 > tol && dot1 > -tol))
        {
          dc = 0.0;
        } else if (dot0 > tol && dot1 <= 0.0)
        {
          dc = fabs(dot1)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[0][0] = clip[1][0] + dc*(clip[0][0]-clip[1][0]);
          clip[0][1] = clip[1][1] + dc*(clip[0][1]-clip[1][1]);
          clip[0][2] = clip[1][2] + dc*(clip[0][2]-clip[1][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        } else if (dot0 <= 0.0 && dot1 > tol)
        {
          dc = fabs(dot0)/MAX(tol,fabs(dot0)+fabs(dot1));
          clip[1][0] = clip[0][0] + dc*(clip[1][0]-clip[0][0]);
          clip[1][1] = clip[0][1] + dc*(clip[1][1]-clip[0][1]);
          clip[1][2] = clip[0][2] + dc*(clip[1][2]-clip[0][2]);
          v1[0] = clip[0][0]-clip[1][0];
          v1[1] = clip[0][1]-clip[1][1];
          v1[2] = clip[0][2]-clip[1][2];
          dc = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
        }
      }
      break;
  }

#endif

  /* Fraction of edge inside element */
  dc /= MAX(tol,ds);

  return(dc);
}

/* Return number of items stored in system */
int Spacing_Obj::Number_of_Entries() {
  return(nsp);
}

/* Retrieve a scalar given a point */
double Spacing_Obj::Retrieve_Scalar(double pt[SD]) {

  double sp, slop[SD];
  int i;
  PList *slist;
  Size_Obj *ptr;

  /* If space root is NULL, exit */
  if (space_root == 0)
  {
    fprintf(stderr,"\nSpacing_Object: Root Octant not defined!");
    exit(0);
  }

  /* Create list of pointers */
  slist = new PList();

  // initialize spacing
  sp = global_space;

  /* Retrieving full list from all levels of tree */
  i = -1;
  slop[0]=slop[1]=tol;// no overlap used in Octree search
//  printf( "* * * tol %24.16e\n", tol );
#if SPACE == 3
  slop[2]=tol;// no overlap used in Octree search
#endif
  space_root->retrieve_list(pt,slop,i,slist);

  printf( "\tRETRIEVE SCALAR: Retrieved spaces in list: %d\n", slist->max );

  /* Now go, through retrieved list */
  for (i=0; i < slist->max; i++)
  {
    ptr = (Size_Obj*)slist->list[i];

    /* If point is not in list, continue */
    if (!Point_In_Element(pt,ptr)) {
//      printf( "\tRETRIEVE SCALAR: Continuing!\n" );
      continue;
    }

    /* If sdim is 1 (scalar) */
    if (ptr->sdim == 1)
    {
      // scalar stored in object
      sp = MIN(sp,ptr->rmt[0]);
    }
    else// if sdim is not 1 (not a scalar)
    {
      // tensor stored, extract minimum spacing in principal directions
      double rmt[SD][SD], left[SD][SD], right[SD][SD], lam[SD];
      Full_Tensor(ptr->rmt,rmt);

      Decompose_Tensor(rmt,left,right,lam);

      sp = MIN(sp,sqrt(1.0/lam[0]));
      sp = MIN(sp,sqrt(1.0/lam[1]));
#if SPACE == 3
      sp = MIN(sp,sqrt(1.0/lam[2]));
#endif
    }
  }

  delete slist;

  return(sp);
}

/* Retrieve a scalar given an edge */
double Spacing_Obj::Retrieve_Edge_Scalar(double p1[SD], double p2[SD]) {

  double sp, mid[SD], lo[SD], hi[SD], slop[SD], v1[SD], v2[SD], v3[SD];
  double dc, wgt, clip[2][SD];
  int i, j, k;
  PList *slist;
  Size_Obj *ptr;

  /* If space root NULL, exit */
  if (space_root == 0)
  {
    fprintf(stderr,"\nSpacing_Object: Root Octant not defined!");
    exit(0);
  }

  // initialize spacing
  sp = 0.0;
  wgt = 0.0;

  slist = new PList();

  /* Retrieving full list from all levels of tree */
  i = -1;
  // Compute edge mid-point
  // Compute slop based on edge half-length
  mid[0] = (p1[0]+p2[0])*0.5;
  mid[1] = (p1[1]+p2[1])*0.5;
  slop[0]=fabs(p1[0]-p2[0])*0.5 + tol;
  slop[1]=fabs(p1[1]-p2[1])*0.5 + tol;
#if SPACE == 3
  mid[2] = (p1[2]+p2[2])*0.5;
  slop[2]=fabs(p1[2]-p2[2])*0.5 + tol;
#endif
  space_root->retrieve_list(mid,slop,i,slist);
  printf( "\tRETRIEVE EDGE SCALAR: Retrieved spaces in list: %d\n",
          slist->max );
  for (i=0; i < slist->max; i++) {

    ptr = (Size_Obj*)slist->list[i];

    /* quick extent test */
    lo[0] = lo[1] =  1.0e20;
    hi[0] = hi[1] = -1.0e20;
#if SPACE == 3
    lo[2] =  1.0e20;
    hi[2] = -1.0e20;
#endif
    for (j=0; j < ptr->nv; j++) {
      for (k=0; k < SD; k++) {
        lo[k] = MIN(lo[k],ptr->vert[j][k]);
        hi[k] = MAX(hi[k],ptr->vert[j][k]);
      }
    }
    dc = Edge_In_Element(p1,p2,ptr);
//    printf( "\tRETRIEVE EDGE SCALAR: dc = %7.3e\n", dc );

    /* If no portion of edge in element (dc = 0) skip */
    if (dc < tol) {
      printf( "\tRETRIEVE EDGE SCALAR: Continuing! ( dc = %7.3e )\n", dc );
      continue;
    }

    /* if sdim is 1 (scalar) */
    if (ptr->sdim == 1) {
      // scalar stored in object
      sp += ptr->rmt[0]*dc;
    }
    else {
      // tensor stored, extract minimum spacing in principal directions
      double rmt[SD][SD], left[SD][SD], right[SD][SD], lam[SD];
      Full_Tensor(ptr->rmt,rmt);

      Decompose_Tensor(rmt,left,right,lam);

#if SPACE == 2
      sp += MIN( sqrt( 1.0 / lam[0] ), sqrt( 1.0 / lam[1] ) ) * dc;
#else
      sp += MIN( sqrt( 1.0 / lam[0] ), MIN( sqrt( 1.0 / lam[1] ), sqrt( 1.0 / lam[2] ) ) ) * dc;
#endif
    }
    wgt += dc;
  }

  if (wgt < tol) {
    printf( "\tRETRIEVE EDGE SCALAR: Using global space. Weight = %7.3e\n",
            wgt );
    sp = global_space;
  }
  else
    printf( "\tRETRIEVE EDGE SCALAR: sp = %7.3e, wgt = %7.3e\n",
            sp, wgt );
    sp /= wgt;

  delete slist;

  return(sp);
}

/* Make scalar into uniform tensor */
void Spacing_Obj::Scalar_To_Tensor(double sp, double rmt[SD][SD])
{
  double tsp = 1.0/sp/sp;
  for (int i=0; i < SD; i++)
    for (int j=0; j < SD; j++)
      rmt[i][j] = (i == j) ? tsp : 0.0;
  return;
}

/* Expand compressed tensor to full matrix */
void Spacing_Obj::Full_Tensor(double sp[], double rmt[SD][SD])
{
#if SPACE == 2
  rmt[0][0] = sp[0];
  rmt[0][1] = sp[1];
  rmt[1][0] = sp[1];
  rmt[1][1] = sp[2];

#else
  rmt[0][0] = sp[0];
  rmt[0][1] = sp[1];
  rmt[0][2] = sp[2];
  rmt[1][0] = sp[1];
  rmt[1][1] = sp[3];
  rmt[1][2] = sp[4];
  rmt[2][0] = sp[2];
  rmt[2][1] = sp[4];
  rmt[2][2] = sp[5];
#endif
}

/* Compare tensors */
double Spacing_Obj::Compare_Tensors(double t1[SD][SD], double t2[SD][SD]) {

  double t1left[SD][SD], t1right[SD][SD], t1lam[SD];
  double t2left[SD][SD], t2right[SD][SD], t2lam[SD];
  double v1[SD], v2[SD], v3[SD], d1, d2, d3, h1, h2, h3;
  double percent = 0.0;

//  printf( "Decompose Tensor\n" );
//  printf( "t1:%24.15e%24.15e\n", t1[0][0], t1[0][1] );
//  printf( "t1:%24.15e%24.15e\n", t1[1][0], t1[1][1] );
  Decompose_Tensor(t1,t1left,t1right,t1lam);
//  printf( "t2:%24.15e%24.15e\n", t2[0][0], t2[0][1] );
//  printf( "t2:%24.15e%24.15e\n", t2[1][0], t2[1][1] );
  Decompose_Tensor(t2,t2left,t2right,t2lam);

  percent = 0.0;

  // use tensor 1 and test against tensor 2 in principal directions
  v1[0] = t1left[0][0];
  v1[1] = t1left[1][0];
  v2[0] = t1left[0][1];
  v2[1] = t1left[1][1];
  h1 = sqrt(1.0/t1lam[0]);
  h2 = sqrt(1.0/t1lam[1]);
  d1 = 1.0/Metric_Length(v1,t2);
  d2 = 1.0/Metric_Length(v2,t2);
  percent = MAX(percent,fabs(h1-d1)/h1);
  percent = MAX(percent,fabs(h2-d2)/h2);
#if SPACE == 3
  v1[2] = t1left[2][0];
  v2[2] = t1left[2][1];
  v3[0] = t1left[0][2];
  v3[1] = t1left[1][2];
  v3[2] = t1left[2][2];
  h3 = sqrt(1.0/t1lam[2]);
  d3 = 1.0/Metric_Length(v3,t2);
  percent = MAX(percent,fabs(h3-d3)/h3);
#endif

  // use tensor 2 and test against tensor 1 in principal directions
  v1[0] = t2left[0][0];
  v1[1] = t2left[1][0];
  v2[0] = t2left[0][1];
  v2[1] = t2left[1][1];
  h1 = sqrt(1.0/t2lam[0]);
  h2 = sqrt(1.0/t2lam[1]);
  d1 = 1.0/Metric_Length(v1,t1);
  d2 = 1.0/Metric_Length(v2,t1);
  percent = MAX(percent,fabs(h1-d1)/h1);
  percent = MAX(percent,fabs(h2-d2)/h2);
#if SPACE == 3
  v1[2] = t2left[2][0];
  v2[2] = t2left[2][1];
  v3[0] = t2left[0][2];
  v3[1] = t2left[1][2];
  v3[2] = t2left[2][2];
  h3 = sqrt(1.0/t2lam[2]);
  d3 = 1.0/Metric_Length(v3,t1);
  percent = MAX(percent,fabs(h3-d3)/h3);
#endif

  return(percent);
}

/* Combine tensors */
bool Spacing_Obj::Combine_Tensors(double t1[SD][SD], double t2[SD][SD], double t[SD][SD]) {// * * * * * * *

  int i, j;
  double t1left[SD][SD], t1right[SD][SD], t1lam[SD];
  double t2left[SD][SD], t2right[SD][SD], t2lam[SD];
  double rmin1, rmin2, v1[SD], v2[SD], v3[SD], d1, d2, d3, h1, h2, h3;
  bool flag = false;

  Decompose_Tensor(t1,t1left,t1right,t1lam);
  Decompose_Tensor(t2,t2left,t2right,t2lam);

  rmin1 = sqrt(1.0/t1lam[0]);
  rmin1 = MIN(rmin1,sqrt(1.0/t1lam[1]));
  rmin2= sqrt(1.0/t2lam[0]);
  rmin2= MIN(rmin2,sqrt(1.0/t2lam[1]));
#if SPACE == 3
  rmin1 = MIN(rmin1,sqrt(1.0/t1lam[2]));
  rmin2= MIN(rmin2,sqrt(1.0/t2lam[2]));
#endif
  if (rmin1 < rmin2) {

    // use tensor 1 and test against tensor 2 in principal directions
    v1[0] = t1left[0][0];
    v1[1] = t1left[1][0];
    v2[0] = t1left[0][1];
    v2[1] = t1left[1][1];
    h1 = sqrt(1.0/t1lam[0]);
    h2 = sqrt(1.0/t1lam[1]);
    d1 = 1.0/Metric_Length(v1,t2);
    d2 = 1.0/Metric_Length(v2,t2);
#if SPACE == 3
    v1[2] = t1left[2][0];
    v2[2] = t1left[2][1];
    v3[0] = t1left[0][2];
    v3[1] = t1left[1][2];
    v3[2] = t1left[2][2];
    h3 = sqrt(1.0/t1lam[2]);
    d3 = 1.0/Metric_Length(v3,t2);
#endif

    if (d1-h1 < -1.0e-12 || d2-h2 < -1.0e-12
#if SPACE == 3
        || d3-h3 < -1.0e-12
#endif
        ) {
      h1 = MIN(h1,d1);
      h2 = MIN(h2,d2);
#if SPACE == 3
      h3 = MIN(h3,d3);
#endif

      // recompute current tensor with new eigenvectors & modified eigenvalues
      Compute_Riemannian_Metric(v1, v2, v3, h1, h2, h3, t);
      flag = true;
    }
    else {
      for (j=0; j < SD; j++)
        for (i=0; i < SD; i++)
          t[i][j] = t1[i][j];
      flag = false;
    }
  }
  else {

    // use tensor 2 and test against tensor  1in principal directions
    v1[0] = t2left[0][0];
    v1[1] = t2left[1][0];
    v2[0] = t2left[0][1];
    v2[1] = t2left[1][1];
    h1 = sqrt(1.0/t2lam[0]);
    h2 = sqrt(1.0/t2lam[1]);
    d1 = 1.0/Metric_Length(v1,t1);
    d2 = 1.0/Metric_Length(v2,t1);
#if SPACE == 3
    v1[2] = t2left[2][0];
    v2[2] = t2left[2][1];
    v3[0] = t2left[0][2];
    v3[1] = t2left[1][2];
    v3[2] = t2left[2][2];
    h3 = sqrt(1.0/t2lam[2]);
    d3 = 1.0/Metric_Length(v3,t1);
#endif

    if (d1 < h1 || d2 < h2
#if SPACE == 3
        || d3 < h3
#endif
        ) {
      h1 = MIN(h1,d1);
      h2 = MIN(h2,d2);
#if SPACE == 3
      h3 = MIN(h3,d3);
#endif

      // recompute current tensor with modified eigenvalues
      Compute_Riemannian_Metric(v1, v2, v3, h1, h2, h3, t);
      flag = true;
    }
    else {
      for (j=0; j < SD; j++)
        for (i=0; i < SD; i++)
          t[i][j] = t2[i][j];
      flag = false;
    }
  }
  return flag;
}

/* Retrieve a tensor given a point */
int Spacing_Obj::Retrieve_Tensor(double pt[SD], double sp[SD][SD]) {

  int i, j, k, s, i0, i1, j0, j1;
  double h1, h2, h3, slop[SD], v1[SD], v2[SD], v3[SD], lo[SD], hi[SD];
  double rmt[SD][SD];
  double tsp, spleft[SD][SD], spright[SD][SD], splam[SD];
  PList *slist;
  Size_Obj *ptr;

  if (space_root == 0)
  {
    fprintf(stderr,"\nSpacing_Object: Root Octant not defined!");
    exit(0);
  }

  slist = new PList();

  // initialize tensor
  tsp = 1.0/global_space/global_space;
  for (i=0; i < SD; i++)
    for (j=0; j < SD; j++)
      sp[i][j] = (i == j) ? tsp : 0.0;

  Decompose_Tensor(sp,spleft,spright,splam);

  i = -1;  // Retrieving full list from all levels of tree
  slop[0]=slop[1]==tol; // no overlap used in Octree search
#if SPACE == 3
  slop[2]=tol; // no overlap used in Octree search
#endif
  space_root->retrieve_list(pt,slop,i,slist);
//  printf( "\tRETRIEVE TENSOR: Retrieved spaces in list: %d\n", slist->max );
  for (i=0; i < slist->max; i++)
  {
    ptr = (Size_Obj*)slist->list[i];

//    printf( "\tRETRIEVE TENSOR: DEBUGGING\n" );
//    printf( "\t\t%7.3e %7.3e\n", ptr->vert[0][0], ptr->vert[0][1] );
//    printf( "\t\t%7.3e %7.3e\n", ptr->vert[1][0], ptr->vert[1][1] );
    if (!Point_In_Element(pt,ptr)) {
//      printf( "RETRIEVE TENSOR: Point not found in element. Continuing!\n" );
      continue;
    }

    if (ptr->sdim == 1)
    {
      // scalar stored in object
      tsp = ptr->rmt[0];

      h1 = sqrt(1.0/splam[0]);
      h2 = sqrt(1.0/splam[1]);
#if SPACE == 3
      h3 = sqrt(1.0/splam[2]);
#endif
      // test size against current eigenvalues
#if SPACE == 2
      if (tsp < h1 || tsp < h2)
#else
      if (tsp < h1 || tsp < h2 || tsp < h3)
#endif
      {
        v1[0] = spleft[0][0];
        v1[1] = spleft[1][0];
        v2[0] = spleft[0][1];
        v2[1] = spleft[1][1];
        h1 = MIN(h1,tsp);
        h2 = MIN(h2,tsp);
#if SPACE == 3
        v1[2] = spleft[2][0];
        v2[2] = spleft[2][1];
        v3[0] = spleft[0][2];
        v3[1] = spleft[1][2];
        v3[2] = spleft[2][2];
        h3 = MIN(h3,tsp);
#endif

        // recompute current tensor with modified eigenvalues
        Compute_Riemannian_Metric(v1, v2, v3, h1, h2, h3, sp);

        // replace eigenvalues
        splam[0] = 1.0/h1/h1;
        splam[1] = 1.0/h2/h2;
#if SPACE == 3
        splam[2] = 1.0/h3/h3;
#endif
      }
    } else
    {
      // tensor stored
      Full_Tensor(ptr->rmt,rmt);

      if (Combine_Tensors(sp,rmt,sp))
        Decompose_Tensor(sp,spleft,spright,splam);
    }
  }

  delete slist;

  return(0);
}

/* Metric length for given edge */
double Spacing_Obj::Compute_Edge_Metric_Length(double p1[SD], double p2[SD], int type) {

  int i, j, k, s, i0, i1, j0, j1;
  double slop[SD], mid[SD], v1[SD], v2[SD], vSD[SD], pt[SD];
  double ML, dc, ds, dot0, dot1, wgt, rmt[SD][SD], clip[2][SD];
  PList *slist;
  Size_Obj *ptr;

  if (space_root == 0)
  {
    fprintf(stderr,"\nSpacing_Object: Root Octant not defined!");
    exit(0);
  }

  // compute length of edge
  v1[0] = p2[0]-p1[0];
  v1[1] = p2[1]-p1[1];
#if SPACE == 2
  ds = sqrt(v1[0]*v1[0] + v1[1]*v1[1]);
#else
  v1[2] = p2[2]-p1[2];
  ds = sqrt(v1[0]*v1[0] + v1[1]*v1[1] + v1[2]*v1[2]);
#endif

  ML = 0.0;
  wgt = 0.0;

  slist = new PList();

  i = -1;  // Retrieving full list from all levels of tree
  // Compute edge mid-point
  mid[0] = (p1[0]+p2[0])*0.5;
  mid[1] = (p1[1]+p2[1])*0.5;
  // Compute slop based on edge half-length
  slop[0]=fabs(p1[0]-p2[0])*0.5 + tol;
  slop[1]=fabs(p1[1]-p2[1])*0.5 + tol;
#if SPACE == 3
  slop[2]=fabs(p1[2]-p2[2])*0.5 + tol;
  mid[2] = (p1[2]+p2[2])*0.5;
#endif
  space_root->retrieve_list(mid,slop,i,slist);
  printf( "\tCOMPUTE EDGE METRIC LENGTH: Retrieved spaces in list: %d\n", slist->max );
  for (i=0; i < slist->max; i++)
  {
    ptr = (Size_Obj*)slist->list[i];

    dc = Edge_In_Element(p1,p2,ptr);

    if (dc < tol) {
      printf( "\tCOMPUTE EDGE METRIC LENGTH: dc = %7.3e. Continuing!\n", dc );
      continue;
    }

    if (ptr->sdim == 1)
    {
      // scalar stored in object
      if (type == 0)
        ML = MAX(ML,ds/ptr->rmt[0]);
      else
        ML += ds/ptr->rmt[0]*dc;
    } else
    {
      // tensor stored
      Full_Tensor(ptr->rmt,rmt);

      v1[0] = p2[0]-p1[0];
      v1[1] = p2[1]-p1[1];
#if SPACE == 3
      v1[2] = p2[2]-p1[2];
#endif
      if (type == 0)
        ML = MAX(ML,Metric_Length(v1,rmt));
      else
        ML += Metric_Length(v1,rmt)*dc;
    }
    wgt += dc;
  }

//  printf( "\tCOMPUTE EDGE METRIC LENGTH: Previous metric length %7.3e\n",
//          ML );
  if (wgt < tol)
    ML = ds/global_space;
  else if (type > 0)
    ML /= MAX(tol,wgt);

  delete slist;

  return(ML);

}

/* Retrieve a tensor given an edge */
int Spacing_Obj::Retrieve_Edge_Tensor(double p1[SD], double p2[SD], double sp[SD][SD]) {

  int i, j, k, s, i0, i1, j0, j1;
  double h1, h2, h3, slop[SD], v1[SD], v2[SD], v3[SD], mid[SD];
  double dc, rmt[SD][SD];
  double tsp, spleft[SD][SD], spright[SD][SD], splam[SD];
  PList *slist;
  Size_Obj *ptr;

  if (space_root == 0)
  {
    fprintf(stderr,"\nSpacing_Object: Root Octant not defined!");
    exit(0);
  }

  slist = new PList();

  // initialize tensor
  tsp = 1.0/global_space/global_space;
  for (i=0; i < SD; i++)
    for (j=0; j < SD; j++)
      sp[i][j] = (i == j) ? tsp : 0.0;

  Decompose_Tensor(sp,spleft,spright,splam);

  i = -1;  // Retrieving full list from all levels of tree
  // Compute edge mid-point
  mid[0] = (p1[0]+p2[0])*0.5;
  mid[1] = (p1[1]+p2[1])*0.5;
  // Compute slop based on edge half-length
  slop[0]=fabs(p1[0]-p2[0])*0.5 + tol;
  slop[1]=fabs(p1[1]-p2[1])*0.5 + tol;
#if SPACE == 3
  mid[2] = (p1[2]+p2[2])*0.5;
  slop[2]=fabs(p1[2]-p2[2])*0.5 + tol;
#endif
  space_root->retrieve_list(mid,slop,i,slist);
  printf( "\tRETRIEVE EDGE TENSOR: Retrieved spaces in list: %d\n", slist->max );
  for (i=0; i < slist->max; i++)
  {
    ptr = (Size_Obj*)slist->list[i];

    dc = Edge_In_Element(p1,p2,ptr);

    if (dc < tol)
      continue;

    if (ptr->sdim == 1)
    {
      // scalar stored in object
      tsp = ptr->rmt[0];

      h1 = sqrt(1.0/splam[0]);
      h2 = sqrt(1.0/splam[1]);
#if SPACE == 3
      h3 = sqrt(1.0/splam[2]);
#endif
      // test size against current eigenvalues
#if SPACE == 2
      if (tsp < h1 || tsp < h2)
#else
      if (tsp < h1 || tsp < h2 || tsp < h3)
#endif
      {
        v1[0] = spleft[0][0];
        v1[1] = spleft[1][0];
        v2[0] = spleft[0][1];
        v2[1] = spleft[1][1];
        h1 = MIN(h1,tsp);
        h2 = MIN(h2,tsp);
#if SPACE == 3
        v1[2] = spleft[2][0];
        v2[2] = spleft[2][1];
        v3[0] = spleft[0][2];
        v3[1] = spleft[1][2];
        v3[2] = spleft[2][2];
        h3 = MIN(h3,tsp);
#endif

        // recompute current tensor with modified eigenvalues
        Compute_Riemannian_Metric(v1, v2, v3, h1, h2, h3, sp);

        // replace eigenvalues
        splam[0] = 1.0/h1/h1;
        splam[1] = 1.0/h2/h2;
#if SPACE == 3
        splam[2] = 1.0/h3/h3;
#endif
      }
    } else
    {
      // tensor stored
      Full_Tensor(ptr->rmt,rmt);

      if (Combine_Tensors(sp,rmt,sp))
        Decompose_Tensor(sp,spleft,spright,splam);
    }
  }

  delete slist;

  return(0);
}

#if SPACE == 3
/* Populate the 3D storage space */
int Spacing_Obj::Populate_Octree() {

  int i, j, k;
  void* *ptr;
  double (*lo)[3], (*hi)[3];

  ptr = new void*[nsp];
  if (ptr == 0)
  {
    fprintf(stderr,"\nPopulate_Octree: Unable to allocate ptr array.");
    fflush(stderr);
    exit(0);
  }
  lo = new double[nsp][3];
  if (lo == 0)
  {
    fprintf(stderr,"\nPopulate_Octree: Unable to allocate lo array.");
    fflush(stderr);
    exit(0);
  }
  hi = new double[nsp][3];
  if (hi == 0)
  {
    fprintf(stderr,"\nPopulate_Octree: Unable to allocate hi array.");
    fflush(stderr);
    exit(0);
  }
  for (i=0; i < nsp; i++)
  {
    //space_root->Store_In_Octree((void*)&space[i],space[i].lo,space[i].hi);
    ptr[i] = (void*)&space[i];
    lo[i][0] = lo[i][1] = lo[i][2] = 1.0e20;
    hi[i][0] = hi[i][1] = hi[i][2] = -1.0e20;
    for (j=0; j < space[i].nv; j++)
    {
      for (k=0; k < 3; k++)
      {
        lo[i][k] = MIN(lo[i][k],space[i].vert[j][k]);
        hi[i][k] = MAX(hi[i][k],space[i].vert[j][k]);
      }
    }
  }

  space_root->Store_In_Octree(nsp,ptr,lo,hi);

  //max_level = space_root->max_level(1);

  delete[] ptr;
  delete[] lo;
  delete[] hi;
  
  return(0);
}
#endif

/* Populate the 2D storage space */
int Spacing_Obj::Populate_Quadtree() {

  int i, j, k;
  void* *ptr;
  double (*lo)[2], (*hi)[2];

  ptr = new void*[nsp];
  if (ptr == 0)
  {
    fprintf(stderr,"\nPopulate_Quadtree: Unable to allocate ptr array.");
    fflush(stderr);
    exit(0);
  }
  lo = new double[nsp][2];
  if (lo == 0)
  {
    fprintf(stderr,"\nPopulate_Quadtree: Unable to allocate lo array.");
    fflush(stderr);
    exit(0);
  }
  hi = new double[nsp][2];
  if (hi == 0)
  {
    fprintf(stderr,"\nPopulate_Quadtree: Unable to allocate hi array.");
    fflush(stderr);
    exit(0);
  }
  for (i=0; i < nsp; i++)
  {
    //space_root->Store_In_Octree((void*)&space[i],space[i].lo,space[i].hi);
    ptr[i] = (void*)&space[i];
    lo[i][0] = lo[i][1] = 1.0e20;
    hi[i][0] = hi[i][1] = -1.0e20;
    for (j=0; j < space[i].nv; j++)
    {
      for (k=0; k < 2; k++)
      {
        lo[i][k] = MIN(lo[i][k],space[i].vert[j][k]);
        hi[i][k] = MAX(hi[i][k],space[i].vert[j][k]);
      }
    }
  }

  space_root->Store_In_Quadtree(nsp,ptr,lo,hi);
//  printf( "Segfaulting\n" );

  //max_level = space_root->max_level(1);

  delete[] ptr;
  delete[] lo;
  delete[] hi;
  
  return(0);
}

void Print_OF() {
  printf( "Opt. Field:  " );
}

void Spacing_Obj::Optimize_Field( int mode,
                                  int * t_nCells,
                                  int ** t_c2n_count,
                                  int *** t_cell_to_node,
                                  int * t_nNodes,
                                  double ** t_x,
                                  double ** t_y,
                                  double mergeThresh,
                                  double ** t_mi_matriz,
                                  Spacing_Obj ** t_a_t ) {

  int i, j, k, n, a, b, check;
  int maxN2C = 0;
  FILE * test = NULL;

  /* Local pointers */
  int nCells, nNodes, * c2n_count, ** cell_to_node;
  double * x, * y, * mi_matriz;
  Spacing_Obj * a_t;
  x = *t_x;
  y = *t_y;
  nCells = *t_nCells;
  nNodes = *t_nNodes;
  mi_matriz = *t_mi_matriz;
  c2n_count = *t_c2n_count;
  cell_to_node = *t_cell_to_node;
  a_t = *t_a_t;

  /* Generating node to cell connectivity */
  /* Taking count */
  int * n2c_count, ** node_to_cell;
  n2c_count = ( int* ) calloc ( nNodes, sizeof( int ) );
  for( i = 0; i < nCells; i++ ) {
    maxN2C = MAX( maxN2C, c2n_count[i] );
    for( j = 0; j < c2n_count[i]; j++ ) {
      n2c_count[cell_to_node[i][j]]++;
    }
  }
  /* Allocating memory, setting count to zero */
  node_to_cell = ( int** ) calloc ( nNodes, sizeof( int* ) );
  for( i = 0; i < nNodes; i++ ) {
    node_to_cell[i] = ( int* ) malloc ( n2c_count[i] * sizeof( int ) );
    n2c_count[i] = 0;
  }
  /* Getting cell into list for nodes */
  for( i = 0; i < nCells; i++ ) {
    for( j = 0; j < c2n_count[i]; j++ ) {
      n = cell_to_node[i][j];
      node_to_cell[n][n2c_count[n]] = i;
      n2c_count[n]++;
    }
  }

  /* Generating cell to cell connectivity */
  /* Assigning memory assuming cell to cell count equal to cell to node count */
  int * c2c_count, ** cell_to_cell;
  bool * boundary;
  boundary     = ( bool* ) calloc ( nNodes, sizeof( bool ) );
  c2c_count    = ( int*  ) calloc ( nCells, sizeof( int  ) );
  cell_to_cell = ( int** ) calloc ( nCells, sizeof( int* ) );
  for( i = 0; i < nCells; i++ ) {
    cell_to_cell[i] = ( int* ) malloc ( c2n_count[i] * sizeof( int ) );
  }
//  for( i = 0; i < nNodes; i++ ) {
//    boundary[i] = false;
//  }
  /* Getting cells into list for cells */
  for( i = 0; i < nCells; i++ ) {
    /* Going though nodes */
    for( j = 0; j < c2n_count[i]; j++ ) {
      /* Getting current and next nodes */
      a = cell_to_node[i][j];
      b = cell_to_node[i][(j+1)%c2n_count[i]];
      /* Marking side for later check */
      check = c2c_count[i];
      /* Going through cells connected to current node */
      for( k = 0; k < n2c_count[a]; k++ ) {
        /* Skipping current cell */
        if( node_to_cell[a][k] == i )  continue;
        /* Goint through cells connected to next node */
        for( n = 0; n < n2c_count[b]; n++ ) {
          /* Skipping current cell */
          if( node_to_cell[b][n] == i )  continue;
          /* If cell for current and next node match, add to list, increase
           * count, stop search */
          if( node_to_cell[a][k] == node_to_cell[b][n] ) {
            cell_to_cell[i][c2c_count[i]] = node_to_cell[a][k];
            c2c_count[i]++;
            break;
          }
        }
        if( c2c_count[i] != check )  break;
      }
      if( c2c_count[i] == check ) {
        boundary[a] = true;
        boundary[b] = true;
      }
    }
  }
  /* Checking out now, the funk so bravo */
//  test = fopen( "boundaries.out", "w" );
//  for( i = 0; i < nNodes; i++ ) {
//    if( boundary[i] )  fprintf( test, "b -> %d\n", i );
//  }
//  fclose( test );
  /*** Freeing memory ***/
//  free( n2c_count );
  /* Reallocating memory as necessary */
  for( i = 0; i < nCells; i++ ) {
    if( c2c_count[i] != c2n_count[i] ) {
      cell_to_cell[i] =
        ( int* ) realloc ( cell_to_cell[i], c2c_count[i] * sizeof( int ) );
    }
  }

  /* Finding a highest most right cell */
  /* Finding a lowest most left node */
  double least_x, least_y;
  least_x = least_y = 1.0e14;
  for( i = 0; i < nNodes; i++ ) {
    least_x = MIN( least_x, x[i] );
    least_y = MIN( least_y, y[i] );
  }
  /* Finding a highest most right node */
  double dx, dy, dist, farthest = 1.0e-14;
  int farNode;
  for( i = 0; i < nNodes; i++ ) {
    dx = x[i] - least_x;
    dy = y[i] - least_y;
    dist = farthest;
    farthest = MAX( farthest, sqrt( dx*dx + dy*dy ) );
    if( fabs( farthest - dist ) > 1.0e-14 ) {
      farNode = i;
    }
  }
  /* Getting cell */
  int farCell;
  farCell = node_to_cell[farNode][0];
  /*** Freeing memory ***/
//  for( i = 0; i < nNodes; i++ ) {
//    free( node_to_cell[i] );
//  }
//  free( node_to_cell );

  /* Generating new cell order */
  /* Necessary parameters */
  int * cell_mark, * new_ordering;
  int local_index[128], local_weight[128];
  int counter, seed, toOrder, currentCell;
  /* Assigning memory */
  cell_mark =    ( int* ) calloc ( nCells, sizeof( int ) );
  new_ordering = ( int* ) calloc ( nCells, sizeof( int ) );
  /* Initializing some parameters */
  counter = 0;
  new_ordering[counter] = farCell;
  cell_mark[farCell] = -1;
  counter++;
  /* Generating new array */
  for( i = 0; i < nCells; i++ ) {
    /* Setting internal parameters */
    seed = new_ordering[i];
    toOrder = 0;
    /* Making qeue of indices and weights, and marking cells */
    for( j = 0; j < c2c_count[seed]; j++ ) {
      currentCell = cell_to_cell[seed][j];
      if( cell_mark[currentCell] == -1 )  continue;
      local_index[toOrder] = currentCell;
      local_weight[toOrder] = c2c_count[currentCell];
      toOrder++;
      cell_mark[currentCell] = -1;
    }
    /* If only one cell in list, no sorting necessary */
    if( toOrder > 1 ) {
      Top_Ordering( toOrder, local_index, local_weight );
    }
    /* Adding sorted indices to old-to-new index map */
    for( j = 0; j < toOrder; j++ ) {
      new_ordering[counter] = local_index[j];
      counter++;
    }
    if( counter == nCells )  break;
  }
  /*** Freeing memory ***/
  free( cell_mark );

  /* Combining tensors */
  /* New parameters */
  int cell, neighbor, tag, newTag = 0;
  int * cell_tag, * region_count;
  int op1, op2, op3;
  int op1t, op2t, op3t;
  double rmt[2][2], rmt1[2][2], rmt2[2][2];
  double left[2][2], right[2][2], lam[2];
  double ** region_rmt;
  double dumb, h1, h2;
  region_count = ( int* ) calloc ( nCells,  sizeof( int ) );
  cell_tag     = ( int* ) malloc ( nCells * sizeof( int ) );
  region_rmt   = ( double** ) calloc ( nCells,  sizeof( double* ) );
  for( i = 0; i < nCells; i++ ) {
    region_rmt[i] = ( double* ) calloc ( 3, sizeof( double ) );
    cell_tag[i] = -1;
  }
  op1t = op2t = op3t = 0;
  /* Check file */
  if( ( test = fopen( "check.out", "w" ) ) == NULL ) {
    printf( "check.out file didn't open correctly!\n" );
    printf( "Exiting!\n" );
    exit( EXIT_FAILURE );
  }
  /* Generating regions of similar tensor */
  for( i = 0; i < nCells; i++ ) {
    /* Gettting first cell */
    cell = new_ordering[i];

    fprintf( test, "%5d->%5d(%d:", i, cell, c2c_count[cell] );
    op1 = op2 = op3 = 0;
    /* Going around cell's neighbors */
    for( j = 0; j < c2c_count[cell]; j++ ) {
      neighbor = cell_to_cell[cell][j];

      /* If both's tensors have been combined, skip */
      if( ( cell_tag[cell] == cell_tag[neighbor] ) && cell_tag[cell] != -1 )
        continue;

      /* Getting tensors from each */
      /* If cell hasn't been treated yet, get tensor on 2x2 array. If it holds a
       * scalar, turn it to tensor first */
      if( cell_tag[cell] == -1 ) {
        if( space[cell].sdim == 1 ) {
          dumb = space[cell].rmt[0];
          dumb = 1.0 / ( dumb*dumb );
          delete[] space[cell].rmt;
          space[cell].rmt = new double[3];
          space[cell].rmt[0] = space[cell].rmt[2] = dumb;
          space[cell].rmt[1] = 0.0;
        }
        Full_Tensor( space[cell].rmt, rmt1 );
      }
      else {
        tag = cell_tag[cell];
        Full_Tensor( region_rmt[tag], rmt1 );
      }
      /* If neighbor hasn't been treated yet, get tensor on 2x2 array. If it
       * holds a scalar, turn it to tensor first */
      if( cell_tag[neighbor] == -1 ) {
        if( space[neighbor].sdim == 1 ) {
          dumb = space[neighbor].rmt[0];
          dumb = 1.0 / ( dumb*dumb );
          delete[] space[neighbor].rmt;
          space[neighbor].rmt = new double[3];
          space[neighbor].rmt[0] = space[neighbor].rmt[2] = dumb;
          space[neighbor].rmt[1] = 0.0;
        }
        Full_Tensor( space[neighbor].rmt, rmt2 );
      }
      else {
        tag = cell_tag[neighbor];
        Full_Tensor( region_rmt[tag], rmt2 );
      }

      /* Testing comparison of tensors */
      if( Compare_Tensors( rmt1, rmt2 ) < mergeThresh ) {
        /* Since they are 'similar', combine */
        Combine_Tensors( rmt1, rmt2, rmt );

        /* If neither have a tag, give'm a new one, increase tag's count, and
         * give'm same tensor ABOUT TO BE CHANGED */
        if( cell_tag[cell] == -1 && cell_tag[neighbor] == -1 ) {
          op1++;
          tag = newTag;
          newTag++;
          cell_tag[cell] = cell_tag[neighbor] = tag;
          region_count[tag] = 2;

          region_rmt[tag][0] = rmt[0][0];
          region_rmt[tag][1] = rmt[0][1];
          region_rmt[tag][2] = rmt[1][1];
        }
        /* If one doesn't have a tag, copy it from the other tensor, increase
         * region tag count, copy combined tensor for all extents with tag */
        else if( cell_tag[cell] == -1 || cell_tag[neighbor] == -1 ) {
          op2++;

          if( cell_tag[cell] != -1 ) {
            tag = cell_tag[neighbor] = cell_tag[cell];
            region_count[tag]++;
          }
          else {
            tag = cell_tag[cell] = cell_tag[neighbor];
            region_count[tag]++;
          }

          region_rmt[tag][0] = rmt[0][0];
          region_rmt[tag][1] = rmt[0][1];
          region_rmt[tag][2] = rmt[1][1];
        }
        /* If both have a tag, pick the smallest tag, copy picked tag on all
         * extents tagged with previous tags, and copy combined tensor on them
         * all too */
        else {
          op3++;
          if( cell_tag[neighbor] < cell_tag[cell] ) {
            tag = cell_tag[neighbor];
            check = cell_tag[cell];
            region_count[tag] += region_count[check];
            region_count[check] = 0;
          }
          else {
            tag = cell_tag[cell];
            check = cell_tag[neighbor];
            region_count[tag] += region_count[check];
            region_count[check] = 0;
          }

          region_rmt[tag][0]   = rmt[0][0];
          region_rmt[tag][1]   = rmt[0][1];
          region_rmt[tag][2]   = rmt[1][1];
          region_rmt[check][0] = 0.0;
          region_rmt[check][1] = 0.0;
          region_rmt[check][2] = 0.0;

          counter = 0;
          a = cell_tag[cell];
          b = cell_tag[neighbor];
          check = region_count[tag];
          for( k = 0; k < nCells; k++ ) {
            n = new_ordering[k];
            if( cell_tag[n] == a || cell_tag[n] == b ) {
              cell_tag[n] = tag;
              counter++;
              if( counter == check )  break;
            }
          }
        }
      }
    }
    /* More check file */
    op1t += op1;
    op2t += op2;
    op3t += op3;
    fprintf( test, "%d)-> op1=%d, op2=%d, op3=%d |",
             c2c_count[cell]-(op1+op2+op3), op1, op2, op3 );
    if( op1 != 0 )  fprintf( test, " -newbies(%d)", newTag-1 );
    if( op2 != 0 )  fprintf( test, " -copy" );
    if( op3 != 0 )  fprintf( test, " -pick" );
    fprintf( test, "\n" );
  }
  /* Print other results TO BE FIXED */
  fclose( test );
  //printf( "* * * Neither have a tag:    %d\n", op1t );
  //printf( "* * * One has a tag:         %d\n", op2t );
  //printf( "* * * Both have a tag:       %d\n", op3t );
  check = 0;
  counter = 0;
  for( i = 0; i < newTag; i++ ) {
    if( region_count[i] != 0 ) {
//      printf( "Region %d -> %d\n", i, region_count[i] );
      check += region_count[i];
      counter++;
    }
  }
  int nMergedReg = counter;
  Print_OF();
  printf( "Merged regions:        %d\n", nMergedReg );
  Print_OF();
  printf( "Cells in a region:     %d\n", check );
  Print_OF();
  printf( "Greatest tag created:  %d\n", newTag-1 );

  /* Moving tags down, retagging cells, reorganizing data */
  int * o2n_tag;
  o2n_tag = ( int* ) malloc ( newTag * sizeof( int ) );
  n = 0;
  for( i = 0; i < newTag; i++ ) {
    o2n_tag[i] = -1;
    if( region_count[i] != 0 ) {
      o2n_tag[i] = n;
      region_count[n] = region_count[i];
      region_rmt[n][0] = region_rmt[i][0];
      region_rmt[n][1] = region_rmt[i][1];
      region_rmt[n][2] = region_rmt[i][2];
      n++;
    }
  }
  if( nMergedReg != n ) {
    Print_OF();
    printf( "Number of regions did NOT check!\n" );
    Print_OF();
    printf( "Exiting!\n" );
    exit( EXIT_FAILURE );
  }
  Print_OF();
  printf( "Number of regions checked (%d)!\n", n );
  Print_OF();
  printf( "Greatest tag created:  %d\n", newTag-1 );
  for( i = 0; i < nCells; i++ ) {
    if( cell_tag[i] != -1 )  cell_tag[i] = o2n_tag[cell_tag[i]];
  }
  /* Giving tags to cell that did not get included */
  newTag = nMergedReg;
  for( n = 0; n < nCells; n++ ) {
    i = new_ordering[n];
    if( cell_tag[i] == -1 ) {
      cell_tag[i] = newTag;
      region_count[newTag] = 1;
      region_rmt[newTag][0] = space[i].rmt[0];
      region_rmt[newTag][1] = space[i].rmt[1];
      region_rmt[newTag][2] = space[i].rmt[2];
      newTag++;
    }
    mi_matriz[i] = (double)cell_tag[i];
  }
  Print_OF();
  printf( "Regions created:       %d\n", newTag );
  Print_OF();
  printf( "Greatest tag created:  %d\n", newTag-1 );
  for( i = newTag; i < nCells; i++ ) {
    free( region_rmt[i] );
  }
  region_rmt   = ( double** ) realloc ( region_rmt, newTag * sizeof( double* ) );
  region_count = ( int* ) realloc ( region_count, newTag * sizeof( int ) );

  /* Generating list of cells in merged regions */
  int ** region_to_cell;
  region_to_cell = ( int** ) calloc ( nMergedReg, sizeof( int* ) );
  for( i = 0; i < nMergedReg; i++ ) {
    region_to_cell[i] = ( int* ) malloc ( region_count[i] * sizeof( int ) );
    region_count[i] = 0;
  }
  for( i = 0; i < nCells; i++ ) {
    n = new_ordering[i];
    tag = cell_tag[n];
    if( tag < nMergedReg ) {
      region_to_cell[tag][region_count[tag]] = n;
      region_count[tag]++;
    }
  }
  test = fopen( "cellRegions.out", "w" );
  for( i = 0; i < nMergedReg; i++ ) {
    fprintf( test, "%d:", i );
    for( j = 0; j < region_count[i]; j++ ) {
      fprintf( test, " %d,", region_to_cell[i][j] );
    }
    fprintf( test, "\n" );
  }
  fclose( test );

  /* Generating node tag array for deletion, and region to node connectivity */
  int ** reg_boundary, * node_tag, * n2n_count, *rb_count, ** reg_edge;
  int eArraySize, node1, node2;
  bool * gone_edge, doubleEdge = true;;

  /* Generating node to node count array, without node to node connectivity */
  n2n_count = ( int* ) malloc ( nNodes * sizeof( int ) );
  for( i = 0; i < nNodes; i++ ) {
    n2n_count[i] = n2c_count[i];
    if( boundary[i] )  n2n_count[i]++;
  }

  /* Tagging nodes to be deleted, and getting region boundary arrays */
  reg_boundary = ( int** ) calloc ( newTag, sizeof( int* ) );
  rb_count = ( int* ) calloc ( newTag, sizeof( int ) );
  node_tag = ( int* ) calloc ( nNodes, sizeof( int ) );
  int leave = 0, resize = 1;
  for( i = 0; i < nMergedReg; i++ ) {

    /* Calculating total number of edges in region, and allocating memory */
    eArraySize = 0;
    for( j = 0; j < region_count[i]; j++ ) {
      eArraySize += c2n_count[region_to_cell[i][j]];
    }
    reg_edge = ( int** ) malloc ( eArraySize * sizeof( int* ) );
    for( j = 0; j < eArraySize; j++ ) {
      reg_edge[j] = ( int* ) malloc ( 2 * sizeof( int ) );
    }
    /* Counting times node is used by cells in region, and getting edges */
    counter = 0;
    for( j = 0; j < region_count[i]; j++ ) {
      cell = region_to_cell[i][j];
      for( k = 0; k < c2n_count[cell]; k++ ) {
        node1 = cell_to_node[cell][k];
        node2 = cell_to_node[cell][(k+1)%c2n_count[cell]];
        node_tag[node1]++;
        reg_edge[counter][0] = node1;
        reg_edge[counter][1] = node2;
        counter++;
      }
    }
    if( eArraySize != counter ) {
      Print_OF();
      printf( "Number of arrays do not check! Reg: %d\n", i );
      Print_OF();
      printf( "Exiting!\n" );
      exit( EXIT_FAILURE );
    }
    /* Marking nodes for deletion as -1 */
    for( j = 0; j < region_count[i]; j++ ) {
      cell = region_to_cell[i][j];
      for( k = 0; k < c2n_count[cell]; k++ ) {
        node1 = cell_to_node[cell][k];
        if( node_tag[node1] > 2 ) {
          if( node_tag[node1] == n2n_count[node1] )
            node_tag[node1] = -1;
          else
            node_tag[node1] = 2;
        }
        //if( node_tag[node1] == -1 || node_tag[node1] == 0 )
        //  continue;
        //if( node_tag[node1] == n2n_count[node1] )
        //  node_tag[node1] = -1;
        //else
        //  node_tag[node1] = 0;
      }
    }
    //for( j = 0; j < nNodes; j++ ) {
    //  if( node_tag[j] == n2n_count[j] ) {
    //    node_tag[j] = -1;
    //  }
    //  else
    //    node_tag[j] = 0;
    //}
    /* Marking edges for deletion from list */
    gone_edge = ( bool* ) calloc ( eArraySize, sizeof( bool ) );
    for( j = 0; j < eArraySize; j++ ) {
      node1 = reg_edge[j][0];
      node2 = reg_edge[j][1];
      if( ( node_tag[node1] == -1 || node_tag[node1] == 2 ) &&
          ( node_tag[node2] == -1 || node_tag[node2] == 2 ) ) {
        if( node_tag[node1] == 2 && node_tag[node2] == 2 ) {
          doubleEdge = false;
          for( k = 0; k < eArraySize; k++ ) {
            if( reg_edge[k][0] == node2 && reg_edge[k][1] == node1 ) {
              doubleEdge = true;
              break;
            }
          }
        }
        if( doubleEdge )  gone_edge[j] = true;
        doubleEdge = true;
      }
    }
    /* Moving data down in edges, and resizing array */
    counter = 0;
    for( j = 0; j < eArraySize; j++ ) {
      if( !gone_edge[j] ) {
        reg_edge[counter][0] = reg_edge[j][0];
        reg_edge[counter][1] = reg_edge[j][1];
        counter++;
      }
    }
    for( j = counter; j < eArraySize; j++ ) {
      free( reg_edge[j] );
    }
    eArraySize = counter;
//    printf( "Reg %d: arraysize %d\n", i, eArraySize );
    reg_edge = ( int** ) realloc ( reg_edge, eArraySize * sizeof( int* ) );
    free( gone_edge );

    /* Sorting edges counter clock wise */
//    printf( "%d(%d) ->", i, eArraySize );
//    for( a = 0; a < eArraySize; a++ ) 
//      printf( " (%d-%d),", reg_edge[a][0], reg_edge[a][1] );
//    printf( "\n" );
    for( a = 1; a < eArraySize-1; a++ ) {
      node1 = reg_edge[a][0];
      node2 = reg_edge[a][1];
      for( b = a; b < eArraySize; b++ ) {
        if( reg_edge[b][0] == reg_edge[a-1][1] ) {
          reg_edge[a][0] = reg_edge[b][0];
          reg_edge[a][1] = reg_edge[b][1];
          reg_edge[b][0] = node1;
          reg_edge[b][1] = node2;
        }
      }
    }
    node1 = reg_edge[0][0];
    node2 = reg_edge[eArraySize-1][1];
    if( node1 != node2 ) {
      resize = 0;
      Print_OF();
      printf( "Node did not match in first and last edge! Reg: %d\n", i );
      Print_OF();
      printf( "Node 1 %d - Node 2 %d\n", node1, node2 );
      Print_OF();
      printf( "Enter 0 to exit now, 1 to continue checking: " );
      scanf( "%d", &leave );
      if( !leave ) {
        Print_OF();
        printf( "Exiting\n" );
        exit( EXIT_FAILURE );
      }
    }

    /* One more check for aligning edges */
    Point p1, p2;
    Vector ev;
    double au1[2], au2[2];
    int e1, e2;
    if( resize ) {
      /* Marking edges for new deletion */
      gone_edge = ( bool* ) calloc ( eArraySize, sizeof( bool ) );
      for( j = 0; j < eArraySize; j++ ) {
        /* Getting absolute value of components of first edge unit vector */
        e1 = j;
        node1 = reg_edge[e1][0];
        node2 = reg_edge[e1][1];
//        node_tag[node1] = node_tag[node2] = -1;//tag node for deletion
        p1 = Point( x[node1], y[node1] );
        p2 = Point( x[node2], y[node2] );
        ev = Vector( p1, p2 );
        ev.normalize();
        au1[0] = fabs( ev[0] );
        au1[1] = fabs( ev[1] );
        /* Getting absolute value of components of second edge unit vector */
        e2 = (e1+1)%eArraySize;
        while( gone_edge[e2] ){
          e2 = (e2+1)%eArraySize;
        };
        node1 = reg_edge[e2][0];
        node2 = reg_edge[e2][1];
//        node_tag[node1] = node_tag[node2] = -1;//tag node for deletion
        p1 = Point( x[node1], y[node1] );
        p2 = Point( x[node2], y[node2] );
        ev = Vector( p1, p2 );
        ev.normalize();
        au2[0] = fabs( ev[0] );
        au2[1] = fabs( ev[1] );
        /* Testing edges for alignment, if aligned */
        if( ( fabs( au1[0] - au2[0] ) < 1.0e-10 ) &&
            ( fabs( au1[1] - au2[1] ) < 1.0e-10 ) ) {
          /* Mark first edge for deletion, and recover nodes for new edge */
          node1 = reg_edge[e1][0];
//          node_tag[node1] = 0;//recover first node of new edge
          gone_edge[e1] = true;
          reg_edge[e2][0] = node1;
          node2 = reg_edge[e2][1];
//          node_tag[node2] = 0;//recover second node of new edge
        }
      }

      /* Moving data down in edges, and resizing array */
      counter = 0;
      for( j = 0; j < eArraySize; j++ ) {
        if( !gone_edge[j] ) {
          reg_edge[counter][0] = reg_edge[j][0];
          reg_edge[counter][1] = reg_edge[j][1];
          counter++;
        }
      }
      for( j = counter; j < eArraySize; j++ ) {
        free( reg_edge[j] );
      }
      eArraySize = counter;
      reg_edge = ( int** ) realloc ( reg_edge, eArraySize * sizeof( int* ) );
      free( gone_edge );
    }

    /* Allocating memory for actual region boundary array, saving data, and
     * freeing data */
    reg_boundary[i] = ( int* ) malloc ( eArraySize * sizeof( int ) );
    for( j = 0; j < eArraySize; j++ ) {
      reg_boundary[i][j] = reg_edge[j][0];
      node_tag[reg_edge[j][0]] = 0;
      free( reg_edge[j] );
    }
    free( reg_edge );
    rb_count[i] = eArraySize;
    maxN2C = MAX( maxN2C, eArraySize );

  }

  /* Retagging nodes for final deletion */
  for( i = 0; i < nNodes; i++ ) {
    node_tag[i] = -1;
  }
  for( i = 0; i < nMergedReg; i++ ) {
    for( j = 0; j < rb_count[i]; j++ ) {
      node_tag[reg_boundary[i][j]] = 0;
    }
  }
  /* Passing boundary information to the reg_boundary array from the
   * node_to_cell connectivity, from extents not merged into regions */
  k = nMergedReg;
  for( n = 0; n < nCells; n++ ) {
    i = new_ordering[n];
    if( cell_tag[i] == -1 ) {
      rb_count[k] = n2c_count[i];
      reg_boundary[k] = ( int* ) malloc ( rb_count[k] * sizeof( int ) );
      for( j = 0; j < rb_count[k]; j++ ) {
        reg_boundary[k][j] = cell_to_node[i][j];
        /* Reclaiming node in case marked for deletion by check for aligning
         * edges */
        node_tag[cell_to_node[i][j]] = 0;
      }
      k++;
    }
  }
  if( k == newTag ) {
    Print_OF();
    printf( "Recovering edges of unmerged cells done successfully!\n" );
  }
  else {
    Print_OF();
    printf( "Edges of unmerged cells were not recovered successfully!\n" );
    Print_OF();
    printf( "Exiting!\n" );
    exit( 0 );
  }

  /* Freeing some memory */
  for( i = 0; i < nNodes; i++ ) {
    free( node_to_cell[i] );
  }
  free( node_to_cell );
  for( i = 0; i < nCells; i++ ) {
    free( cell_to_cell[i] );
  }
  free( cell_to_cell );

  /* Reorganizing data */
  if( resize ) {

    /* Deleting Spacing_Obj list and connectivity arrays for later recreation */
    delete a_t;
    a_t = new Spacing_Obj();
    for( i = 0; i < nCells; i++ ) {
      free( cell_to_node[i] );
    }
    free( cell_to_node );
    free( c2n_count );

    /* Allocating memory for vert to be passed to Spacing_Obj list */
    double vert[100000][2];
    //vert = ( double** ) malloc ( maxN2C * sizeof( double* ) );
    //for( i = 0; i < maxN2C; i++ ) {
    //  vert[i] = ( double* ) malloc ( 2 * sizeof( double ) );
    //}

    /* Creating map of new nodes, and moving down coordinates */
    int * new_cent = NULL;
    new_cent = ( int* ) malloc ( nNodes * sizeof( int ) );
    j = 0;
    for( i = 0; i < nNodes; i++ ) {
      new_cent[i] = -1;
      if( node_tag[i] != -1 ) {
        new_cent[i] = j;
        x[j] = x[i];
        y[j] = y[i];
        j++;
      }
    }

    /* Freeing memory and resizing coordinate arrays */
    nNodes = j;
    *t_nNodes = nNodes;
    nCells = newTag;
    *t_nCells = nCells;
    x = ( double* ) realloc ( x, nNodes * sizeof( double ) );
    y = ( double* ) realloc ( y, nNodes * sizeof( double ) );
    mi_matriz = ( double* ) realloc ( mi_matriz, nCells * sizeof( double ) );

    /* Reindexing connectivity nodes, and storing information */
    for( i = 0; i < nCells; i++ ) {

      /* Updating region tag */
      mi_matriz[i] = (double)i;
      for( j = 0; j < rb_count[i]; j++ ) {
        /* Updating region boundary node indexes */
        reg_boundary[i][j] = new_cent[reg_boundary[i][j]];
        /* Copying coordinates to array of region vertices */
        vert[j][0] = x[reg_boundary[i][j]];
        vert[j][1] = y[reg_boundary[i][j]];
      }

      /* Getting spacing info ready for testing */
      a_t->Full_Tensor( region_rmt[i], rmt );
      a_t->Decompose_Tensor( rmt, left, right, lam );
      h1 = sqrt( 1.0 / lam[0] );
      h2 = sqrt( 1.0 / lam[1] );
      /* Store tensors */
      if( mode == 1 || ( mode == 2 && MAX( h1, h2 ) / MIN( h1, h2 ) > 1.5 ) ) {
        a_t->Store_Tensor( rmt, rb_count[i], vert );
      }
      /* Store scalars */
      else {
        dumb = MIN( h1, h2 );
        a_t->Store_Scalar( dumb, rb_count[i], vert );
      }

    }

    /* Freeing memory */
    //for( i = 0; i < maxN2C; i++ ) {
    //  free( vert[i] );
    //}
    //free( vert );
    free( new_cent );

    /* Interchanging pointers */
    *t_c2n_count = rb_count;
    *t_cell_to_node = reg_boundary;

  }

  /*** Freeing memory ***/
  free( boundary );
  free( node_tag );
  free( cell_tag );
  free( region_count );
  free( n2c_count );
  free( n2n_count );
  for( i = 0; i < nMergedReg; i++ ) {
    free( region_to_cell[i] );
  }
  free( region_to_cell );
  free( o2n_tag );
  free( c2c_count );
  free( new_ordering );
  for( i = 0; i < newTag; i++ ) {
    free( region_rmt[i] );
  }
  free( region_rmt );

  if( !resize ) {
    for( i = 0; i < newTag; i++ ) {
      free( reg_boundary[i] );
      free( region_rmt[i] );
    }
    free( reg_boundary );
    free( rb_count );
  }
  /* Checking results */
//  test = fopen( "reordering.out", "w" );
//  for( i = 0; i < nCells; i++ ) {
//    fprintf( test, "%4d:%4d\n", i, new_ordering[i] );
//  }
//  fclose( test );

}




//void Spacing_Obj::Optimize_Field(int mode)
//{
//  double glo[3], ghi[3], rmt[3][3], lo[3], hi[3], rmt1[3][3], rmt2[3][3];
//  double dx, dy, dz, mid[3], slop[3], eps, vec[6];
//  int im, jm, km, i, j, k, level, m, max_level, n, num;
//  PList *slist, *dlist;
//  Size_Obj *ptr, *ptr1, *ptr2, *tsize;
//
//  space_root->extents(glo,ghi);
//
//  max_level = space_root->max_level(1);
//  printf("\nMaximum number of levels in Octree = %d",max_level);
//  printf("\nTotal number of entries = %d",nsp);

  //dx = (ghi[0]-glo[0]);
  //dy = (ghi[1]-glo[1]);
  //dz = (ghi[2]-glo[2]);
  //eps = MIN(dx,MIN(dy,dz));
  //eps /= pow(2,max_level);
  //eps *= 0.0001;

  //slist = new PList();
  //dlist = new PList();

  //// check each octant and subdivide entries to push to finest level


  //for (level = 1; level <= max_level; level++)
  //{
  //  printf("\nWorking on level %d, total # of entries = %d",level,nsp);
  //  fflush(stdout);

  //  im = jm = km = (int)pow(2,(level-1));

  //  dx = (ghi[0]-glo[0])/im;
  //  dy = (ghi[1]-glo[1])/jm;
  //  dz = (ghi[2]-glo[2])/km;

  //  slop[0] = dx/2.0 - eps;
  //  slop[1] = dy/2.0 - eps;
  //  slop[2] = dz/2.0 - eps;

  //  for (k=0; k < km; k++)
  //  {
  //    for (j=0; j < jm; j++)
  //    {
  //      for (i=0; i < im; i++)
  //      {
  //        
  //        mid[0] = glo[0] + i*dx + dx/2.0;
  //        mid[1] = glo[1] + j*dy + dy/2.0;
  //        mid[2] = glo[2] + k*dz + dz/2.0;
  //        
  //        dlist->Redimension(0);

  //        slist->Redimension(0);

  //        if (mode == 1) // push entries to lowest levels
  //        {
  //          // first see if there are children at next level
  //          // if not, don't push down
  //          slist->Redimension(0);
  //          space_root->retrieve_list(mid,slop,level+1,slist);
  //          if (slist->max == 0)
  //            continue;
  //        }

  //        // now grab entries at level
  //        slist->Redimension(0);
  //        space_root->retrieve_list(mid,slop,level,slist);
  //        if (slist->max == 0)
  //          continue;
  //        // immediately remove from Octree
  //        for (m=0; m < slist->max; m++)
  //        {
  //          ptr = (Size_Obj*)slist->list[m];
  //          lo[0] = ptr->lo[0];
  //          lo[1] = ptr->lo[1];
  //          lo[2] = ptr->lo[2];
  //          hi[0] = ptr->hi[0];
  //          hi[1] = ptr->hi[1];
  //          hi[2] = ptr->hi[2];
  //          if (space_root->Remove_From_Octree((void*)ptr, lo, hi) != 0)
  //          {
  //            //printf("\nFailed deleting entry %d from Octree",m);
  //            if (space_root->Remove_From_Octree((void*)ptr) != 0)
  //            {
  //              printf("\nSecond attempted failed deleting entry %d from Octree",m);
  //              exit(0);
  //            }
  //          }
  //        }

  //        if (mode == 1)
  //        {
  //          // now divide using mid point and reinsert in Octree

  //          // check X coordinate
  //          for (m=slist->max-1; m >= 0; m--)
  //          {
  //            ptr = (Size_Obj*)slist->list[m];
  //            lo[0] = ptr->lo[0];
  //            lo[1] = ptr->lo[1];
  //            lo[2] = ptr->lo[2];
  //            hi[0] = ptr->hi[0];
  //            hi[1] = ptr->hi[1];
  //            hi[2] = ptr->hi[2];
  //            if (lo[0]+eps < mid[0] && hi[0]-eps > mid[0])
  //            {
  //              lo[0] = mid[0] + eps;
  //              ptr->hi[0] = mid[0] - eps;
  //              if (ptr->sdim == 1)
  //                n = Store_Scalar(ptr->rmt[0],lo,hi);
  //              else
  //              {
  //                Full_Tensor(ptr->rmt,rmt);
  //                n = Store_Tensor(rmt,lo,hi);
  //              }
  //              slist->Add_To_List((void*)&space[n]);
  //            }
  //          }
  //          // check Y coordinate
  //          for (m=slist->max-1; m >= 0; m--)
  //          {
  //            ptr = (Size_Obj*)slist->list[m];
  //            lo[0] = ptr->lo[0];
  //            lo[1] = ptr->lo[1];
  //            lo[2] = ptr->lo[2];
  //            hi[0] = ptr->hi[0];
  //            hi[1] = ptr->hi[1];
  //            hi[2] = ptr->hi[2];
  //            if (lo[1]+eps < mid[1] && hi[1]-eps > mid[1])
  //            {
  //              lo[1] = mid[1] + eps;
  //              ptr->hi[1] = mid[1] - eps;
  //              if (ptr->sdim == 1)
  //                n = Store_Scalar(ptr->rmt[0],lo,hi);
  //              else
  //              {
  //                Full_Tensor(ptr->rmt,rmt);
  //                n = Store_Tensor(rmt,lo,hi);
  //              }
  //              slist->Add_To_List((void*)&space[n]);
  //            }
  //          }
  //          // check Z coordinate
  //          for (m=slist->max-1; m >= 0; m--)
  //          {
  //            ptr = (Size_Obj*)slist->list[m];
  //            lo[0] = ptr->lo[0];
  //            lo[1] = ptr->lo[1];
  //            lo[2] = ptr->lo[2];
  //            hi[0] = ptr->hi[0];
  //            hi[1] = ptr->hi[1];
  //            hi[2] = ptr->hi[2];
  //            if (lo[2]+eps < mid[2] && hi[2]-eps > mid[2])
  //            {
  //              lo[2] = mid[2] + eps;
  //              ptr->hi[2] = mid[2] - eps;
  //              if (ptr->sdim == 1)
  //                n = Store_Scalar(ptr->rmt[0],lo,hi);
  //              else
  //              {
  //                Full_Tensor(ptr->rmt,rmt);
  //                n = Store_Tensor(rmt,lo,hi);
  //              }
  //              slist->Add_To_List((void*)&space[n]);
  //            }
  //          }
  //        }

  //        if (mode == 2) // check overlaps and either combine or subdivide
  //        {
  //          double diff;

  //          // first perform combinations
  //          for (m=0; m < slist->max-1; m++)
  //          {
  //            ptr1 = (Size_Obj*)slist->list[m];
  //  
  //            n = m+1;
  //            while (n < slist->max)
  //            {
  //              ptr2 = (Size_Obj*)slist->list[n];

  //              /* If any of these happens, there is NO overlap */
  //              if (ptr1->lo[0]+eps > ptr2->hi[0] || ptr1->hi[0]-eps < ptr2->lo[0] ||
  //                  ptr1->lo[1]+eps > ptr2->hi[1] || ptr1->hi[1]-eps < ptr2->lo[1] ||
  //                  ptr1->lo[2]+eps > ptr2->hi[2] || ptr1->hi[2]-eps < ptr2->lo[2])
  //              {
  //                n++;
  //                continue;
  //              }
  //
  //              diff = (fabs(ptr1->lo[0]-ptr2->lo[0])+fabs(ptr1->hi[0]-ptr2->hi[0]))/
  //                      (MAX(ptr1->hi[0],ptr2->hi[0])-MIN(ptr1->lo[0],ptr2->lo[0]));
  //              diff = MAX(diff,(fabs(ptr1->lo[1]-ptr2->lo[1])+fabs(ptr1->hi[1]-ptr2->hi[1]))/
  //                              (MAX(ptr1->hi[1],ptr2->hi[1])-MIN(ptr1->lo[1],ptr2->lo[1])));
  //              diff = MAX(diff,(fabs(ptr1->lo[2]-ptr2->lo[2])+fabs(ptr1->hi[2]-ptr2->hi[2]))/
  //                              (MAX(ptr1->hi[2],ptr2->hi[2])-MIN(ptr1->lo[2],ptr2->lo[2])));
  //              if (ptr1->sdim == 1 && ptr2->sdim == 1) // combination remains a scalar
  //              {
  //                if (diff < 0.05 ||
  //                    fabs(ptr1->rmt[0]-ptr2->rmt[0])/MAX(ptr1->rmt[0],ptr2->rmt[0]) < 0.05)
  //                {
  //                  slist->Delete_From_List(ptr2);
  //                  dlist->Add_To_List(ptr2);
  //                  // modify pointer 1 contents
  //                  ptr1->lo[0] = MIN(ptr1->lo[0],ptr2->lo[0]);
  //                  ptr1->lo[1] = MIN(ptr1->lo[1],ptr2->lo[1]);
  //                  ptr1->lo[2] = MIN(ptr1->lo[2],ptr2->lo[2]);
  //                  ptr1->hi[0] = MAX(ptr1->hi[0],ptr2->hi[0]);
  //                  ptr1->hi[1] = MAX(ptr1->hi[1],ptr2->hi[1]);
  //                  ptr1->hi[2] = MAX(ptr1->hi[2],ptr2->hi[2]);
  //                  ptr1->rmt[0] = (ptr1->rmt[0]+ptr2->rmt[0])*0.5;
  //                } else
  //                  n++; // increment counter
  //              } else // combination is a tensor
  //              {
  //                if (ptr1->sdim == 1)
  //                  Scalar_To_Tensor(ptr1->rmt[0],rmt1);
  //                else
  //                  Full_Tensor(ptr1->rmt,rmt1);
  //                if (ptr2->sdim == 1)
  //                  Scalar_To_Tensor(ptr2->rmt[0],rmt2);
  //                else
  //                  Full_Tensor(ptr2->rmt,rmt2);
  //                if (diff < 0.05 || Compare_Tensors(rmt1,rmt2) < 0.05)
  //                {
  //                  slist->Delete_From_List(ptr2);
  //                  dlist->Add_To_List(ptr2);
  //                  // modify pointer 1 contents
  //                  ptr1->lo[0] = MIN(ptr1->lo[0],ptr2->lo[0]);
  //                  ptr1->lo[1] = MIN(ptr1->lo[1],ptr2->lo[1]);
  //                  ptr1->lo[2] = MIN(ptr1->lo[2],ptr2->lo[2]);
  //                  ptr1->hi[0] = MAX(ptr1->hi[0],ptr2->hi[0]);
  //                  ptr1->hi[1] = MAX(ptr1->hi[1],ptr2->hi[1]);
  //                  ptr1->hi[2] = MAX(ptr1->hi[2],ptr2->hi[2]);
  //                  Combine_Tensors(rmt1,rmt2,rmt);
  //                  if (ptr1->sdim == 1)
  //                  {
  //                    vec[0] = rmt[0][0];
  //                    vec[1] = rmt[0][1];
  //                    vec[2] = rmt[0][2];
  //                    vec[3] = rmt[1][1];
  //                    vec[4] = rmt[1][2];
  //                    vec[5] = rmt[2][2];
  //                    tsize = new Size_Obj(6,vec,ptr1->lo,ptr1->hi);
  //                    *ptr1 = *tsize;
  //                    delete tsize;
  //                  } else
  //                  {
  //                    ptr1->rmt[0] = rmt[0][0];
  //                    ptr1->rmt[1] = rmt[0][1];
  //                    ptr1->rmt[2] = rmt[0][2];
  //                    ptr1->rmt[3] = rmt[1][1];
  //                    ptr1->rmt[4] = rmt[1][2];
  //                    ptr1->rmt[5] = rmt[2][2];
  //                  }
  //                } else
  //                  n++;
  //              }
  //            }
  //          }

  //          printf("\nNumber of items in spacing list = %d",slist->max);
  //          printf("\nNumber of items in delete list = %d",dlist->max);
  //          fflush(stdout);

  //          double split[2];

  //          // now perform X splitting
  //          for (m=0; m < slist->max-1; m++)
  //          {
  //            ptr1 = (Size_Obj*)slist->list[m];
  //  
  //            for (n=m+1; n < slist->max; n++)
  //            {
  //              ptr2 = (Size_Obj*)slist->list[n];

  //              // extent test
  //              if (ptr1->lo[0]+eps > ptr2->hi[0] || ptr1->hi[0]-eps < ptr2->lo[0] ||
  //                  ptr1->lo[1]+eps > ptr2->hi[1] || ptr1->hi[1]-eps < ptr2->lo[1] ||
  //                  ptr1->lo[2]+eps > ptr2->hi[2] || ptr1->hi[2]-eps < ptr2->lo[2])
  //                continue;
  //
  //              split[0] = MAX(ptr1->lo[0],ptr2->lo[0]);
  //              split[1] = MIN(ptr1->hi[0],ptr2->hi[0]);
  //     
  //              if (split[0] > ptr1->lo[0]+eps && split[0] < ptr1->hi[0]-eps)
  //              {
  //                lo[0] = split[0];
  //                lo[1] = ptr1->lo[1];
  //                lo[2] = ptr1->lo[2];
  //                hi[0] = ptr1->hi[0];
  //                hi[1] = ptr1->hi[1];
  //                hi[2] = ptr1->hi[2];
  //                ptr1->hi[0] = split[0];
  //                if (ptr1->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr1->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr1->rmt,rmt1);
  //                  n = Store_Tensor(rmt1,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[1] > ptr1->lo[0]+eps && split[1] < ptr1->hi[0]-eps)
  //              {
  //                lo[0] = ptr1->lo[0];
  //                lo[1] = ptr1->lo[1];
  //                lo[2] = ptr1->lo[2];
  //                hi[0] = split[1];
  //                hi[1] = ptr1->hi[1];
  //                hi[2] = ptr1->hi[2];
  //                ptr1->lo[0] = split[1];
  //                if (ptr1->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr1->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr1->rmt,rmt1);
  //                  n = Store_Tensor(rmt1,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[0] > ptr2->lo[0]+eps && split[0] < ptr2->hi[0]-eps)
  //              {
  //                lo[0] = split[0];
  //                lo[1] = ptr2->lo[1];
  //                lo[2] = ptr2->lo[2];
  //                hi[0] = ptr2->hi[0];
  //                hi[1] = ptr2->hi[1];
  //                hi[2] = ptr2->hi[2];
  //                ptr2->hi[0] = split[0];
  //                if (ptr2->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr2->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr2->rmt,rmt2);
  //                  n = Store_Tensor(rmt2,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[1] > ptr2->lo[0]+eps && split[1] < ptr2->hi[0]-eps)
  //              {
  //                lo[0] = ptr2->lo[0];
  //                lo[1] = ptr2->lo[1];
  //                lo[2] = ptr2->lo[2];
  //                hi[0] = split[1];
  //                hi[1] = ptr2->hi[1];
  //                hi[2] = ptr2->hi[2];
  //                ptr2->lo[0] = split[1];
  //                if (ptr2->sdim == 1)
  //                {
  //                  n = Store_Scalar(0.5*(ptr1->rmt[0]+ptr2->rmt[0]),lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr2->rmt,rmt2);
  //                  n = Store_Tensor(rmt2,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //            }
  //          }

  //          printf("\nX splitting complete.");
  //          printf("\nNumber of items in spacing list = %d",slist->max);
  //          fflush(stdout);

  //          // now perform Y splitting
  //          for (m=0; m < slist->max-1; m++)
  //          {
  //            ptr1 = (Size_Obj*)slist->list[m];
  //  
  //            for (n=m+1; n < slist->max; n++)
  //            {
  //              ptr2 = (Size_Obj*)slist->list[n];

  //              // extent test
  //              if (ptr1->lo[0] > ptr2->hi[0] || ptr1->hi[0] < ptr2->lo[0] ||
  //                  ptr1->lo[1] > ptr2->hi[1] || ptr1->hi[1] < ptr2->lo[1] ||
  //                  ptr1->lo[2] > ptr2->hi[2] || ptr1->hi[2] < ptr2->lo[2])
  //                continue;
  //
  //              split[0] = MAX(ptr1->lo[1],ptr2->lo[1]);
  //              split[1] = MIN(ptr1->hi[1],ptr2->hi[1]);
  //     
  //              if (split[0] > ptr1->lo[1]+eps && split[0] < ptr1->hi[1]-eps)
  //              {
  //                lo[0] = ptr1->lo[0];
  //                lo[1] = split[0];
  //                lo[2] = ptr1->lo[2];
  //                hi[0] = ptr1->hi[0];
  //                hi[1] = ptr1->hi[1];
  //                hi[2] = ptr1->hi[2];
  //                ptr1->hi[1] = split[1];
  //                if (ptr1->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr1->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr1->rmt,rmt1);
  //                  n = Store_Tensor(rmt1,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[1] > ptr1->lo[1]+eps && split[1] < ptr1->hi[1]-eps)
  //              {
  //                lo[0] = ptr1->lo[0];
  //                lo[1] = ptr1->lo[1];
  //                lo[2] = ptr1->lo[2];
  //                hi[0] = ptr1->hi[0];
  //                hi[1] = split[1];
  //                hi[2] = ptr1->hi[2];
  //                ptr1->lo[1] = split[1];
  //                if (ptr1->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr1->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr1->rmt,rmt1);
  //                  n = Store_Tensor(rmt1,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[0] > ptr2->lo[1]+eps && split[0] < ptr2->hi[1]-eps)
  //              {
  //                lo[0] = ptr2->lo[0];
  //                lo[1] = split[0];
  //                lo[2] = ptr2->lo[2];
  //                hi[0] = ptr2->hi[0];
  //                hi[1] = ptr2->hi[1];
  //                hi[2] = ptr2->hi[2];
  //                ptr2->hi[1] = split[0];
  //                if (ptr2->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr2->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr2->rmt,rmt2);
  //                  n = Store_Tensor(rmt2,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[1] > ptr2->lo[1]+eps && split[1] < ptr2->hi[1]-eps)
  //              {
  //                lo[0] = ptr2->lo[0];
  //                lo[1] = ptr2->lo[1];
  //                lo[2] = ptr2->lo[2];
  //                hi[0] = ptr2->hi[0];
  //                hi[1] = split[1];
  //                hi[2] = ptr2->hi[2];
  //                ptr2->lo[1] = split[1];
  //                if (ptr2->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr2->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr2->rmt,rmt2);
  //                  n = Store_Tensor(rmt2,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //            }
  //          }

  //          printf("\nY splitting complete.");
  //          printf("\nNumber of items in spacing list = %d",slist->max);
  //          fflush(stdout);

  //          // now perform Z splitting
  //          for (m=0; m < slist->max-1; m++)
  //          {
  //            ptr1 = (Size_Obj*)slist->list[m];
  //  
  //            for (n=m+1; n < slist->max; n++)
  //            {
  //              ptr2 = (Size_Obj*)slist->list[n];

  //              // extent test
  //              if (ptr1->lo[0] > ptr2->hi[0] || ptr1->hi[0] < ptr2->lo[0] ||
  //                  ptr1->lo[1] > ptr2->hi[1] || ptr1->hi[1] < ptr2->lo[1] ||
  //                  ptr1->lo[2] > ptr2->hi[2] || ptr1->hi[2] < ptr2->lo[2])
  //                continue;
  //
  //              split[0] = MAX(ptr1->lo[2],ptr2->lo[2]);
  //              split[1] = MIN(ptr1->hi[2],ptr2->hi[2]);
  //     
  //              if (split[0] > ptr1->lo[2]+eps && split[0] < ptr1->hi[2]-eps)
  //              {
  //                lo[0] = ptr1->lo[0];
  //                lo[1] = ptr1->lo[1];
  //                lo[2] = split[0];
  //                hi[0] = ptr1->hi[0];
  //                hi[1] = ptr1->hi[1];
  //                hi[2] = ptr1->hi[2];
  //                ptr1->hi[2] = split[0];
  //                if (ptr1->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr1->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr1->rmt,rmt1);
  //                  n = Store_Tensor(rmt1,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[1] > ptr1->lo[2]+eps && split[1] < ptr1->hi[2]-eps)
  //              {
  //                lo[0] = ptr1->lo[0];
  //                lo[1] = ptr1->lo[1];
  //                lo[2] = ptr1->lo[2];
  //                hi[0] = ptr1->hi[0];
  //                hi[1] = ptr1->hi[1];
  //                hi[2] = split[1];
  //                ptr1->lo[2] = split[1];
  //                if (ptr1->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr1->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr1->rmt,rmt1);
  //                  n = Store_Tensor(rmt1,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[0] > ptr2->lo[2]+eps && split[0] < ptr2->hi[2]-eps)
  //              {
  //                lo[0] = ptr2->lo[0];
  //                lo[1] = ptr2->lo[1];
  //                lo[2] = split[0];
  //                hi[0] = ptr2->hi[0];
  //                hi[1] = ptr2->hi[1];
  //                hi[2] = ptr2->hi[2];
  //                ptr2->hi[2] = split[0];
  //                if (ptr2->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr2->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr2->rmt,rmt2);
  //                  n = Store_Tensor(rmt2,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //              if (split[1] > ptr2->lo[2]+eps && split[1] < ptr2->hi[2]-eps)
  //              {
  //                lo[0] = ptr2->lo[0];
  //                lo[1] = ptr2->lo[1];
  //                lo[2] = ptr2->lo[2];
  //                hi[0] = ptr2->hi[0];
  //                hi[1] = ptr2->hi[1];
  //                hi[2] = split[1];
  //                ptr2->lo[2] = split[1];
  //                if (ptr2->sdim == 1)
  //                {
  //                  n = Store_Scalar(ptr2->rmt[0],lo,hi);
  //                } else
  //                {
  //                  Full_Tensor(ptr2->rmt,rmt2);
  //                  n = Store_Tensor(rmt2,lo,hi);
  //                }
  //                slist->Add_To_List((void*)&space[n]);
  //              }
  //            }
  //          }

  //          printf("\nZ splitting complete.");
  //          printf("\nNumber of items in spacing list = %d",slist->max);
  //          fflush(stdout);

  //        }

  //        // now store back in Octree
  //        num = slist->max;
  //        void* *tptr;
  //        double (*tlo)[3], (*thi)[3];
  //        tptr = new void*[num];
  //        tlo = new double[num][3];
  //        thi = new double[num][3];
  //        for (m=0; m < num; m++)
  //        {
  //          ptr = (Size_Obj*)slist->list[m];
  //          tptr[i] = (void*)ptr;//tptr[m] = (void*)ptr;
  //          for (n=0; n < 3; n++)
  //          {
  //            //lo[n] = ptr->lo[n];
  //            //hi[n] = ptr->hi[n];
  //            tlo[i][j] = ptr->lo[j];//tlo[m][n] = ptr->lo[n];
  //            thi[i][j] = ptr->hi[j];//thi[m][n] = ptr->hi[n];
  //          }                                            
  //          //space_root->Store_In_Octree((void*)ptr,lo, hi);
  //        }                                              
                                                           
  //        space_root->Store_In_Octree(num,tptr,tlo,thi); 
                                                           
  //        while (dlist->max > 0)                         
  //        {                                              
  //          while (dlist->Is_In_List(&space[nsp-1]) && d list->max > 0)
  //          {                                            
  //            nsp--;                                     
  //            dlist->Delete_From_List(&space[nsp]);      
  //          }                                            
  //          if (dlist->max == 0)                         
  //            break;                                     
  //          ptr = (Size_Obj*)dlist->list[0];             
  //          dlist->Delete_From_List(ptr);                
  //          nsp--;                                       
  //          lo[0] = space[nsp].lo[0];                    
  //          lo[1] = space[nsp].lo[1];
  //          lo[2] = space[nsp].lo[2];
  //          hi[0] = space[nsp].hi[0];
  //          hi[1] = space[nsp].hi[1];
  //          hi[2] = space[nsp].hi[2];
  //          // copy contents from end to pointer 2 location
  //          *ptr = space[nsp];
  //          if (!space_root->Replace_in_Octree((void*)&space[nsp],(void*)ptr,lo,hi))
  //          {
  //            //printf("\n1st attempt failed replacing entry with %d",nsp);
  //            if (!space_root->Replace_in_Octree((void*)&space[nsp],(void*)ptr))
  //            {
  //              printf("\n2nd attempt failed replacing entry with %d",nsp);
  //              exit(0);
  //            }
  //          }
  //        }

  //        //delete[] tptr;
  //        //delete[] tlo;
  //        //delete[] thi;
  //      }
  //    }
  //  }
  //}
  //delete slist;
  //delete dlist;

  //printf("\nTotal number of entries = %d",nsp);
//  fflush(stdout);
//}

/* Store a scalar with given extent */
int Spacing_Obj::Store_Scalar(double sp, double tlo[SD], double thi[SD]) {

  /* If snp+1 is greater than sp_dim, reallocate memory on space and construct
   * on each single new lot of space */
  if (nsp+1 >= sp_dim)
  {
    int old_dim = sp_dim;
    space = (Size_Obj*)realloc((void*)space,(sp_dim+Space_CHUNK)*sizeof(Size_Obj));
    if (space == 0)
    {
      fprintf(stderr,"\nStore_Scalar1: Unable to reallocate space array.");
      fflush(stderr);
      exit(0);
    }
    sp_dim += Space_CHUNK;
    for (int i=old_dim; i < sp_dim; i++)
      space[i].Construct();
  }
  double array[1];
  int tdim=1;
  array[0]=sp;
  Size_Obj *ptr = new Size_Obj(tdim,array,tlo,thi);
  space[nsp] = *ptr;
  delete ptr;

  return(nsp++);
}

/* Store a scalar with given element */
int Spacing_Obj::Store_Scalar(double sp, int tnv, double tvert[][SD]) {

  /* If snp+1 is greater than sp_dim, reallocate memory on space and construct
   * on each single new lot of space */
  if (nsp+1 >= sp_dim)
  {
    int old_dim = sp_dim;
    space = (Size_Obj*)realloc((void*)space,(sp_dim+Space_CHUNK)*sizeof(Size_Obj));
    if (space == 0)
    {
      fprintf(stderr,"\nStore_Scalar2: Unable to reallocate space array.");
      fflush(stderr);
      exit(0);
    }
    sp_dim += Space_CHUNK;
    for (int i=old_dim; i < sp_dim; i++)
      space[i].Construct();
  }
  double array[1];
  int tdim=1;
  array[0]=sp;
  Size_Obj *ptr = new Size_Obj(tdim,array,tnv,tvert);
  space[nsp] = *ptr;
  delete ptr;

  return(nsp++);
}

/* Store a tensor with given extent */
int Spacing_Obj::Store_Tensor(double sp[SD][SD], double tlo[SD], double thi[SD]) {

  /* If snp+1 is greater than sp_dim, reallocate memory on space and construct
   * on each single new lot of space */
  if (nsp+1 >= sp_dim)
  {
    int old_dim = sp_dim;
    space = (Size_Obj*)realloc((void*)space,(sp_dim+Space_CHUNK)*sizeof(Size_Obj));
    if (space == 0)
    {
      fprintf(stderr,"\nStore_Tensor1: Unable to reallocate space array.");
      fflush(stderr);
      exit(0);
    }
    sp_dim += Space_CHUNK;
    for (int i=old_dim; i < sp_dim; i++)
      space[i].Construct();
  }
#if SPACE == 2
  int tdim = 3;
  double array[3];
  array[0] = sp[0][0];
  array[1] = sp[0][1];
  array[2] = sp[1][1];
#else
  int tdim = 6;
  double array[6];
  array[0] = sp[0][0];
  array[1] = sp[0][1];
  array[2] = sp[0][2];
  array[3] = sp[1][1];
  array[4] = sp[1][2];
  array[5] = sp[2][2];
#endif
  Size_Obj *ptr = new Size_Obj(tdim,array,tlo,thi);
  space[nsp] = *ptr;
  delete ptr;
  
  return(nsp++);
}

/* Store a tensor with given element */
int Spacing_Obj::Store_Tensor(double sp[SD][SD], int tnv, double tvert[][SD]) {

  /* If snp+1 is greater than sp_dim, reallocate memory on space and construct
   * on each single new lot of space */
  if (nsp+1 >= sp_dim)
  {
    int old_dim = sp_dim;
    space = (Size_Obj*)realloc((void*)space,(sp_dim+Space_CHUNK)*sizeof(Size_Obj));
    if (space == 0)
    {
      fprintf(stderr,"\nStore_Tensor2: Unable to reallocate space array.");
      fflush(stderr);
      exit(0);
    }
    sp_dim += Space_CHUNK;
    for (int i=old_dim; i < sp_dim; i++)
      space[i].Construct();
  }
#if SPACE == 2
  int tdim = 3;
  double array[3];
  array[0] = sp[0][0];
  array[1] = sp[0][1];
  array[2] = sp[1][1];
#else
  int tdim = 6;
  double array[6];
  array[0] = sp[0][0];
  array[1] = sp[0][1];
  array[2] = sp[0][2];
  array[3] = sp[1][1];
  array[4] = sp[1][2];
  array[5] = sp[2][2];
#endif
  Size_Obj *ptr = new Size_Obj(tdim,array,tnv,tvert);
  space[nsp] = *ptr;
  delete ptr;
  
  return(nsp++);
}

/* Retrieve specific item */
int Spacing_Obj::Retrieve_Tensor_Item(int n,
                                      double rmt[SD][SD],
                                      int &nvrt,
                                      double vert[][SD]) {
  int i, j;

  if (n < 0 || n >= nsp)
     return(0);

  nvrt = space[n].nv;
  for (i=0; i < space[n].nv; i++)
    for (j=0; j < SD; j++)
      vert[i][j] = space[n].vert[i][j];

  if (space[n].sdim == 1)
    Scalar_To_Tensor(space[n].rmt[0],rmt);
  else
    Full_Tensor(space[n].rmt, rmt);
  
  return(1);
}

/*  Compute a Riemannian Metric */
void Spacing_Obj::Compute_Riemannian_Metric(double v1[SD], double v2[SD], double v3[SD],
                                           double h1, double h2, double h3, double RR[SD][SD]) {

  double RI[SD][SD], Rl[SD][SD];
  double lam[SD][SD];
  int ir, jc;

//    printf( "      %24.16e, %24.16e >> %24.16e, %24.16e\n",
//            v1[0], v1[1], v2[0], v2[1] );
  /* This algorithm gives back: ( R X lambda X Rt ) */

  RR[0][0] = v1[0];
  RR[1][0] = v1[1];
  RR[0][1] = v2[0];
  RR[1][1] = v2[1];
  RI[0][0] = v1[0];
  RI[0][1] = v1[1];
  RI[1][0] = v2[0];
  RI[1][1] = v2[1];
  lam[0][0] = 1.0/h1/h1;
  lam[0][1] = 0.0;
  lam[1][0] = 0.0;
  lam[1][1] = 1.0/h2/h2;
#if SPACE ==  3
  RR[0][2] = v3[0];
  RR[1][2] = v3[1];
  RR[2][0] = v1[2];
  RR[2][1] = v2[2];
  RR[2][2] = v3[2]; 
  RI[0][2] = v1[2];
  RI[1][2] = v2[2];
  RI[2][0] = v3[0];
  RI[2][1] = v3[1];
  RI[2][2] = v3[2];
  lam[0][2] = 0.0;
  lam[1][2] = 0.0;
  lam[2][0] = 0.0;
  lam[2][1] = 0.0;
  lam[2][2] = 1.0/h3/h3;
#endif

  for (jc=0; jc < SD; jc++) {
    for (ir=0; ir < SD; ir++) {
#if SPACE == 2
      Rl[ir][jc] = RR[ir][0] * lam[0][jc] +
                   RR[ir][1] * lam[1][jc];
#else
      Rl[ir][jc] = RR[ir][0] * lam[0][jc] +
                   RR[ir][1] * lam[1][jc] +
                   RR[ir][2] * lam[2][jc];
#endif
    }
  }

  for (jc=0; jc < SD; jc++) {
    for (ir=0; ir < SD; ir++) {
#if SPACE == 2
      RR[ir][jc] = Rl[ir][0] * RI[0][jc] +
                   Rl[ir][1] * RI[1][jc];
#else
      RR[ir][jc] = Rl[ir][0] * RI[0][jc] +
                   Rl[ir][1] * RI[1][jc] +
                   Rl[ir][2] * RI[2][jc];
#endif
    }
  }

}

void Spacing_Obj::Decompose_Tensor(double RMT[SD][SD],
                                   double left[SD][SD],
                                   double right[SD][SD],
                                   double lam[SD]) {

  int j;
  double **atmp, **vtmp, *wtmp;
  /* Create 4x4's */
  atmp = new double*[SD+1];
  vtmp = new double*[SD+1];
  for (j=0; j < SD+1; j++)
  {
    atmp[j] = new double[SD+1];
    vtmp[j] = new double[SD+1];
  }
  /* Create line of 4 */
  wtmp = new double[SD+1];

  // store existing matrix, on last 3x3 part of matrix
  atmp[1][1] = RMT[0][0];
  atmp[1][2] = RMT[0][1];
  atmp[2][1] = RMT[1][0];
  atmp[2][2] = RMT[1][1];
#if SPACE == 3
  atmp[1][3] = RMT[0][2];
  atmp[2][3] = RMT[1][2];
  atmp[3][1] = RMT[2][0];
  atmp[3][2] = RMT[2][1];
  atmp[3][3] = RMT[2][2];
#endif

  /* Singular value decomposition */
  if (svdcmp(atmp,SD,SD,wtmp,vtmp))
  {
    fprintf(stderr,"\nError decomposing matrix!\n");
    for (int i=0; i < SD; i++)
#if SPACE == 2
      fprintf(stderr,"\n  %14.7e %14.7e",RMT[i][0],RMT[i][1]);
#else
      fprintf(stderr,"\n  %14.7e %14.7e %14.7e",RMT[i][0],RMT[i][1],RMT[i][2]);
#endif
    fprintf(stderr,"\n");
    fflush(stderr);
    double off, diag;
    off = diag = 0.0;
#if SPACE == 2
    diag = sqrt(RMT[0][0]*RMT[0][0]+RMT[1][1]*RMT[1][1]);
    off = sqrt(RMT[0][1]*RMT[0][1]+RMT[0][2]*RMT[0][2]);
#else
    diag = sqrt(RMT[0][0]*RMT[0][0]+RMT[1][1]*RMT[1][1]+RMT[2][2]*RMT[2][2]);
    off = sqrt(RMT[0][1]*RMT[0][1]+RMT[0][2]*RMT[0][2]+RMT[1][2]*RMT[1][2]);
#endif
    if (diag > 10*off)
    {
      fprintf(stderr,"\nStoring Cartesian aligned tensor!");
      fflush(stderr);
      vtmp[1][1] = atmp[1][1] = 1.0;
      vtmp[1][2] = atmp[1][2] = 0.0;
      vtmp[2][1] = atmp[2][1] = 0.0;
      vtmp[2][2] = atmp[2][2] = 1.0;
      wtmp[1] = RMT[0][0];
      wtmp[2] = RMT[1][1];
#if SPACE == 3
      vtmp[1][3] = atmp[1][3] = 0.0;
      vtmp[2][3] = atmp[2][3] = 0.0;
      vtmp[3][1] = atmp[3][1] = 0.0;
      vtmp[3][2] = atmp[3][2] = 0.0;
      vtmp[3][3] = atmp[3][3] = 1.0;
      wtmp[3] = RMT[2][2];
#endif
    } else
      exit(0);
  }

  // store left eigenvectors
  left[0][0] = atmp[1][1];
  left[0][1] = atmp[1][2];
  left[1][0] = atmp[2][1];
  left[1][1] = atmp[2][2];

  // store right eigenvectors
  right[0][0] = vtmp[1][1];
  right[0][1] = vtmp[1][2];
  right[1][0] = vtmp[2][1];
  right[1][1] = vtmp[2][2];

  // eigenvalues
  lam[0] = wtmp[1];
  lam[1] = wtmp[2];

#if SPACE == 3
  // store left eigenvectors
  left[0][2] = atmp[1][3];
  left[1][2] = atmp[2][3];
  left[2][0] = atmp[3][1];
  left[2][1] = atmp[3][2];
  left[2][2] = atmp[3][3];
  // store right eigenvectors
  right[0][2] = vtmp[1][3];
  right[1][2] = vtmp[2][3];
  right[2][0] = vtmp[3][1];
  right[2][1] = vtmp[3][2];
  right[2][2] = vtmp[3][3];
  // eigenvalues
  lam[2] = wtmp[3];
#endif
  for (j=0; j < SD+1; j++)
  {
    delete[] atmp[j];
    delete[] vtmp[j];
  }
  delete[] atmp;
  delete[] vtmp;
  delete[] wtmp;
}
 
// Determine metric length for given edge, using tensor supplied
double Spacing_Obj::Metric_Length(double v[SD], double rmt[SD][SD])
{
  int i;
  double mag, tmp[SD];

  // perform vector * matrix * vector multiplication
  tmp[0]=tmp[1]=0.0;
#if SPACE == 3
  tmp[2]=0.0; 
#endif
  for (i=0; i < SD; i++) {
    tmp[i] = v[0] * rmt[0][i] +
             v[1] * rmt[1][i];
#if SPACE == 3
    tmp[i] += v[2] * rmt[2][i];
#endif
  }
#if SPACE == 2
  mag = tmp[0]*v[0]+tmp[1]*v[1];
#else
  mag = tmp[0]*v[0]+tmp[1]*v[1]+tmp[2]*v[2];
#endif

  // return metric length
  return(sqrt(fabs(mag)));
}

#if SPACE == 2

int Spacing_Obj::Import_Spacing( char fname[] ) {

  /*** Variables ***/
  FILE * fp;
  int n, i, j, nVert, nTensors;
  const int bdim = 4096;
  char buff[bdim];
  double v1, v2, v3, distGlobal;
  double vert[100000][2], rmnt[2][2], root_lo[2], root_hi[2], dist;
  root_lo[0] = root_lo[1] =  1.0e20;
  root_hi[0] = root_hi[1] = -1.0e20;

  /* Opening file */
  if( ( fp = fopen( fname, "r" ) ) == NULL ) {
    printf( "ERROR: Unable to open tensor file <%s>\n", fname );
    return( 1 );
  }

  /* Reading number of tensors */
  fgets( buff, bdim, fp );
  fgets( buff, bdim, fp );
  sscanf( buff, "%d", &nTensors );
  fgets( buff, bdim, fp );

  /* Looping through entries */
  for( i = 0; i < nTensors; i++ ) {

    //if( i % 10 == 0 )  printf( "Item %d\n", i );
    /* Getting # vertices, and spacing info */
    fgets( buff, bdim, fp );
    sscanf( buff, "%d %d %lf %lf %lf", &nVert, &n, &v1, &v2, &v3 );

    /* Getting vertices coordinates */
    for( j = 0; j < nVert; j++ ) {
      fgets( buff, bdim, fp );
      sscanf( buff, "%lf %lf", &vert[j][0], &vert[j][1] );
      root_lo[0] = MIN( root_lo[0], vert[j][0] );
      root_lo[1] = MIN( root_lo[1], vert[j][1] );
      root_hi[0] = MAX( root_hi[0], vert[j][0] );
      root_hi[1] = MAX( root_hi[1], vert[j][1] );
    }

    /* Storing scalars in Spacing_Obj list */
    if( n == 1 ) {
      /* Storing polygons */
      if( nVert > 2 ) {
        Store_Scalar( v1, nVert, vert );
      }
      /* Storing simple extents */
      else{
        Store_Scalar( v1, vert[0], vert[1] );
      }
    }
    /* Storing tensors ind Spacing_Obj list */
    else {
      /* Passing values to matrix */
      rmnt[0][0] = v1;
      rmnt[0][1] = v2;
      rmnt[1][0] = v2;
      rmnt[1][1] = v3;
      /* Storing polygons */
      if( nVert > 2 ) {
        Store_Tensor( rmnt, nVert, vert );
      }
      /* Storing simple extents */
      else {
        Store_Tensor( rmnt, vert[0], vert[1] );
      }
    }

  }

  /* Closing file */
  fclose( fp );

  /* Making bounds of root greater than highest and lowest points */
  distGlobal = ( root_hi[0] - root_lo[0] ) / 10.0;
  distGlobal = MAX( distGlobal, ( root_hi[1] - root_lo[1] ) / 10.0 );
  dist       = distGlobal * 1.0e-5;
  root_lo[0] -= dist;
  root_lo[1] -= dist;
  root_hi[0] += dist;
  root_hi[1] += dist;

  printf( "IMPORT SPACING: Number of Sp    = %d\n", nsp );
  printf( "IMPORT SPACING: Sp Max Level    = %d\n", max_level );
  printf( "IMPORT SPACING: Sp Dimension    = %d\n", sp_dim );
  printf( "IMPORT SPACING: Distance        = %12.6e\n", dist );
  printf( "IMPORT SPACING: Global Distance = %12.6e\n", distGlobal );
  printf( "IMPORT SPACING: Low point       = ( %12.6e, %12.6e )\n", root_lo[0], root_lo[1] );
  printf( "IMPORT SPACING: High point     = ( %12.6e, %12.6e )\n", root_hi[0], root_hi[1] );
  /* Initializing root with calculated hi's lo's , global distance,and
   * max space level zero, and populating quadtree */
  Initialize_Root( root_lo, root_hi, distGlobal, 0 );
  Populate_Quadtree();
//  printf( "Segfaulting!\n" );

  return( 0 );

}

int Spacing_Obj::Export_Spacing( char fname[] ) {

  /*** Variables ***/
  int n, i, j, nVert;
  FILE * fp;

  /* Opening file with given name */
  if( ( fp = fopen( fname, "w" ) ) == NULL ) {
    printf( "ERROR: Unable to open tensor file <%s>\n", fname );
    return( 0 );
  }

  /* Writing number of tensors */
  fprintf( fp, "# number of tensors\n" );
  fprintf( fp, "%d\n", nsp );

  /* Printing to file: #tensors vert_x-y's tensors_1-2-3 */
  fprintf( fp, "# number of vertices, 0-scalar & 1-tensor, spacing info, next line: vertices coordinates\n" );
  for( n = 0; n < nsp; n++ ) {
    nVert = space[n].nv;
    /* Exporting tensor */
    if( space[n].sdim > 1 ) {
      fprintf( fp, "%5d %5d %24.14e %24.14e %24.14e\n",
               nVert, space[n].sdim, space[n].rmt[0], space[n].rmt[1], space[n].rmt[2] );
    }
    /* Exporting scalar */
    else {
      fprintf( fp, "%5d %5d %24.14e\n",
               nVert, space[n].sdim, space[n].rmt[0] );
    }

    for( i = 0; i < nVert; i++ ) {
      fprintf( fp, " %24.14e %24.14e\n",
               space[n].vert[i][0], space[n].vert[i][1] );
    }

  }

  /* Closing file */
  fclose( fp );

  return( 1 );

}

#else

#include <hdf5.h>

/* Export spacing data to HDF5 file */
int Spacing_Obj::Export_Spacing( char *fname,
                                 FILE *prnt ) {

  int i, j, k;
  char objname[32];
  int *itmp = 0;
  double *dtmp = 0;
  hid_t file_id, Obj_group_id;
  hid_t dspace_id, dset_id;
  hsize_t dim;
  herr_t status;

  // Initialize HDF
  status = H5open();

  // open hdf5 file, overwriting if necessary.
  file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
  if (file_id < 0)
  {
    fprintf(stderr,"\nError: unable to create file <%s>\n",fname);
    exit(0);
  }

  // create object name entry for later use
  sprintf(objname,"SF");
  Obj_group_id = H5Gcreate(file_id, objname, 0);

  dim = 2;
  itmp=(int*)realloc((void*)itmp,(int)dim*sizeof(int));
  itmp[0] = max_level;
  itmp[1] = nsp;
  dspace_id = H5Screate_simple(1,&dim,NULL);
  dset_id = H5Dcreate(Obj_group_id,"Parameters",H5T_NATIVE_INT,dspace_id,H5P_DEFAULT);
  status=H5Dwrite(dset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,itmp);
  status=H5Sclose(dspace_id);
  status=H5Dclose(dset_id);
  free(itmp);
  itmp=0;

  dim = 7;
  dtmp=(double*)realloc((void*)dtmp,(int)dim*sizeof(double));
  dtmp[0] = global_space;
  double glo[3], ghi[3];
  space_root->extents(glo,ghi);
  dtmp[1] = glo[0];
  dtmp[2] = glo[1];
  dtmp[3] = glo[2];
  dtmp[4] = ghi[0];
  dtmp[5] = ghi[1];
  dtmp[6] = ghi[2];
  dspace_id = H5Screate_simple(1,&dim,NULL);
  dset_id = H5Dcreate(Obj_group_id,"Global_Extents",H5T_NATIVE_DOUBLE,dspace_id,H5P_DEFAULT);
  status=H5Dwrite(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,dtmp);
  status=H5Sclose(dspace_id);
  status=H5Dclose(dset_id);
  free(dtmp);
  dtmp=0;

  itmp=(int*)realloc((void*)itmp,nsp*sizeof(int));

  dim = nsp;
  for (i=0; i < nsp; i++)
    itmp[i] = space[i].nv;
  dspace_id = H5Screate_simple(1,&dim,NULL);
  dset_id = H5Dcreate(Obj_group_id,"Vert_dims",H5T_NATIVE_INT,dspace_id,H5P_DEFAULT);
  status=H5Dwrite(dset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,itmp);
  status=H5Sclose(dspace_id);
  status=H5Dclose(dset_id);

  dim = nsp;
  for (i=0; i < nsp; i++)
    itmp[i] = space[i].sdim;
  dspace_id = H5Screate_simple(1,&dim,NULL);
  dset_id = H5Dcreate(Obj_group_id,"Tensor_dims",H5T_NATIVE_INT,dspace_id,H5P_DEFAULT);
  status=H5Dwrite(dset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,itmp);
  status=H5Sclose(dspace_id);
  status=H5Dclose(dset_id);
  free(itmp);
  itmp=0;

  dim = 0;
  for (i=0; i < nsp; i++)
    dim += space[i].nv;
  dim *= 3;
  dtmp = (double*)realloc((void*)dtmp,dim*sizeof(double));
  for (k=i=0; i < nsp; i++)
  {
    for (j=0; j < space[i].nv; j++)
    {
      dtmp[k++] = space[i].vert[j][0];
      dtmp[k++] = space[i].vert[j][1];
      dtmp[k++] = space[i].vert[j][2];
    }
  }
  dspace_id = H5Screate_simple(1,&dim,NULL);
  dset_id = H5Dcreate(Obj_group_id,"Vertices",H5T_NATIVE_DOUBLE,dspace_id,H5P_DEFAULT);
  status=H5Dwrite(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,dtmp);
  status=H5Sclose(dspace_id);
  status=H5Dclose(dset_id);
  free(dtmp);
  dtmp=0;

  dim = 0;
  for (i=0; i < nsp; i++)
    dim += space[i].sdim;
  dtmp = (double*)realloc((void*)dtmp,dim*sizeof(double));
  for (k=i=0; i < nsp; i++)
    for (j=0; j < space[i].sdim; j++)
      dtmp[k++] = space[i].rmt[j];
  dspace_id = H5Screate_simple(1,&dim,NULL);
  dset_id = H5Dcreate(Obj_group_id,"Tensors",H5T_NATIVE_DOUBLE,dspace_id,H5P_DEFAULT);
  status=H5Dwrite(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,dtmp);
  status=H5Sclose(dspace_id);
  status=H5Dclose(dset_id);
  free(dtmp);
  dtmp=0;

  status = H5Gclose(Obj_group_id);
  status = H5Fclose(file_id);
  status = H5close();

  return (0);

}


/* Import spacing data from tensor file */
int Spacing_Obj::Import_Spacing( char *fname,
                                 int div,
                                 int ndiv,
                                 FILE *prnt ) {

  double lo[3], hi[3];
  lo[0]=lo[1]=lo[2]= -1.0e20;
  hi[0]=hi[1]=hi[2]=  1.0e20;

  Import_Spacing(fname,div,ndiv,lo,hi,prnt);

}

/* Import spacing data from tensor file */
int Spacing_Obj::Import_Spacing( char *fname,
                                 int div,
                                 int ndiv,
                                 double lo[3],
                                 double hi[3],
                                 FILE *prnt ) {

  int i, j, k;
  char objname[32];
  int *itmp = 0;
  int *jtmp = 0;
  double *dtmp = 0;
  double *vtmp = 0;
  hid_t file_id, Obj_group_id;
  hid_t dset_id, vset_id, tset_id, file_space, mem_space;
  hsize_t dim, size[1], offset[1], stride[1], block[1], vlast, tlast, last;
  hssize_t off[2];
  herr_t status;

  // Initialize HDF
  status = H5open();

  // Open hdf5 file for reading
  file_id=H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);
  if (file_id < 0)
  {
    fprintf(stderr,"\nError opening file <%s>.",fname);
    exit(0);
  }

  sprintf( objname, "SF" );
  Obj_group_id = H5Gopen( file_id, objname );

  dim = 2;
  itmp = ( int* ) realloc ( ( void* ) itmp, ( int ) dim * sizeof( int ) );
  if( itmp == NULL )
  {
    fprintf(stderr,"\nSpacing_Object::Import_Spacing: Unable to allocate itmp for %d items for file %s\n",(int)dim,fname);
    fflush(stderr);
    exit(0);
  }
  dset_id = H5Dopen( Obj_group_id, "Parameters" );
  if( ( status = H5Dread( dset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, itmp ) ) < 0 )
  {
    fprintf(stderr,"\nError reading parameters.");
    exit(0);
  }
  status = H5Dclose ( dset_id );

  if( max_level == 0 ) // if not defined
    max_level = itmp[0]; // read max level from file
  int tnsp = itmp[1];

  free(itmp);
  itmp=0;

  double glo[3], ghi[3];
  dim = 7;
  dtmp=(double*)realloc((void*)dtmp,(int)dim*sizeof(double));
  if (dtmp == NULL)
  {
    fprintf(stderr,"\nSpacing_Object::Import_Spacing: Unable to allocate dtmp for %d items for file %s\n",(int)dim,fname);
    fflush(stderr);
    exit(0);
  }
  dset_id = H5Dopen(Obj_group_id,"Global_Extents");
  if ((status=H5Dread(dset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,dtmp)) < 0)
  {
    fprintf(stderr,"\nError reading global extents.");
    exit(0);
  }
  status=H5Dclose(dset_id);
  global_space = dtmp[0];
  glo[0] = dtmp[1];
  glo[1] = dtmp[2];
  glo[2] = dtmp[3];
  ghi[0] = dtmp[4];
  ghi[1] = dtmp[5];
  ghi[2] = dtmp[6];
  free(dtmp);
  dtmp=0;

  if (prnt)
  {
    fprintf(prnt,"\nReading Spacing Field Data:");
    fprintf(prnt,"\nNumber of entities = %d",tnsp);
  }

  div = MAX(1,MIN(div,ndiv)); // Just in case the user passed it wrong
  int slab_start, slab_end;
  int slab_inc = tnsp/ndiv+1;
  int sub_start, sub_end;
  int sub_inc = 1000000;
  last = vlast = tlast = 0;
  for (int slab = 1; slab <= div; slab++)
  {
    slab_start = last+1;
    slab_end = MIN(last+slab_inc,tnsp);

    int nsub = MAX(1,(slab_end-slab_start+1)/sub_inc + 1);
    for (int sub=0; sub < nsub; sub++)
    {
      sub_start = last+1;
      sub_end = MIN(last+MIN(slab_inc,sub_inc),MIN(slab_end,tnsp));
      int idim = sub_end-sub_start+1;
      itmp=(int*)realloc((void*)itmp,idim*sizeof(int));
      if (itmp == NULL)
      {
        fprintf(stderr,"\nSpacing_Object::Import_Spacing: Unable to allocate itmp for %d items for file %s\n",idim,fname);
        fflush(stderr);
        exit(0);
      }
      dim = idim;
      dset_id = H5Dopen(Obj_group_id,"Vert_dims");
      if (ndiv > 1 || nsub > 1)
      {
        offset[0] = 0;
        stride[0] = 1;
        size[0] = dim;
        block[0] = 1;
        mem_space = H5Screate_simple(1,&dim,NULL);
        H5Sselect_hyperslab(mem_space,H5S_SELECT_SET, offset, stride, size, block);

        offset[0] = last;
        stride[0] = 1;
        size[0] = dim;
        block[0] = 1;
        file_space = H5Dget_space(dset_id);
        H5Sselect_hyperslab(file_space,H5S_SELECT_SET, offset, stride, size, block);
        //off[0] = last+1;
        //H5Soffset_simple(file_space,off);

        status=H5Dread(dset_id,H5T_NATIVE_INT,mem_space,file_space,H5P_DEFAULT,itmp);
        H5Sclose(mem_space);
        H5Sclose(file_space);
      } else
        status=H5Dread(dset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,itmp);
      if (status < 0)
      {
        fprintf(stderr,"\nError reading Vert dim data.");
        exit(0);
      }
      status=H5Dclose(dset_id);

      jtmp=(int*)realloc((void*)jtmp,idim*sizeof(int));
      if (jtmp == NULL)
      {
        fprintf(stderr,"\nSpacing_Object::Import_Spacing: Unable to allocate jtmp for %d items for file %s\n",idim,fname);
        fflush(stderr);
        exit(0);
      }
      dset_id = H5Dopen(Obj_group_id,"Tensor_dims");
      if (ndiv > 1 || nsub > 1)
      {
        offset[0] = 0;
        stride[0] = 1;
        size[0] = dim;
        block[0] = 1;
        mem_space = H5Screate_simple(1,&dim,NULL);
        H5Sselect_hyperslab(mem_space,H5S_SELECT_SET, offset, stride, size, block);

        offset[0] = last;
        stride[0] = 1;
        size[0] = dim;
        block[0] = 1;
        file_space = H5Dget_space(dset_id);
        H5Sselect_hyperslab(file_space,H5S_SELECT_SET, offset, stride, size, block);
        //off[0] = last+1;
        //H5Soffset_simple(file_space,off);

        status=H5Dread(dset_id,H5T_NATIVE_INT,mem_space,file_space,H5P_DEFAULT,jtmp);
        H5Sclose(mem_space);
        H5Sclose(file_space);
      } else
        status=H5Dread(dset_id,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,jtmp);
      if (status < 0)
      {
        fprintf(stderr,"\nError reading Tensor dim data.");
        exit(0);
      }
      status=H5Dclose(dset_id);
      last = sub_end;

      dim = 0;
      for (i=0; i < idim; i++)
        dim += itmp[i];
      dim *= 3;
      vtmp = (double*)realloc((void*)vtmp,(int)dim*sizeof(double));
      if (vtmp == NULL)
      {
        fprintf(stderr,"\nSpacing_Object::Import_Spacing: Unable to allocate vtmp for %d items for file %s\n",(int)dim,fname);
        fflush(stderr);
        exit(0);
      }

      if (slab == div)
      {
        vset_id = H5Dopen(Obj_group_id,"Vertices");

        if (ndiv > 1 || nsub > 1)
        {
          offset[0] = 0;
          stride[0] = 1;
          size[0] = dim;
          block[0] = 1;
          mem_space = H5Screate_simple(1,&dim,NULL);
          H5Sselect_hyperslab(mem_space,H5S_SELECT_SET, offset, stride, size, block);

          offset[0] = vlast;
          stride[0] = 1;
          size[0] = dim;
          block[0] = 1;
          file_space = H5Dget_space(vset_id);
          H5Sselect_hyperslab(file_space,H5S_SELECT_SET, offset, stride, size, block);
          //off[0] = vlast+1;
          //H5Soffset_simple(file_space,off);
        
          status=H5Dread(vset_id,H5T_NATIVE_DOUBLE,mem_space,file_space,H5P_DEFAULT,vtmp);
          H5Sclose(mem_space);
          H5Sclose(file_space);
        } else
          status=H5Dread(vset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,vtmp);
        if (status < 0)
        {
          fprintf(stderr,"\nError reading Vertices data.");
          exit(0);
        }
        status=H5Dclose(vset_id);
      }
      vlast += dim;

      dim = 0;
      for (i=0; i < idim; i++)
        dim += jtmp[i];
      dtmp = (double*)realloc((void*)dtmp,(int)dim*sizeof(double));
      if (dtmp == NULL)
      {
        fprintf(stderr,"\nSpacing_Object::Import_Spacing: Unable to allocate dtmp for %d items for file %s\n",(int)dim,fname);
        fflush(stderr);
        exit(0);
      }

      if (slab == div)
      {
        tset_id = H5Dopen(Obj_group_id,"Tensors");

        if (ndiv > 1 || nsub > 1)
        {
          offset[0] = 0;
          stride[0] = 1;
          size[0] = dim;
          block[0] = 1;
          mem_space = H5Screate_simple(1,&dim,NULL);
          H5Sselect_hyperslab(mem_space,H5S_SELECT_SET, offset, stride, size, block);

          offset[0] = tlast;
          stride[0] = 1;
          size[0] = dim;
          block[0] = 1;
          file_space = H5Dget_space(tset_id);
          H5Sselect_hyperslab(file_space,H5S_SELECT_SET, offset, stride, size, block);
          //off[0] = tlast+1;
          //H5Soffset_simple(file_space,off);

          status=H5Dread(tset_id,H5T_NATIVE_DOUBLE,mem_space,file_space,H5P_DEFAULT,dtmp);
          H5Sclose(mem_space);
          H5Sclose(file_space);
        } else
          status=H5Dread(tset_id,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,dtmp);
        if (status < 0)
        {
          fprintf(stderr,"\nError reading Tensor data.");
          exit(0);
        }
        status=H5Dclose(tset_id);
      }
      tlast += dim;

      if (slab == div)
      {
        double sp, rt[3][3];
        double (*vert)[3];

        int v = 0;
        int t = 0;
        for (i=0; i < idim; i++)
        {
          vert = new double[itmp[i]][3];
          if (vert == 0)
          {
            fprintf(stderr,"\nSpacing_Object::Import_Spacing: Unable to allocate vert for %d items for file %s\n",itmp[i],fname);
            fflush(stderr);
            exit(0);
          }
          double tlo[3], thi[3];
          tlo[0]=tlo[1]=tlo[2]=1.0e20;
          thi[0]=thi[1]=thi[2]=-1.0e20;
          for (j=0; j < itmp[i]; j++)
          {
            vert[j][0] = vtmp[v++];
            vert[j][1] = vtmp[v++];
            vert[j][2] = vtmp[v++];
            tlo[0] = MIN(tlo[0],vert[j][0]);
            tlo[1] = MIN(tlo[1],vert[j][1]);
            tlo[2] = MIN(tlo[2],vert[j][2]);
            thi[0] = MAX(thi[0],vert[j][0]);
            thi[1] = MAX(thi[1],vert[j][1]);
            thi[2] = MAX(thi[2],vert[j][2]);
          }

          // perform extent test on coordinates
          bool store_flag = true;
          if (tlo[0] > hi[0] || tlo[1] > hi[1] || tlo[2] > hi[2] ||
              thi[0] < lo[0] || thi[1] < lo[1] || thi[2] < lo[2])
            store_flag = false;

          if (jtmp[i] == 1)
          {
            sp = dtmp[t++];
            if (store_flag)
            {
              if (itmp[i] == 2)
                Store_Scalar(sp,vert[0],vert[1]);
              else
                Store_Scalar(sp,itmp[i],vert);
            }
          } else
          {
            rt[0][0] = dtmp[t++];
            rt[0][1] = dtmp[t++];
            rt[0][2] = dtmp[t++];
            rt[1][1] = dtmp[t++];
            rt[1][2] = dtmp[t++];
            rt[2][2] = dtmp[t++];
            rt[1][0] = rt[0][1];
            rt[2][0] = rt[0][2];
            rt[2][1] = rt[1][2];
            if (store_flag)
            {
              if (itmp[i] == 2)
                Store_Tensor(rt,vert[0],vert[1]);
              else
                Store_Tensor(rt,itmp[i],vert);
            }
          }

          delete[] vert;

        }
      }
    }
  }

  status = H5Gclose(Obj_group_id);
  status = H5Fclose(file_id);
  status = H5close();

  free(itmp); itmp=0;
  free(jtmp); jtmp=0;
  free(dtmp); dtmp=0;
  free(vtmp); vtmp=0;

  if (prnt)
  {
    fprintf(prnt,"\nGlobal limits:");
    fprintf(prnt,"\n  %lg < X < %lg",glo[0],ghi[0]);
    fprintf(prnt,"\n  %lg < X < %lg",glo[1],ghi[1]);
    fprintf(prnt,"\n  %lg < X < %lg",glo[2],ghi[2]);
    fprintf(prnt,"\nGlobal spacing = %lg",global_space);
  }

  Initialize_Root(glo,ghi,global_space,max_level);
  Populate_Octree();

  return (0);

}
#endif

