/* THIS IS THE 2D FRIENDLY VERSION OF Spacing_Obj.h */
#include<stdio.h>
#include<stdlib.h>
#include"Quadtree_Storage.h"
#include"SUtil.h"
#include"Space.h"

/* Used for testing, TO BE DELETED */
//double * mi_matriz;

#ifndef Spacing_obj_h
#define Spacing_obj_h

/* This class holds ? */
class Size_Obj
{
 public:
  int sdim;// size dimension?
  double *rmt;// Riemannian tensor, a matrix
  int nv;// number of vertex
  double (*vert)[SD];  // modified vertex array for extent box or CGNS standard elements

  /* Constructor, set all pointers and variables to NULL */
  Size_Obj()
  {
    sdim = 0;
    rmt = 0;
    nv = 0;
    vert = 0;
  }
  /* Constructor for a point, takes variables low and hi coordinates */
  Size_Obj(int tdim, double trmt[], double tlo[SD], double thi[SD])
  {
    int i;
    sdim = tdim;
    nv = 2;// lowest and highest points?
    vert = new double[nv][SD];
    for (i=0; i < SD; i++)
    {
      vert[0][i] = tlo[i];
      vert[1][i] = thi[i];
    }
    rmt = new double[sdim];
    for (i=0; i < sdim; i++)
      rmt[i] = trmt[i];
  }
  /* Constructor for a cell, takes vertex and number of vertex */
  Size_Obj(int tdim, double trmt[], int tnv, double tvert[][SD])
  {
    int i, j;
    nv = tnv;
    vert = new double[nv][SD];
    for (i=0; i < nv; i++)
      for (j=0; j < SD; j++)
        vert[i][j] = tvert[i][j];
    sdim = tdim;
    rmt = new double[sdim];
    for (i=0; i < sdim; i++)
      rmt[i] = trmt[i];
  }
  /* Construct function, same as first constructor */
  void Construct()
  {
    sdim = 0;
    rmt = 0;
    nv = 0;
    vert = 0;
  }
  /* Destructor, clean up memory, set pointers to NULL */
  ~Size_Obj()
  {
    delete[] rmt;
    rmt=0;
    delete[] vert;
    vert = 0;
    nv = 0;
    sdim = 0;
  }
  /* Destructor functions, does same as destructor */
  void Destruct()
  {
    delete[] rmt;
    rmt=0;
    delete[] vert;
    vert = 0;
    nv = 0;
    sdim = 0;
  }
  /* Another constructor, takes entire Size_Obj object */
  Size_Obj(Size_Obj& s) // copy constructor
  {
    int i, j;
    nv=s.nv;
    vert = new double[nv][SD];
    for (i=0; i < nv; i++)
      for (j=0; j < SD; j++)
        vert[i][j] = s.vert[i][j];
    sdim = s.sdim;
    rmt = new double[sdim];
    for (i=0; i < sdim; i++)
      rmt[i] = s.rmt[i];
  }
  /* Copies object of right hand side of = sign */
  Size_Obj& operator=(Size_Obj& s) // operator = for use when LHS already exists
  {
    int i, j;
    if( vert != 0 ) delete[] vert; vert=0; // free up old memory
    nv=s.nv;
    vert = new double[nv][SD];//           ***
    for (i=0; i < nv; i++)
      for (j=0; j < SD; j++)
        vert[i][j] = s.vert[i][j];
    if (rmt != 0) delete[] rmt; rmt=0; // free up old memory
    sdim = s.sdim;
    rmt = new double[sdim];//              ***
    for (i=0; i < sdim; i++)
      rmt[i] = s.rmt[i];
    return *this;
  }
};

#define Space_CHUNK 100000

class Spacing_Obj
{
  private:
  //int nsp, sp_dim;//number of spacers
  //int max_level;
  //double global_space;
  //Size_Obj *space;//size object
  Quadtree_Storage *space_root;//2D tree root storage

  public:
  /* Constructor */
  int nsp, sp_dim;//number of spacers
  int max_level;
  double global_space;
  Size_Obj *space;//size object
  Spacing_Obj()
  {
    nsp = sp_dim = max_level = 0;
    global_space = 1.0e20;
    space = 0;
    space_root = 0;
  }
  /* Destructor, clean up memory */
  ~Spacing_Obj()
  {
    if (space != 0) {
      for( int i = 0; i < sp_dim; i++ ) {
        space[i].Destruct();
      }
      free( space );
    }
    space = 0;
    if (space_root != 0)
      delete space_root;
    space_root = 0;
    nsp = sp_dim = max_level = 0;
    global_space = 0.0;
  }
  /* Initialize root using lo, hi, global space, level, smn? */
  //Now defined below, and defined in cpp file
/* Initialize root using lo, hi, global space, level, smn? */
//void Initialize_Root(double lo[SD], double hi[SD],  double gsp, int lvl=0, double smn=0.0)
//{
//
//#if SPACE == 2
//  Quadtree_Storage *dummy=0;
//  space_root = new Quadtree_Storage((Quadtree_Storage*)dummy,lo,hi,lvl,smn); // Create root octant using extents
//#else
//  Octree_Storage *dummy=0;
//  space_root = new Octree_Storage((Octree_Storage*)dummy,lo,hi,lvl,smn); // Create root octant using extents
//#endif
//  global_space = gsp;
//  max_level = lvl;
//  printf( "* * * Debugging!\n" );
//}

  /* Returning number of nsp */
  int Number_Of_Spaces() {
    return( nsp );
  }

  /* Returning component of tensor */
  double Tensor_Component( int tIndex, int tComp ) {
    return( space[tIndex].rmt[tComp] );
  }

  /* Returning lo vertex of element */
  double Tensor_Vert( int tIndex, int vertex, int vComp ) {
    // vertex: 0 lo, 1 hi, or actual vertex number
    // vComp: 0 x, 1 y, 2 z, if working in 3D
    return( space[tIndex].vert[vertex][vComp] );
  }

  void Initialize_Root(double lo[SD],
                       double hi[SD],
                       double gsp,
                       int lvl,
                       double smn);

  int Point_In_Element(double pt[SD],
                       Size_Obj *ptr); // Is point in element

  double Edge_In_Element(double node1[SD],
                         double node2[SD],
                         Size_Obj *ptr); // Compute portion of edge in element

  int Number_of_Entries(); // Return number of items stored in system

  double Retrieve_Scalar(double pt[SD]); // Retrieve a scalar given a point

  double Retrieve_Edge_Scalar(double p1[SD],
                              double p2[SD]); // Retrieve a scalar given an edge

  void Scalar_To_Tensor(double sp,
                        double rmt[SD][SD]); // Make scalar into uniform tensor

  double Compute_Edge_Metric_Length(double p1[SD],
                                    double p2[SD],
                                    int type); // Metric length for given edge

  int Retrieve_Tensor(double pt[SD],
                      double rmt[SD][SD]); // Retrieve a tensor given a point

  int Retrieve_Edge_Tensor(double p1[SD],
                           double p2[SD],
                           double rmt[SD][SD]); // Retrieve a tensor given an edge

  int Retrieve_Tensor_Item(int n,
                           double rmt[SD][SD],
                           int &nvrt,
                           double vert[][SD]); // Retrieve specific item

  int Store_Scalar(double sp,
                   double tlo[SD],
                   double thi[SD]); // Store a scalar with given extent

  int Store_Scalar(double sp,
                   int tv,
                   double tvert[][SD]); // Store a scalar with given cell element

  int Store_Tensor(double sp[SD][SD],
                   double tlo[SD],
                   double thi[SD]); // Store a tensor with given extent

  int Store_Tensor(double sp[SD][SD],
                   int tv,
                   double tvert[][SD]); // Store a tensor with given cell element

  double Metric_Length(double v[SD],
                       double rmt[SD][SD]); // Compute metric length of vector given tensor

  void Compute_Riemannian_Metric(double v1[SD],
                                 double v2[SD],
                                 double v3[SD],
                                 double h1,
                                 double h2,
                                 double h3,
                                 double rmt[SD][SD]);

  void Decompose_Tensor(double RMT[SD][SD],
                        double left[SD][SD],
                        double right[SD][SD],
                        double lam[SD]);

  bool Combine_Tensors(double t1[SD][SD],
                       double t2[SD][SD],
                       double t[SD][SD]); // Combine tensors * * * * * *

  double Compare_Tensors(double t1[SD][SD],
                         double t2[SD][SD]); // Compare tensors

  void Full_Tensor(double sp[],
                   double rmt[SD][SD]); // Expand compressed tensor to full matrix

  int Populate_Octree();

  int Populate_Quadtree();

  void Optimize_Field( int mode,
                       int * t_nCells,// Merge extents
                       int ** t_c2n_count,
                       int *** t_cell_to_node,
                       int * t_nNodes,
                       double ** t_x,
                       double ** t_y,
                       double mergeThresh,
                       double ** t_dumb_array,
                       Spacing_Obj ** t_a_t );

//  void Optimize_Field(int mode);
  
#if SPACE == 2

  int Export_Spacing( char fname[] ); // Export spacing data to .tensor file

  int Import_Spacing( char fname[] ); // Import spacing data from .tensor file

//  int Import_Spacing( char *fname, 
//                      double lo[2], 
//                      double hi[2]  ); // Import spacing data from .tensor file

#else

  int Export_Spacing(char *fname,
                     FILE *prnt=0); // Export spacing data to HDF5 file

  int Import_Spacing(char *fname,
                     int div,
                     int ndiv,
                     FILE *prnt=0); // Import spacing data from HDF5 file

  int Import_Spacing( char *fname, 
                      int div, 
                      int ndiv, 
                      double lo[3], 
                      double hi[3],
                      FILE *prnt=0 ); // Import spacing data from HDF5 file
#endif
};
#endif

