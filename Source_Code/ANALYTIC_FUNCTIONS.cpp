/* This file contains functions for calculation of analytic functions used for
 * calculating tensors in 2D. It is always assumed that there exist 4 variables */
#include"ANALYTIC_FUNCTIONS.h"
#define GAMMA 1.4

/***************************************************************************/
/*                            Velocity Magnitude                           */
/***************************************************************************/
void Velocity_Magnitude( int nNodes,
                         double * Q,
                         double * vel_mag ) {

  int i;

  /* Pythagorean of x- and y-momentum divided by square of density */
  for( i = 0; i < nNodes; i++ ) {
    vel_mag[i] = sqrt( Q[i*4+1]*Q[i*4+1] + Q[i*4+2]*Q[i*4+2] ) / Q[i*4+0];
  }

}

/***************************************************************************/
/*                               Pressure                                  */
/***************************************************************************/
void Pressure( int nNodes,
               double * Q,
               double * pressure ) {

  int i;

  /* P = ( gamma - 1 ) * rho * ( et - 1/2 * ( u^2 + v^2 ) ) */
  for( i = 0; i < nNodes; i++ ) {
    pressure[i] = ( GAMMA - 1.0 ) * ( Q[i*4+3] -
           0.5 * ( Q[i*4+1]*Q[i*4+1] + Q[i*4+2]*Q[i*4+2] ) / Q[i*4+0] );
  }

}

/***************************************************************************/
/*                             Mach Number                                 */
/***************************************************************************/
void Mach( int nNodes,
           double * Q,
           double * mach ) {
  int i;
  double V, P, c;

  /* Repeating steps of velocity magnitude and pressure functions and adding
   * the calculation of the speed of sound and the mach number */
  for( i = 0; i < nNodes; i++ ) {
    V = sqrt( Q[i*4+1]*Q[i*4+1] + Q[i*4+2]*Q[i*4+2] ) / Q[i*4+0];
    P = ( GAMMA - 1.0 ) * ( Q[i*4+3] - 0.5 *
        ( Q[i*4+1]*Q[i*4+1] + Q[i*4+2]*Q[i*4+2] ) / Q[i*4+0] );
    c = sqrt( GAMMA * P / Q[i*4+0] );
    mach[i] = V / c;
  }
}


/***************************************************************************/
/*                              Density                                    */
/***************************************************************************/
void Density( int nNodes,
              double * Q,
              double * density ) {
  int i;
  double V, P, c;

  /* Repeating steps of velocity magnitude and pressure functions and adding
   * the calculation of the speed of sound and the mach number */
  for( i = 0; i < nNodes; i++ ) {
    density[i] = Q[4*i];
  }
}

