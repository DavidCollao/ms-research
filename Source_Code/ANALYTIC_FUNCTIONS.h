#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

#ifndef ANALYTIC_FUNCTIONS_H
#define ANALYTIC_FUNCTIONS_H

#if defined __CPLUSPLUS
extern "C" {
#endif

/* Velocity Magnitude */
void Velocity_Magnitude( int nNodes,
                         double * Q,
                         double * vel_mag );

/* Pressure */
void Pressure( int nNodes,
               double * Q,
               double * pressure );

/* Mach Number */
void Mach( int nNodes,
           double * Q,
           double * mach );

/* Density */
void Density( int nNodes,
              double * Q,
              double * density );

#if defined __CPLUSPLUS
}
#endif

#endif

