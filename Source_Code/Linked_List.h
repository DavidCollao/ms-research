#include <stdio.h>
#include <stdlib.h>

#ifndef Linked_List_h
#define Linked_List_h

class Linked_Node
{
  public:
  int data;
  Linked_Node *next;
  /* Constructor */
  Linked_Node(int i, Linked_Node *n) {
    data = i, next = n;
    // data = index? next = next bigger index node?
  }
};

class Linked_List
{
  public:
  Linked_Node *head;
  /* Constructor, set head pointer to NULL */
  Linked_List() { 
    head = 0;
  }
  /* Destructor, goes through each head and sets to current and saves each next
   * and deletes current */
  ~Linked_List() {
    Linked_Node *current = head;
    while (current) {
      Linked_Node *nxt = current->next;
      delete current;
      current = nxt;
    }
    head = 0;
  }

  /* Creates new Linked_Node, sets previous head to next and set itself as head */
  void Insert(int i) {
    head = new Linked_Node(i, head);
  }

  /* Counts number of links in list */
  int Length() {
    int l = 0;
    Linked_Node *current = head;
    while (current) {
      l++;
      current = current->next;
    }
    return(l);
  }

  /* Deletes link i. Looks for link, deletes and joins next of previous link
   * and head of next link as a new link */
  void Remove(int i) {
    Linked_Node *current = head;
    Linked_Node *prev = 0;
    while (current && current->data != i) {
      prev = current;
      current = current->next;
    }
    if (current && current->data == i) {
      Linked_Node *nxt = current->next;
      if (prev == 0)
        head = nxt;
      else
        prev->next = nxt;
      delete current;
    }
  }

  /* Looks for link of node i and replaces by node j */
  int Replace(int i, int j) {
    int flag = 0;
    Linked_Node *current = head;
    while (current && current->data != i) {
      current = current->next;
    }
    if (current && current->data == i) {
      current->data = j;
      flag = 1;
    }

    return (flag);
  }

  /* Looks for node i in links and returns flag */
  int In_list(int i) {
    int flag = 0;
    Linked_Node *current = head;
    while (current && current->data != i) {
      current = current->next;
    }
    if (current && current->data == i)
      flag = 1;

    return (flag);
  }

};

#endif
