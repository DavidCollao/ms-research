#include <stdio.h>
#include "PList.h"

/* PList is a class */

#define MIN(x,y) ((x) <= (y) ? (x) : (y))
#define MAX(x,y) ((x) >= (y) ? (x) : (y))

/* Points List Redimension */
bool PList::Redimension(int size) {

  /* If "size" <= zero,*/
  if (size <= 0) {
    /* if "list" not NULL, free it, set it to NULL; and set dim, max to zero*/
    if (list != NULL)
      free(list);
    list = NULL;
    dim = max = 0;
    return true;

  }
  /* If "size" different from "dim" */
  else if (size != dim) {
    /* if "list" not NULL, reallocate "list", "extent" to "size" */
    if (list != 0) {
      list=(void**)realloc((void*)list,size*sizeof(void*));
      extent=(EBox*)realloc((void*)extent,size*sizeof(EBox));
    }
    /* otherwise, allocate "list" and "extent" to size */
    else {
      list=(void**)malloc(size*sizeof(void*));
      extent=(EBox*)malloc(size*sizeof(EBox));
    }

    /* Check if memory was not allocated for "list" */
    if (list == 0) {
#ifdef VERBOSE_ERRORS
      fprintf(stderr,"Could not allocate space for list.");
#endif
      return false;
    }

    /* Set the new added elements in "list" to NULL */
    for (int i=max; i < size; i++)  list[i] = NULL;

    /* Set "dim" to size */
    dim = size;

    /* if dim < max, set max to dim */
    if (dim < max)  max = dim;

  }

  /* After allocating or reallocating memory for "list" and "extent" return
   * true */
  return true;
}

/* Function to check if point is in list */
bool PList::Is_In_List(void* n) {
  /* Initialize flag to false */
  int i;
  bool flag=false;
  /* Loop through points list array and if flag still false
   * if "n" is in list, turn flag to true*/
  for (i=0; i < max && flag==false; i++)
    if (list[i] == n)
      flag=true;

  return flag;
}

/* Count how many times "n" is in list */
int PList::Times_In_List(void* n) {

  int i, count;
  /* Initialize count to zero
   * Go through elements in points list
   * when "n" found in list, add the count */
  for (count=i=0; i < max; i++)
    if (list[i] == n)
      count++;

  return(count);
}

/* Function returns index in which "n" is located */
int PList::Index(void* n)
{
  int i, j;

  /* Set "j" to secure number -1 */
  j = -1;
  /* Go through points list array and while j still -1
   * if "n" is found, return the index in which it's found*/
  for (i=0; i < max && j < 0; i++)
    if (list[i] == n)
      j = i;

  return(j);
}

/* Function checks if n is in list. If not, it adds it */
bool PList::Check_List(void* n, double lo[SD], double hi[SD]) {
  /* Declaring variables and setting possible new dimension 5 units bigger than
   * current*/
  const int INC = 5;
  int new_dim;
  //new_dim = MIN(dim+100000,MAX(INC,dim*10));
  new_dim = dim+INC;
  /* If n is not in list
   * if "max" is greater or equal to "dim" */
  if (!Is_In_List(n)) {
    if (max >= dim) {
      /* if trying to redimension(list, extent to new_dim) returns false*/
      if (!Redimension(new_dim)) {
#ifdef VERBOSE_ERRORS
        fprintf(stderr,"Could not add to list.");
#endif
        /* Exit function returning false */
        return false;
      }
    }
    /* Put "n" in points list, add lo's, hi's in extent array, increase max by
     * one*/
    list[max] = n;
    extent[max] = EBox(lo,hi);
    max++;
  }

  /* If in list, return true */
  return true;
}

/* Adds element to points list */
bool PList::Add_To_List(void* n, double lo[SD], double hi[SD]) {
  /* Declaring variables and setting possible new dimension 5 units bigger than
   * current*/
  const int INC = 5;
  int new_dim;
  //new_dim = MIN(dim+100000,MAX(INC,dim*10));
  new_dim = dim+INC;
  /* If "max" still less than "dim", add n to list; and lo's, hi's to extent,
   * and increase max by one */
  if (max < dim) {
    list[max] = n;
    extent[max] = EBox(lo,hi);
    max++;
  }
  /* Otherwise, redimension and do same as above */
  else if (Redimension(new_dim)) {
    list[max] = n;
    extent[max] = EBox(lo,hi);
    max++;
    return true;
  }
  /* If redimension above returned false, quit and return false */
  else {
#ifdef VERBOSE_ERRORS
    fprintf(stderr,"Could not add to list.");
#endif
    return false;
  }
  return true;

}

/* Deletes element from list */
bool PList::Delete_From_List(void* n) {
  /* Declare variable and set flag to false */
  int i, j;
  bool flag = false;
  /* Go through elements in list */
  for (i=0; i < max; i++)
    /* Check for element "n" in list */
    if (list[i] == n) {
      /* If found, copy the rest of the elements skipping the element found */
      for (j=i; j < max-1; j++) {
        list[j] = list[j+1];
        extent[j] = extent[j+1];
      }
      /* And decrease max by one and set flag to true, and break */
      max--;
      flag = true;
      break;
    }

  return flag;
}

/*  */
bool PList::Replace(void* m, void* n, double lo[SD], double hi[SD]) {
  /* Declare variables and set flag to false */
  int i;
  bool flag = false;
  /* Go through elements in point list */
  for (i=0; i < max; i++)
    /* If element "m" found in list, replace by n, and replace lo's, hi's in
     * extent, and set flag to true */
    if (list[i] == m) {
      list[i] = n;
      extent[i] = EBox(lo,hi);
      flag = true;
    }

  return flag;
}

