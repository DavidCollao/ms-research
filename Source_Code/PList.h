#include <stdio.h>
#include <stdlib.h>
#include"Space.h"

/* Changing file to take 2D options too */
#ifndef PList_h
#define PList_h

/* Definition of EBox class */
class EBox {

  public:

  /* Declare the variables */
#if SPACE == 2
  double lo[2];
  double hi[2];
#else
  double lo[3];
  double hi[3];
#endif

  /* Constructor EBox: takes no arguments */
  EBox() {
    /* Set lo[3] = hi[3] = zero */
#if SPACE == 2
    lo[0]=lo[1]=hi[0]=hi[1]=0.0;
#else
    lo[0]=lo[1]=lo[2]=hi[0]=hi[1]=hi[2]=0.0;
#endif
  }

  /* Constructor EBox: takes initial values for lo[] and hi[] */
#if SPACE == 2
  EBox(double ilo[2], double ihi[2]) {
    /* Set lo[3] and hi[3] to the given arguments */
    lo[0]=ilo[0]; lo[1]=ilo[1];
    hi[0]=ihi[0]; hi[1]=ihi[1];
  }
#else
  EBox(double ilo[3], double ihi[3]) {
    /* Set lo[3] and hi[3] to the given arguments */
    lo[0]=ilo[0]; lo[1]=ilo[1]; lo[2]=ilo[2];
    hi[0]=ihi[0]; hi[1]=ihi[1]; hi[2]=ihi[2];
  }
#endif

};

/* Definition of class PList */
class PList {

  /* The only private variable */
  private:

  int dim;

  /* Declare the public variables */
  public:

  /* Declaring double pointer list, max in list, and EBox extent array */
  void **list;
  EBox *extent;
  int max;

  /* Defining the constructor */
  PList() {
    /* Set dim and max to zero, set list and extent to NULL */
    dim = max = 0; list = NULL; extent = NULL;
  }

  /* Defining the destructor */
  ~PList() {
    /* Set dim, max to zero */
    dim = max = 0;

    /* If list not NULL, free it and set it to NULL */
    if (list != NULL) {
      free(list);
      list = NULL;
    }

    /* If extent not NULL, free it and set it to NULL */
    if (extent != NULL) {
      free(extent);
      extent = NULL;
    }

  }

  /* Defining a constructor function */
  void construct() {
    /* Set dim, max to zero; set list, extent to NULL */
    dim = max = 0; list = NULL; extent = NULL;
  }

  /* Defining a destructor function */
  void destruct() {
    /* Set dim, max to zero */
    dim = max = 0;
    /* If list not NULL, free it and set it to NULL */
    if (list != NULL) {
      free(list);
      list = NULL;
    }
    /* If extent not NULL, free it and set it to NULL */
    if (extent != NULL) {
      free(extent);
      extent = NULL;
    }

  }

  /* Define a print dimension function */
  void print_dim(FILE *outf) {//takes file pointer
    /* print the dimension of the integer list "dim" on a file */
    fprintf(outf,"\nInteger list dimension =%d",dim);
  }
  /* Define a printing function */
  void print(FILE *outf) {//takes file pointer
    /* Calls "print_dim" function */
    print_dim(outf);
    /* Prints to a file max(#elements) in pointer list */
    fprintf(outf,"\nPointer list maximum index =%d",max);
    for (int i=0; i < max; i++) {
      /* Prints to file pointer list index, value in list, lo  and
       * hi values in box*/
#if SPACE == 2
      fprintf(outf,"\nlist(%d)= %p, box = (%lg,%lg) - (%lg,%lg)",i,list[i],
             extent[i].lo[0],extent[i].lo[1],
             extent[i].hi[0],extent[i].hi[1]);
#else
      fprintf(outf,"\nlist(%d)= %p, box = (%lg,%lg,%lg) - (%lg,%lg,%lg)",i,list[i],
             extent[i].lo[0],extent[i].lo[1],extent[i].lo[2],
             extent[i].hi[0],extent[i].hi[1],extent[i].hi[2]);
#endif
    }
  }

  /* Declare other PList functions dedined in PList.cpp */
  bool Redimension(int size);
  bool Is_In_List(void* n);
  int Times_In_List(void* n);
  int Index(void* n);
  bool Delete_From_List(void* n);
  bool Replace(void * m, void * n, double lo[SD], double hi[SD]);
  bool Check_List(void* n, double lo[SD], double hi[SD]);
  bool Add_To_List(void* n, double lo[SD], double hi[SD]);
 
};

#endif
