#include <stdio.h>
#include <math.h>
#include "Space.h"
/* SPACE defines 2D or 3D */
/* SPACE is defined in Space.h */

#ifndef Point_h
#define Point_h

/* Point class */
class Point {

  double pos[SPACE];

  /* pos = coordinates, declared above */
  public:
  /* Constructors for 2D and 3D coordinates. Dependent upon SPACE */
  #if SPACE == 2
    inline Point(double x=0.0, double y=0.0)
    { pos[0] = x; pos[1] = y; }
  #else
    inline Point(double x=0.0, double y=0.0, double z=0.0)
    { pos[0] = x; pos[1] = y; pos[2] = z; }
  #endif

  /* Destructor, does not need to do anything */
  ~Point() { }

  /* Print functions, print coordinates of points to file. Dependent upon
   * SPACE */
  #if SPACE == 2
  void print(FILE *outf) {

       fprintf(outf,"\nPoint (x,y)= (%.12g,%.12g)",pos[0],pos[1]);
  }
  #else
  void print(FILE *outf) {

       fprintf(outf,"\nPoint (x,y,z)= (%.12g,%.12g,%.12g)",pos[0],pos[1],pos[2]);
  }
  #endif

  /* Declaring overloaded operators */
  inline double & operator () (int);//FORTRAN MODE. Takes int, returns reference to a double
  inline double operator () (int) const;//Takes int, returns double, no mod on private variable
  inline double & operator [] (int);//Takes int, returns reference to a double
  inline Point operator * (double) const;//Takes double, returns Point, no mod on private variable
  inline Point operator / (double) const;//Takes double, returns Point, no mod on private variable
  inline Point operator + (const Point &) const;//Takes const reference to Point, returns Point, no mod on private variable
  inline Point operator - (const Point &) const;//Takes const reference to Point, returns Point, no mod on private variable inline Point &operator += (const Point &);//Takes const reference to Point, returns reference to Point
  inline Point &operator += (const Point &);//Takes const reference to Point, returns reference to Point
  inline Point &operator -= (const Point &);//Takes const reference to Point, returns reference to Point
  inline Point &operator += (double);//Takes double, returns reference to Point
  inline Point &operator -= (double);//Takes double, returns reference to Point
  inline Point &operator *= (double);//Takes double, returns reference to Point
  inline Point &operator /= (double);//Takes double, returns reference to Point
};

inline double & Point::operator () (int i)//used ( at left of = ) when wanting to assign a value to pos[i] 
{ return pos[i]; }

inline double Point::operator () (int i) const//used to return stored value
{ return pos[i]; }

inline double & Point::operator [] (int i)//used ( at left of = ) when wanting to assign a value to pos[i]
{ return pos[i]; }

#if SPACE == 2
    inline Point Point::operator * (double other) const 
    { return Point(pos[0]*other, pos[1]*other); }//returns new Point with such parameters
#else
    inline Point Point::operator * (double other) const 
    { return Point(pos[0]*other, pos[1]*other, pos[2]*other); }//returns new Point with such parameters
#endif

#if SPACE == 2
inline Point Point::operator / (double other) const 
{
#ifdef _DEBUG_
    if (fabs(other) <=1e-20) throw new mException (__LINE__,__FILE__);
#endif
    return Point(pos[0]/other, pos[1]/other);//returns new Point with such parameters
}
#else
inline Point Point::operator / (double other) const 
{
#ifdef _DEBUG_
	if (fabs(other) <=1e-20) throw new mException (__LINE__,__FILE__);
#endif
    return Point(pos[0]/other, pos[1]/other, pos[2]/other);//returns new Point with such parameters
} 
#endif

#if SPACE == 2
    inline Point Point::operator + (const Point &other) const 
    { return Point(pos[0]+other.pos[0], pos[1]+other.pos[1]); }

    inline Point Point::operator - (const Point &other) const 
    { return Point(pos[0]-other.pos[0], pos[1]-other.pos[1]); }
#else
    inline Point Point::operator + (const Point &other) const 
    { return Point(pos[0]+other.pos[0], pos[1]+other.pos[1], pos[2]+other.pos[2]); }

    inline Point Point::operator - (const Point &other) const 
    { return Point(pos[0]-other.pos[0], pos[1]-other.pos[1], pos[2]-other.pos[2]); }
#endif

inline Point& Point::operator += (const Point &other) 
{
	for (int i = 0; i < SPACE; i++)
          {
            pos[i]+=other.pos[i];
          }
	return *this;
}

inline Point& Point::operator -= (const Point &other) 
{
	for (int i = 0; i < SPACE; i++)
          {
            pos[i]-=other.pos[i];
          }
	return *this;
}

inline Point& Point::operator += (double other) 
{
	for (int i = 0; i < SPACE; i++)
          {
            pos[i]+=other;
          }
	return *this;
}

inline Point& Point::operator -= (double other) 
{
	for (int i = 0; i < SPACE; i++)
          {
            pos[i]-=other;
          }
	return *this;
}

inline Point& Point::operator *= (double other) 
{
	for (int i = 0; i < SPACE; i++)
          {
            pos[i]*=other;
          }
	return *this;
}

inline Point& Point::operator /= (double other) 
{
#ifdef _DEBUG_
	if (fabs(other) <=1e-20) throw new mException (__LINE__,__FILE__);
#endif
	for (int i = 0; i < SPACE; i++)
          {
            pos[i]/=other;
          }
	return *this;
}

//inline double distance(Point p1, Point p2)
//{
//  double dx = p2[0]-p1[0];
//  double dy = p2[1]-p1[1];
//  return(sqrt(dx*dx+dy*dy));
//}

#endif
