#include <stdio.h>
#include "Quadtree_Storage.h"

#define MIN(x,y) ((x) <= (y) ? (x) : (y))
#define MAX(x,y) ((x) >= (y) ? (x) : (y))

/* An octant is one of eight parts of the 2D Euclidean coordinate system */
/* This file defines the functions declared in Quadtree_Storage.h */

/* return extents of Octant, in tlo and thi */
void Quadtree_Storage::extents(double tlo[2],
                               double thi[2]) {

  int i;
  for (i=0; i < 2; i++) {
    tlo[i] = lo[i];
    thi[i] = hi[i];
  }
}

/* proceed down branches and return finest level */
int Quadtree_Storage::max_level(int lvl) {
  int i, j, l;

  l = 0;
  /* Go through kids */
  for (j=0; j < 2; j++)
    for (i=0; i < 2; i++)
      /* If kid found not NULL */
      if (kids[i][j] != 0)
        /* Get max level+1 of kids */
        l = MAX( l, kids[i][j]->max_level( lvl + 1 ) );

  /* The maximum level obtained gets compared with lvl and returned */
  return(MAX(l,lvl));
}

/* given point p and slop factor tol, proceed down tree and populate pointer list */
/* Takes point coordinates, tolerance, level, and a PList class */
bool Quadtree_Storage::retrieve_list( double p[2],
                                      double tol[2],
                                      int lvl,
                                      PList *flist ) {

  int i;
  double plo[2], phi[2];
  bool rflag;

  /* Set flag to true */
  rflag = true;

  /* compute extent box coordinates */
  for(i=0; i < 2; i++) {
    plo[i] = p[i] - tol[i];
    phi[i] = p[i] + tol[i];
  }

  /* perform extent test, if outside return to parent */
  /* If phi's lower than lo's, or plo's higher than hi's, return true */
  if(phi[0] < lo[0] || plo[0] > hi[0] ||
      phi[1] < lo[1] || plo[1] > hi[1])
    return rflag;//if extents of passed point is outside current box
                 //exit function true without adding to the counter

  /* if not, proceed further down tree, if possible */
  if (lvl <= 0 || level < lvl) {
    /* If flag still true, and kid pointer not NULL, call retrieve_list
     * recursively, for each kid */
    if (rflag == true && kids[0][0] != 0)
      rflag = kids[0][0]->retrieve_list(p,tol,lvl,flist);
    if (rflag == true && kids[1][0] != 0)
      rflag = kids[1][0]->retrieve_list(p,tol,lvl,flist);
    if (rflag == true && kids[0][1] != 0)
      rflag = kids[0][1]->retrieve_list(p,tol,lvl,flist);
    if (rflag == true && kids[1][1] != 0)
      rflag = kids[1][1]->retrieve_list(p,tol,lvl,flist);
  }

  /* add object from current octant to list */
  /* If lvl <= zero or equal to lvl */
  if (lvl <= 0 || level == lvl)
    for (i=0; i < objects->max && rflag == true; i++) {
      if (phi[0] < objects->extent[i].lo[0] || plo[0] > objects->extent[i].hi[0] ||
          phi[1] < objects->extent[i].lo[1] || plo[1] > objects->extent[i].hi[1])
        continue;
      /* Add to flist and return flag */
      rflag = flist->Add_To_List(objects->list[i],objects->extent[i].lo,objects->extent[i].hi);
    }

  return rflag;
}

/* Return finest octant size from tree for given point p */
double Quadtree_Storage::finest_size(double p[2]) {

  /* Initialize size to something huge */
  double size=1.0e20;

  /* If point coordinates out of domain, return huge size */
  if (p[0] < lo[0] || p[0] > hi[0] ||
      p[1] < lo[1] || p[1] > hi[1])
    return(size);

  /* If any of kids not NULL, get size by calling "finest_size" recursively, return
   * size on same variable "size" */
  if (kids[0][0] != 0)
    size=MIN(size,kids[0][0]->finest_size(p));
  if (kids[1][0] != 0)
    size=MIN(size,kids[1][0]->finest_size(p));
  if (kids[0][1] != 0)
    size=MIN(size,kids[0][1]->finest_size(p));
  if (kids[1][1] != 0)
    size=MIN(size,kids[1][1]->finest_size(p));

  /* If max in objects list is greater than zero */
  if (objects->max > 0) {
    /* Calculate the difference in hi's and lo's, and return the minimum of
     * them and size itself */
    double dx, dy;
    dx=hi[0]-lo[0];
    dy=hi[1]-lo[1];
    size=MIN(size,MIN(dx,dy));
  }

  /* return size */
  return(size);
}

/* process a list of nptr object pointers with extents, ilo and ihi, and store in tree */
void Quadtree_Storage::Store_In_Quadtree(int nptr,
                                         void* incoming[],
                                         double (*ilo)[2],
                                         double (*ihi)[2]) {

  int i, j, m, n;
  double mid[2], tlo[2], thi[2], dmin;
  void* *kp[2][2];
  double (*klo[2][2])[2], (*khi[2][2])[2];
  int kptr[2][2];//counter?

  /* compute size minimum
     compute mid-point coordinates */
  dmin=1e20;
  for (i=0; i < 2; i++) {
    mid[i] = (lo[i] + hi[i])*0.5;
    dmin = MIN(dmin,hi[i]-lo[i]);
  }
  dmin *= 0.5;


  /* initialize child pointers and counter */
  for (j=0; j < 2; j++)
    for (i=0; i < 2; i++) {
      kptr[i][j] = 0;
      kp[i][j] = 0;
      klo[i][j] = 0;
      khi[i][j] = 0;
    }

  /* Go through number of pointers, count number of pointers for each child */
  for (n=0; n < nptr; n++) {
    /* determine Quadrant to store object */
    /* This test depends on the box ( hi node, lo node ) containing tensor (
     * it's a grid position and element or node 'size' factor ). If tensor fits
     * in one of the quadrants, then it gets stored further in that quadrant.
     * But if extension of box falls on one of the lines defining the quadrants
     * or if the level_limit doesnt allow the tree to grow further, then the
     * tensor gets stored on that mother. */
    i = j = -1;
    if (ihi[n][0] < mid[0]) i=0;
    if (ilo[n][0] > mid[0]) i=1;
    if (ihi[n][1] < mid[1]) j=0;
    if (ilo[n][1] > mid[1]) j=1;

    /* Test for quadrant in which point fit with it's tols */
    if (i < 0 || j < 0 ||
        (size_limit > 1e-20 && dmin < size_limit) || 
        (level_limit > 0 && level+1 > level_limit)) continue;

    /* Augment counter */
    kptr[i][j]++;
  }

  /* Allocate space for children arrays and create child */
  /* Go through kids */
  for (j=0; j < 2; j++)
    for (i=0; i < 2; i++)
      /* If counter for this kid is greater than zero */
      if (kptr[i][j] > 0) {

        /* Allocate memory */
        kp[i][j] = new void*[kptr[i][j]];
        klo[i][j] = new double[kptr[i][j]][2];
        khi[i][j] = new double[kptr[i][j]][2];

        /* If kids pointer still NULL, ? */
        if (kids[i][j] == 0) {
          tlo[0] = (i > 0) ? mid[0] : lo[0];
          tlo[1] = (j > 0) ? mid[1] : lo[1];
          thi[0] = (i < 1) ? mid[0] : hi[0];
          thi[1] = (j < 1) ? mid[1] : hi[1];
          /* create child Octant */
          kids[i][j] = new Quadtree_Storage( this, tlo, thi, level_limit, size_limit );

        }
      }

  /* reset counter for each child */
  for (j=0; j < 2; j++)
    for (i=0; i < 2; i++)
      kptr[i][j] = 0;

  /* store data in arrays to pass to children */
  for (n=0; n < nptr; n++) {

    // determine Octant to store object
    i = j = -1;
    if (ihi[n][0] < mid[0]) i=0;
    if (ilo[n][0] > mid[0]) i=1;
    if (ihi[n][1] < mid[1]) j=0;
    if (ilo[n][1] > mid[1]) j=1;

    /* Test to see if quadrant was found */
    if (i < 0 || j < 0 ||
        (size_limit > 1e-20 && dmin < size_limit) || 
        (level_limit > 0 && level+1 > level_limit)) {
      /* store in current Octant */
      objects->Check_List(incoming[n],ilo[n],ihi[n]);
//      printf( "POPULATE QUADTREE: In level %d: %d( %.3lf, %.3lf )->( %.3lf, %.3lf )\n",
//              level, n+1, ihi[n][0], ihi[n][1], ilo[n][0], ilo[n][1] );
    }
    else {
      /* add to arrays for child */
      kp[i][j][kptr[i][j]] = incoming[n];
      for(m=0; m < 2; m++) {
        klo[i][j][kptr[i][j]][m] = ilo[n][m];
        khi[i][j][kptr[i][j]][m] = ihi[n][m];
      }
      kptr[i][j]++;
    }
  }

//  printf( "Segfaulting\n" );
  /* pass new data to children */
  for (j=0; j < 2; j++)
    for (i=0; i < 2; i++)
      if (kptr[i][j] > 0) {
        kids[i][j]->Store_In_Quadtree(kptr[i][j], kp[i][j],
                        klo[i][j], khi[i][j]);

        // clean up memory
        delete[] kp[i][j];
        delete[] klo[i][j];
        delete[] khi[i][j];
      }

}

/* given extents, ilo and ihi, store a single object pointers in tree */
void Quadtree_Storage::Store_In_Quadtree(void* incoming,
                                         double ilo[2],
                                         double ihi[2]) {

  int i, j;
  double mid[2], tlo[2], thi[2], dmin;

  /* compute mid-point coordinates */
  dmin=1e20;
  for (i=0; i < 2; i++)
  {
    mid[i] = (lo[i] + hi[i])*0.5;
    dmin = MIN(dmin,hi[i]-lo[i]);
  }
  dmin *= 0.5;
  
  /* determine Octant to store object */
  /* This test depends on the box ( hi node, lo node ) containing tensor (
   * it's a grid position and element or node 'size' factor ). If tensor fits
   * in one of the quadrants, then it gets stored further in that quadrant.
   * But if extension of box falls on one of the lines defining the quadrants
   * or if the level_limit doesnt allow the tree to grow further, then the
   * tensor gets stored on that mother. */
  i = j = -1;
  if (ihi[0] < mid[0]) i=0;
  if (ilo[0] > mid[0]) i=1;
  if (ihi[1] < mid[1]) j=0;
  if (ilo[1] > mid[1]) j=1;

  /* Test to see if quadrant was caught */
  if (i < 0 || j < 0 ||
      (size_limit > 1e-20 && dmin < size_limit) || 
      (level_limit > 0 && level+1 > level_limit))
    objects->Check_List(incoming,ilo,ihi); // store in current Octant
  else { // pass down tree

    /* If kid is NULL, create child octant */
    if (kids[i][j] == 0)
    {
      tlo[0] = (i > 0) ? mid[0] : lo[0];
      tlo[1] = (j > 0) ? mid[1] : lo[1];
      thi[0] = (i < 1) ? mid[0] : hi[0];
      thi[1] = (j < 1) ? mid[1] : hi[1];

      /* create child Octant */
      kids[i][j] = new Quadtree_Storage(this, tlo, thi, level_limit, size_limit);
    }

    /* pass to child Octant */
    kids[i][j]->Store_In_Quadtree(incoming, ilo, ihi);
  }

}

/* remove an object pointer from tree */
int Quadtree_Storage::Remove_From_Quadtree(void* incoming) {

  int i, j;
  int flag = 1;
  bool nokids;

  // status return flag:
  //     1 - item not found
  //     0 - item found and deleted
  //    -1 - item found and deleted, no more items so child can be deleted.

  if (objects->Delete_From_List(incoming)) // delete from current Octant
    flag = 0;

  /* not found yet, look further down tree */
  if (flag == 1) {

    /* Go through kids while flag = 1 */
    for (j=0; j < 2 && flag == 1; j++) {
      for (i=0; i < 2 && flag == 1; i++) {

        /* If kids not NULL, call remove from quadtree recursively and pass
         * incoming */
        if (kids[i][j] != 0) {

          /* This returns a flag too */
          flag = kids[i][j]->Remove_From_Quadtree(incoming);
          if (flag == -1) {// no more objects so delete child

            delete kids[i][j];
            kids[i][j] = 0;
            flag = 0;
          }
        }
      }
    }
  }

  /* nothing left, test for deletion */
  if (flag == 0 && objects->max == 0) {

    objects->Redimension(0); // free list memory
    nokids=true;
    for (j=0; j < 2 && nokids; j++)
      for (i=0; i < 2 && nokids; i++)
        if (kids[i][j] != 0) nokids = false;
    if (nokids && mom != 0)  // Cannot delete root octant!!!
      flag = -1; // set up for deletion
  }

  return(flag);
}

/* given extent with object, remove an object pointer from tree */
int Quadtree_Storage::Remove_From_Quadtree(void* incoming,
                                           double ilo[2],
                                           double ihi[2]) {

  int flag, i, j;
  double mid[2];
  bool nokids;

  // status return flag:
  //     1 - item not found
  //     0 - item found and deleted
  //    -1 - item found and deleted, no more items so child can be deleted.
  /* Set flag to item not found */
  flag = 1;

  // compute mid-point coordinates
  for (i=0; i < 2; i++)
    mid[i] = (lo[i] + hi[i])*0.5;
  
  // determine Octant to search for object
  i = j = -1;
  if (ihi[0] < mid[0]) i=0;
  if (ilo[0] > mid[0]) i=1;
  if (ihi[1] < mid[1]) j=0;
  if (ilo[1] > mid[1]) j=1;
  
  /* If quadrant not detected */
  if (i < 0 || j < 0) {

    if (objects->Delete_From_List(incoming)) // delete from current Octant
      flag = 0;
    if (flag == 0 && objects->max == 0) {// nothing left, test for deletion

      objects->Redimension(0); // free list memory
      nokids=true;
      for (j=0; j < 2 && nokids; j++)
        for (i=0; i < 2 && nokids; i++)
          if (kids[i][j] != 0) nokids = false;
      if (nokids && mom != 0)  // Cannot delete root octant!!!
        flag = -1; // set up for deletion
    }
  }
  else { // pass down tree

    // does child exist?
    if (kids[i][j] != 0) // pass to child Octant
      flag = kids[i][j]->Remove_From_Quadtree(incoming, ilo, ihi);
    if (flag == -1) {

      delete kids[i][j];
      kids[i][j] = 0;
      flag = 0;
    }
  }

  return(flag);
}

// replace an object pointer in tree
//bool Quadtree_Storage::Replace_in_Quadtree(void* orig_ptr, void* new_ptr)
//{
//  int i, j;
//  bool flag = false;
//
//  // status return flag:
//  //     false - item not found
//  //     true - item found and replaced
//
//  flag = objects->Replace(orig_ptr,new_ptr); // replace in object list of current octant
//
//  // not found yet, look further down tree
//  for (j=0; j < 2 && !flag; j++)
//    for (i=0; i < 2 && !flag; i++)
//      if (kids[i][j] != 0)
//        flag = kids[i][j]->Replace_in_Quadtree(orig_ptr, new_ptr);
//
//  return(flag);
//}

/* given object with extents, replace an object pointer in tree */
bool Quadtree_Storage::Replace_in_Quadtree(void* orig_ptr,
                                           void* new_ptr,
                                           double ilo[2],
                                           double ihi[2]) {

  int i, j, k;
  double mid[2];
  bool flag = false;

  // status return flag:
  //     false - item not found
  //     true - item found and replaced

  for (i=0; i < 2; i++)
    mid[i] = (lo[i] + hi[i])*0.5;

  // determine Octant to search for object
  i = j = -1;
  if (ihi[0] < mid[0]) i=0;
  if (ilo[0] > mid[0]) i=1;
  if (ihi[1] < mid[1]) j=0;
  if (ilo[1] > mid[1]) j=1;
  
  if (i < 0 || j < 0) {

    flag = objects->Replace(orig_ptr,new_ptr,ilo,ihi); // replace in object list of current octant
  }
  else { // pass down tree

    // does child exist?
    if (kids[i][j] != 0) // pass to child Octant
      flag = kids[i][j]->Replace_in_Quadtree(orig_ptr, new_ptr, ilo, ihi);
  }

  return(flag);
}

