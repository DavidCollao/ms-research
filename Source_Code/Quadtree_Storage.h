#include <stdio.h>
#include <stdio.h>
#include "PList.h"

#ifndef quadtree_storage_h
#define quadtree_storage_h

/* Definition of quadtree storage */
class Quadtree_Storage {

  private:
    /* Quadtree_Storage's declared, lo and hi private declared, and PList
     * declared, and others */
    Quadtree_Storage *kids[2][2];
    Quadtree_Storage *mom;
    double lo[2], hi[2];
    PList* objects;
    int level;
    int level_limit;
    double size_limit;

  /* Writing public variables and functions */
  public:
  /* Constructor: takes coordinates of box corner points, level and size
   * limit(slm) */
  Quadtree_Storage(Quadtree_Storage *m,
                   double plo[2],
                   double phi[2],
                   int lvl=0,
                   double slm=0.0) {

    int i, j;
  
    /* store extent box corner points */
    for (i=0; i < 2; i++) {
      lo[i] = plo[i];
      hi[i] = phi[i];
    }
  
    /* store pointer to mother and compute level */
    mom = m;
    /* If mom is not NULL, get level from mom and add one */
    if (mom > 0)
      level = mom->level+1;
    /* If mom is NULL, start level at one */
    else
      level = 1;
  
    /* initialize object pointer */
    objects = new PList();
  
    /* initialize child pointers, set them to NULL */
    for (j=0; j < 2; j++)
      for (i=0; i < 2; i++)
        kids[i][j] = 0;

    /* Initialize levels */
    level_limit = lvl; // optional argument for maximum number of levels
    size_limit = slm;  // optional argument for minimum octant size

  }

  /* Destructor, takes nothing */
  ~Quadtree_Storage() {

    int i, j;
  
    /* delete any kids that exist (this will proceed down tree), and set them
     * to NULL */
    for (j=0; j < 2; j++)
      for (i=0; i < 2; i++) {
        if (kids[i][j] != 0)
          delete kids[i][j];
        kids[i][j] = 0;
      }

    /* Delete objects and set other variables to zero */
    delete objects;
    objects = 0;
    mom = 0;
    level = 0;

  }

  /* Declare other functions later defined in Quadtree_Storage.cpp */
  //bool Replace_in_Quadtree(void* orig_ptr, void* new_ptr);
  void extents(double tlo[2],
               double thi[2]);
  int max_level(int l);
  bool retrieve_list(double p[2],
                     double tol[2],
                     int lvl,
                     PList *list);
  double finest_size(double p[2]);
  void Store_In_Quadtree(int nobj,
                         void* obj_ptr[],
                         double (*obj_lo)[2],
                         double (*obj_hi)[2]);
  void Store_In_Quadtree(void* obj_ptr,
                         double obj_lo[2],
                         double obj_hi[2]);
  int Remove_From_Quadtree(void* obj_ptr);
  int Remove_From_Quadtree(void* obj_ptr,
                           double obj_lo[2],
                           double obj_hi[2]);
  bool Replace_in_Quadtree(void* orig_ptr,
                           void* new_ptr,
                           double obj_lo[2],
                           double obj_hi[2]);

};
#endif

