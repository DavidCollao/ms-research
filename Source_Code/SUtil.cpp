#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"SUtil.h"
#include"Space.h"

#define MIN(x,y) ((x) <= (y) ? (x) : (y))
#define MAX(x,y) ((x) >= (y) ? (x) : (y))
#define ABS(x) ((x) >= 0 ? (x) : -(x))
#define SIGN(a,b) ((b) < 0.0 ? -ABS(a) : ABS(a))

double distance( Point p1, Point p2) {
  #if SPACE == 2
    double dx = p2[0]-p1[0];
    double dy = p2[1]-p1[1];
    double mag = sqrt(dx*dx+dy*dy);
  #else
    double dx = p2[0]-p1[0];
    double dy = p2[1]-p1[1];
    double dz = p2[2]-p1[2];
    double mag = sqrt(dx*dx+dy*dy+dz*dz);
  #endif
    return mag;
}

double triangle_area( Point p1, Point p2, Point p3 ) {
  Vector v1 = Vector( p1, p2 );
  Vector v2 = Vector( p1, p3 );
  double area = ( v1 % v2 ) * 0.5;
  return( area );
}

double quadrilateral_area( Point p1, Point p2, Point p3, Point p4 ) {
  Vector v1 = Vector( p1, p3 );
  Vector v2 = Vector( p2, p4 );
  double area = ( v1 % v2 ) * 0.5;
  return( area );
}

double angle( Point p1, Point p2, Point p3 ) {
  double angle, sign, crossProduct;
  Vector v1 = Vector( p1, p2 );
  Vector v2 = Vector( p1, p3 );
  crossProduct = v1 % v2;
  if( fabs( crossProduct ) < 1.0e-12 ) {
    if( ( v1 * v2 ) < 1.0e-12 ) {
      return( M_PI );
    }
    else {
      return( 0.0 );
    }
  }
  sign = crossProduct / fabs( crossProduct );
  angle = sign * acos( ( v1 * v2 ) / ( v1.magnitude() * v2.magnitude() ) );
  return( angle );
}

Vector triangle_gradient( Point p0,
                          Point p1,
                          Point p2,
                          double f0,
                          double f1,
                          double f2 ) {

  double area, cx, cy, ff;
  Vector side, face, grad;

  cx = cy = 0.0;

  /* Get scalar area */
  area = triangle_area( p0, p1, p2 );

  /* Going through faces */
  side = Vector( p0, p1 );
  face = Vector( side[1], -side[0] );
  ff   = 0.5 * ( f0 + f1 );
  cx  += ff * face[0];
  cy  += ff * face[1];

  side = Vector( p1, p2 );
  face = Vector( side[1], -side[0] );
  ff   = 0.5 * ( f1 + f2 );
  cx  += ff * face[0];
  cy  += ff * face[1];

  side = Vector( p2, p0 );
  face = Vector( side[1], -side[0] );
  ff   = 0.5 * ( f2 + f0 );
  cx  += ff * face[0];
  cy  += ff * face[1];

  /* Calculating gradient */
  grad = Vector( cx / area, cy / area );

  return( grad );

}

Vector quadrilateral_gradient( Point p0,
                               Point p1,
                               Point p2,
                               Point p3,
                               double f0,
                               double f1,
                               double f2,
                               double f3 ) {

  double area, cx, cy, ff;
  Vector side, face, grad;

  cx = cy = 0.0;

  /* Get scalar area */
  area = quadrilateral_area( p0, p1, p2, p3 );

  /* Going through faces */
  side = Vector( p0, p1 );
  face = Vector( side[1], -side[0] );
  ff   = 0.5 * ( f0 + f1 );
  cx  += ff * face[0];
  cy  += ff * face[1];

  side = Vector( p1, p2 );
  face = Vector( side[1], -side[0] );
  ff   = 0.5 * ( f1 + f2 );
  cx  += ff * face[0];
  cy  += ff * face[1];

  side = Vector( p2, p3 );
  face = Vector( side[1], -side[0] );
  ff   = 0.5 * ( f2 + f3 );
  cx  += ff * face[0];
  cy  += ff * face[1];

  side = Vector( p3, p0 );
  face = Vector( side[1], -side[0] );
  ff   = 0.5 * ( f3 + f0 );
  cx  += ff * face[0];
  cy  += ff * face[1];

  /* Calculating gradient */
  grad = Vector( cx / area, cy / area );

  return( grad );

}

#if SPACE == 3
double tetrahedral_volume(Point p1, Point p2, Point p3, Point p4) {
  Vector v1 = Vector(p1,p2);
  Vector v2 = Vector(p1,p3);
  Vector v3 = Vector(p1,p4);
  double vol = scalar_triple_product(v1,v2,v3)/6.0;

  return(vol);
}

double pyramid_volume(Point p1, Point p2, Point p3, Point p4, Point p5) {
  Point fc = (p1 + p2 + p3 + p4)*0.25;
  Vector v1, v2, v3;
  double vol;
  v1 = Vector(fc,p1);
  v2 = Vector(fc,p2);
  v3 = Vector(fc,p5);
  vol = scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p2);
  v2 = Vector(fc,p3);
  v3 = Vector(fc,p5);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p3);
  v2 = Vector(fc,p4);
  v3 = Vector(fc,p5);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p4);
  v2 = Vector(fc,p1);
  v3 = Vector(fc,p5);
  vol += scalar_triple_product(v1,v2,v3)/6.0;

  return(vol);
}

double prism_volume(Point p1, Point p2, Point p3, Point p4, Point p5, Point p6) {
  Point cg = (p1 + p2 + p3 + p4 + p5 + p6)/6.0;
  Point fc;
  Vector v1, v2, v3;
  double vol = 0.0;
  // side 1
  fc = (p1 + p2 + p4 + p5)*0.25;
  v1 = Vector(fc,p2);
  v2 = Vector(fc,p1);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p1);
  v2 = Vector(fc,p4);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p4);
  v2 = Vector(fc,p5);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p5);
  v2 = Vector(fc,p2);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  // side 2
  fc = (p2 + p3 + p5 + p6)*0.25;
  v1 = Vector(fc,p3);
  v2 = Vector(fc,p2);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p2);
  v2 = Vector(fc,p5);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p5);
  v2 = Vector(fc,p6);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p6);
  v2 = Vector(fc,p3);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  // side 3
  fc = (p3 + p1 + p6 + p4)*0.25;
  v1 = Vector(fc,p1);
  v2 = Vector(fc,p3);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p3);
  v2 = Vector(fc,p6);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p6);
  v2 = Vector(fc,p4);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p4);
  v2 = Vector(fc,p1);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  // side 4
  v1 = Vector(p1,p2);
  v2 = Vector(p1,p3);
  v3 = Vector(p1,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  // side 5
  v1 = Vector(p4,p6);
  v2 = Vector(p4,p5);
  v3 = Vector(p4,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;

  return(vol);
}

double hexahedral_volume(Point p1, Point p2, Point p3, Point p4,
                         Point p5, Point p6, Point p7, Point p8) {
  Point cg = (p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8)/8.0;
  Point fc;
  Vector v1, v2, v3;
  double vol = 0.0;
  fc = (p1 + p2 + p3 + p4)*0.25;
  v1 = Vector(fc,p1);
  v2 = Vector(fc,p2);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p2);
  v2 = Vector(fc,p3);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p3);
  v2 = Vector(fc,p4);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p4);
  v2 = Vector(fc,p1);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  fc = (p1 + p2 + p5 + p6)*0.25;
  v1 = Vector(fc,p1);
  v2 = Vector(fc,p5);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p5);
  v2 = Vector(fc,p6);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p6);
  v2 = Vector(fc,p2);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p2);
  v2 = Vector(fc,p1);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  fc = (p2 + p3 + p6 + p7)*0.25;
  v1 = Vector(fc,p2);
  v2 = Vector(fc,p6);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p6);
  v2 = Vector(fc,p7);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p7);
  v2 = Vector(fc,p3);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p3);
  v2 = Vector(fc,p2);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  fc = (p3 + p4 + p7 + p8)*0.25;
  v1 = Vector(fc,p3);
  v2 = Vector(fc,p7);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p7);
  v2 = Vector(fc,p8);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p8);
  v2 = Vector(fc,p4);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p4);
  v2 = Vector(fc,p3);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  fc = (p1 + p4 + p5 + p8)*0.25;
  v1 = Vector(fc,p4);
  v2 = Vector(fc,p8);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p8);
  v2 = Vector(fc,p5);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p5);
  v2 = Vector(fc,p1);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p1);
  v2 = Vector(fc,p4);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  fc = (p5 + p6 + p7 + p8)*0.25;
  v1 = Vector(fc,p5);
  v2 = Vector(fc,p8);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p8);
  v2 = Vector(fc,p7);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p7);
  v2 = Vector(fc,p6);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;
  v1 = Vector(fc,p6);
  v2 = Vector(fc,p5);
  v3 = Vector(fc,cg);
  vol += scalar_triple_product(v1,v2,v3)/6.0;

  return(vol);
}

Vector tetrahedral_gradient(Point p0, Point p1, Point p2, Point p3,
                            double f0, double f1, double f2, double f3) {
  double cx, cy, cz, vol, ff;
  Vector v1, v2, area, grad;

  vol = tetrahedral_volume(p0,p1,p2,p3);

  cx = cy = cz = 0.0;

  // face 0
  v1 = Vector(p0,p2);
  v2 = Vector(p0,p1);
  area = (v1 % v2)*0.5;
  ff = (f0+f1+f2)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 1
  v1 = Vector(p0,p1);
  v2 = Vector(p0,p3);
  area = (v1 % v2)*0.5;
  ff = (f0+f1+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 2
  v1 = Vector(p1,p2);
  v2 = Vector(p1,p3);
  area = (v1 % v2)*0.5;
  ff = (f1+f2+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 3
  v1 = Vector(p2,p0);
  v2 = Vector(p2,p3);
  area = (v1 % v2)*0.5;
  ff = (f2+f0+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];

  grad = Vector(cx/vol,cy/vol,cz/vol);

  return(grad);
}

Vector pyramid_gradient(Point p0, Point p1, Point p2, Point p3, Point p4,
                        double f0, double f1, double f2, double f3, double f4) {
  double cx, cy, cz, vol, ff, fcg;
  Vector v1, v2, area, grad;
  Point cg;

  vol = pyramid_volume(p0,p1,p2,p3,p4);

  cx = cy = cz = 0.0;

  // face 0
  cg = (p0+p1+p2+p3)*0.25;
  fcg = (f0+f1+f2+f3)*0.25;
  v1 = Vector(cg,p0);
  v2 = Vector(cg,p3);
  area = (v1 % v2)*0.5;
  ff = (fcg+f0+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p3);
  v2 = Vector(cg,p2);
  area = (v1 % v2)*0.5;
  ff = (fcg+f3+f2)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p2);
  v2 = Vector(cg,p1);
  area = (v1 % v2)*0.5;
  ff = (fcg+f2+f1)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p1);
  v2 = Vector(cg,p0);
  area = (v1 % v2)*0.5;
  ff = (fcg+f1+f0)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 1
  v1 = Vector(p4,p0);
  v2 = Vector(p4,p1);
  area = (v1 % v2)*0.5;
  ff = (f0+f1+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 2
  v1 = Vector(p4,p1);
  v2 = Vector(p4,p2);
  area = (v1 % v2)*0.5;
  ff = (f1+f2+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 3
  v1 = Vector(p4,p2);
  v2 = Vector(p4,p3);
  area = (v1 % v2)*0.5;
  ff = (f2+f3+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 4
  v1 = Vector(p4,p3);
  v2 = Vector(p4,p0);
  area = (v1 % v2)*0.5;
  ff = (f0+f3+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];

  grad = Vector(cx/vol,cy/vol,cz/vol);

  return(grad);
}

Vector prism_gradient(Point p0, Point p1, Point p2,
                      Point p3, Point p4, Point p5,
                      double f0, double f1, double f2,
                      double f3, double f4, double f5) {
  double cx, cy, cz, vol, ff, fcg;
  Vector v1, v2, area, grad;
  Point cg;

  vol = prism_volume(p0,p1,p2,p3,p4,p5);

  cx = cy = cz = 0.0;

  // face 0
  cg = (p0+p1+p4+p3)*0.25;
  fcg = (f0+f1+f4+f3)*0.25;
  v1 = Vector(cg,p0);
  v2 = Vector(cg,p1);
  area = (v1 % v2)*0.5;
  ff = (fcg+f0+f1)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p1);
  v2 = Vector(cg,p4);
  area = (v1 % v2)*0.5;
  ff = (fcg+f1+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p4);
  v2 = Vector(cg,p3);
  area = (v1 % v2)*0.5;
  ff = (fcg+f4+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p3);
  v2 = Vector(cg,p0);
  area = (v1 % v2)*0.5;
  ff = (fcg+f3+f0)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 1
  cg = (p1+p2+p5+p4)*0.25;
  fcg = (f1+f2+f5+f4)*0.25;
  v1 = Vector(cg,p1);
  v2 = Vector(cg,p2);
  area = (v1 % v2)*0.5;
  ff = (fcg+f1+f2)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p2);
  v2 = Vector(cg,p5);
  area = (v1 % v2)*0.5;
  ff = (fcg+f2+f5)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p5);
  v2 = Vector(cg,p4);
  area = (v1 % v2)*0.5;
  ff = (fcg+f5+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p4);
  v2 = Vector(cg,p1);
  area = (v1 % v2)*0.5;
  ff = (fcg+f4+f1)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 2
  cg = (p2+p0+p3+p5)*0.25;
  fcg = (f2+f0+f3+f5)*0.25;
  v1 = Vector(cg,p2);
  v2 = Vector(cg,p0);
  area = (v1 % v2)*0.5;
  ff = (fcg+f2+f0)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p0);
  v2 = Vector(cg,p3);
  area = (v1 % v2)*0.5;
  ff = (fcg+f0+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p3);
  v2 = Vector(cg,p5);
  area = (v1 % v2)*0.5;
  ff = (fcg+f3+f5)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p5);
  v2 = Vector(cg,p2);
  area = (v1 % v2)*0.5;
  ff = (fcg+f5+f2)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 3
  v1 = Vector(p0,p2);
  v2 = Vector(p0,p1);
  area = (v1 % v2)*0.5;
  ff = (f0+f2+f1)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 4
  v1 = Vector(p3,p4);
  v2 = Vector(p3,p5);
  area = (v1 % v2)*0.5;
  ff = (f3+f4+f5)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];

  grad = Vector(cx/vol,cy/vol,cz/vol);

  return(grad);
}

Vector hexahedral_gradient(Point p0, Point p1, Point p2, Point p3,
                           Point p4, Point p5, Point p6, Point p7,
                           double f0, double f1, double f2, double f3,
                           double f4, double f5, double f6, double f7) {
  double cx, cy, cz, vol, ff, fcg;
  Vector v1, v2, area, grad;
  Point cg;

  vol = hexahedral_volume(p0,p1,p2,p3,p4,p5,p6,p7);

  cx = cy = cz = 0.0;

  // face 0
  cg = (p0+p3+p2+p1)*0.25;
  fcg = (f0+f3+f2+f1)*0.25;
  v1 = Vector(cg,p0);
  v2 = Vector(cg,p3);
  area = (v1 % v2)*0.5;
  ff = (fcg+f0+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p3);
  v2 = Vector(cg,p2);
  area = (v1 % v2)*0.5;
  ff = (fcg+f3+f2)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p2);
  v2 = Vector(cg,p1);
  area = (v1 % v2)*0.5;
  ff = (fcg+f2+f1)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p1);
  v2 = Vector(cg,p0);
  area = (v1 % v2)*0.5;
  ff = (fcg+f1+f0)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 1
  cg = (p0+p1+p5+p4)*0.25;
  fcg = (f0+f1+f5+f4)*0.25;
  v1 = Vector(cg,p0);
  v2 = Vector(cg,p1);
  area = (v1 % v2)*0.5;
  ff = (fcg+f0+f1)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p1);
  v2 = Vector(cg,p5);
  area = (v1 % v2)*0.5;
  ff = (fcg+f1+f5)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p5);
  v2 = Vector(cg,p4);
  area = (v1 % v2)*0.5;
  ff = (fcg+f5+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p4);
  v2 = Vector(cg,p0);
  area = (v1 % v2)*0.5;
  ff = (fcg+f4+f0)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 2
  cg = (p1+p2+p6+p5)*0.25;
  fcg = (f1+f2+f6+f5)*0.25;
  v1 = Vector(cg,p1);
  v2 = Vector(cg,p2);
  area = (v1 % v2)*0.5;
  ff = (fcg+f1+f2)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p2);
  v2 = Vector(cg,p6);
  area = (v1 % v2)*0.5;
  ff = (fcg+f2+f6)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p6);
  v2 = Vector(cg,p5);
  area = (v1 % v2)*0.5;
  ff = (fcg+f6+f5)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p5);
  v2 = Vector(cg,p1);
  area = (v1 % v2)*0.5;
  ff = (fcg+f5+f1)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 3
  cg = (p2+p3+p7+p6)*0.25;
  fcg = (f2+f3+f7+f6)*0.25;
  v1 = Vector(cg,p2);
  v2 = Vector(cg,p3);
  area = (v1 % v2)*0.5;
  ff = (fcg+f2+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p3);
  v2 = Vector(cg,p7);
  area = (v1 % v2)*0.5;
  ff = (fcg+f3+f7)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p7);
  v2 = Vector(cg,p6);
  area = (v1 % v2)*0.5;
  ff = (fcg+f7+f6)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p6);
  v2 = Vector(cg,p2);
  area = (v1 % v2)*0.5;
  ff = (fcg+f6+f2)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 4
  cg = (p0+p4+p7+p3)*0.25;
  fcg = (f0+f4+f7+f3)*0.25;
  v1 = Vector(cg,p0);
  v2 = Vector(cg,p4);
  area = (v1 % v2)*0.5;
  ff = (fcg+f0+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p4);
  v2 = Vector(cg,p7);
  area = (v1 % v2)*0.5;
  ff = (fcg+f4+f7)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p7);
  v2 = Vector(cg,p3);
  area = (v1 % v2)*0.5;
  ff = (fcg+f7+f3)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p3);
  v2 = Vector(cg,p0);
  area = (v1 % v2)*0.5;
  ff = (fcg+f3+f0)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  // face 5
  cg = (p4+p5+p6+p7)*0.25;
  fcg = (f4+f5+f6+f7)*0.25;
  v1 = Vector(cg,p4);
  v2 = Vector(cg,p5);
  area = (v1 % v2)*0.5;
  ff = (fcg+f4+f5)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p5);
  v2 = Vector(cg,p6);
  area = (v1 % v2)*0.5;
  ff = (fcg+f5+f6)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p6);
  v2 = Vector(cg,p7);
  area = (v1 % v2)*0.5;
  ff = (fcg+f6+f7)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];
  v1 = Vector(cg,p7);
  v2 = Vector(cg,p4);
  area = (v1 % v2)*0.5;
  ff = (fcg+f7+f4)/3.0;
  cx += ff*area[0];
  cy += ff*area[1];
  cz += ff*area[2];

  grad = Vector(cx/vol,cy/vol,cz/vol);

  return(grad);
}

#endif
