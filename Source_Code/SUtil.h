//#include "mpi.h"
#include <stdio.h>
#include "Point.h"
#include "Vector.h"

#ifndef SUtil_h
#define SUtil_h

double distance( Point p1,
                 Point p2);

double triangle_area( Point p1,
                      Point p2,
                      Point p3 );

double quadrilateral_area( Point p1,
                           Point p2,
                           Point p3,
                           Point p4 );

Vector triangle_gradient( Point p0,
                          Point p1,
                          Point p2,
                          double f0,
                          double f1,
                          double f2 );

Vector quadrilateral_gradient( Point p0,
                               Point p1,
                               Point p2,
                               Point p3,
                               double f0,
                               double f1,
                               double f2,
                               double f3 );

double angle( Point p1, Point p2, Point p3 );

double tetrahedral_volume(Point p1,
                          Point p2,
                          Point p3,
                          Point p4);

double pyramid_volume(Point p1,
                      Point p2,
                      Point p3,
                      Point p4,
                      Point p5);

double prism_volume(Point p1,
                    Point p2,
                    Point p3,
                    Point p4,
                    Point p5,
                    Point p6);

double hexahedral_volume(Point p1,
                         Point p2,
                         Point p3,
                         Point p4,
                         Point p5,
                         Point p6,
                         Point p7,
                         Point p8);

Vector tetrahedral_gradient(Point p1,
                            Point p2,
                            Point p3,
                            Point p4,
                            double f1,
                            double f2,
                            double f3,
                            double f4);

Vector pyramid_gradient(Point p1,
                        Point p2,
                        Point p3,
                        Point p4,
                        Point p5,
                        double f1,
                        double f2,
                        double f3,
                        double f4,
                        double f5);

Vector prism_gradient(Point p1,
                      Point p2,
                      Point p3,
                      Point p4,
                      Point p5,
                      Point p6,
                      double f1,
                      double f2,
                      double f3,
                      double f4,
                      double f5,
                      double f6);

Vector hexahedral_gradient(Point p1,
                           Point p2,
                           Point p3,
                           Point p4,
                           Point p5,
                           Point p6,
                           Point p7,
                           Point p8,
                           double f1,
                           double f2,
                           double f3,
                           double f4,
                           double f5,
                           double f6,
                           double f7,
                           double f8);

void Top_Ordering( const int max,
                   int * item,
                   int * function );

#endif

