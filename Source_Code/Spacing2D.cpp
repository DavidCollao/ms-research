#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "Vector.h"
#include "amesh_obj.h"
#include "SUtil.h"
#include "journal.h"
#include "Spacing_Obj.h"
#include "Plotfile.h"

#define MIN(x,y) ((x) <= (y) ? (x) : (y))
#define MAX(x,y) ((x) >= (y) ? (x) : (y))

#define GAS_CONSTANT 1716.5613

/* This code is 2D version of the provided spacing routine */

/* Velocity Magnitude. NEED TO CHANGE ALL THIS FOR ONE FUNCTION */
double 2Dvelmag_from_regime_0( double Q[] ) {

  return( sqrt( Q[1]*Q[1] + Q[2]*Q[2] ) );//density 1(?)
}

double 2Dvelmag_from_regime_1( double Q[] ) {

  return( sqrt( Q[1]*Q[1] + Q[2]*Q[2] )/ Q[0] );
}

double 2Dvelmag_from_regime_2( double Q[] ) {

  return( sqrt( Q[1]*Q[1] + Q[2]*Q[2] ) );//density 1(?)
}

double 2Dvelmag_from_FALCON( double Q[] ) {

  return( sqrt( Q[0]*Q[0] + Q[1]*Q[1] ) );//density 1(?)
}

/* Pressure. NEED TO CHANGE ALL THIS FOR ONE FUNCTION */
double 2Dpressure_from_regime_0(double Q[])
{
  return (Q[0]+1.0/1.4);
}

double 2Dpressure_from_regime_1(double Q[])
{
  double gamma=1.4;
  double vmag = sqrt( Q[1]*Q[1]+ Q[2]*Q[2] )/ Q[0];
  double energy = Q[3]/Q[0] - 0.5 * vmag*vmag;
  return( Q[0]*(gamma-1.0)*energy);
}

double 2Dpressure_from_regime_6( double Q[] ) {

  return( Q[0]*GAS_CONSTANT*Q[3] );
}

double 2Dpressure_from_FALCON( double Q[] ) {

  return( Q[3]*Q[2]*GAS_CONSTANT);
}

/* Total Pressure. NEED TO CHANGE ALL THIS FOR ONE FUNCTION */
double 2Dtotal_pressure_from_regime_0( double Q[] ) {

  double p = pressure_from_regime_0(Q);
  double vm = velmag_from_regime_0(Q);
  return(p+0.5*vm*vm);
}

/* Mach Number. NEED TO CHANGE ALL THIS FOR ONE FUNCTION */
double 2Dmach_from_regime_1(double Q[]) {

  double gamma=1.4;
  double vmag = sqrt( Q[1]*Q[1]+ Q[2]*Q[2] )/ Q[0];
  double energy = Q[3]/Q[0] - 0.5*vmag*vmag;
  energy = MAX(energy,1.0e-12);
  double pressure = Q[0]*(gamma-1.0)*energy;
  pressure = MAX(pressure,1.0e-12);
  /* sos: speed of sound */
  double sos = sqrt(gamma*pressure/Q[0]);
  sos = MAX(sos,1.0e-12);
  return(vmag/sos);
}

double 2Dmach_from_regime_2( double Q[] ) {

  double gamma=1.4;
  double vmag = sqrt( Q[1]*Q[1]+ Q[2]*Q[2] ) / Q[0];
  double sos = sqrt(gamma*Q[3]/Q[0]);
  sos = MAX(sos,1.0e-12);
  return(vmag/sos);
}

double 2Dmach_from_FALCON( double Q[] ) {

  double gamma=1.4;
  double vmag = sqrt( Q[0]*Q[0]+ Q[1]*Q[1] );
  double sos = sqrt( gamma*GAS_CONSTANT*Q[3] );
  sos = MAX(sos,1.0e-12);
  return(vmag/sos);
}

#ifndef Qmesh_obj_h
#define Qmesh_obj_h
class Qmesh_obj : public amesh_obj
{
 public:

  int nvars;
  double **Q;
  char **Q_name;
  char **b_name;

  Qmesh_obj() : amesh_obj()
  { nvars=0; Q=0; Q_name=0; b_name=0;}
  virtual ~Qmesh_obj()
  {
    int i;
    if (nvars > 0)
    {
      for (i=0; i < nn; i++)
        free(Q[i]);
      free(Q);
      for (i=0; i < nvars; i++)
        free(Q_name[i]);
      free(Q_name);
    }
    Q=0;
    Q_name=0;
    nvars=0;
    for (i=0; i < nb; i++)
      free(b_name[i]);
    free(b_name);
  }

  void 2Dvorticity_helicity(double f[], int mode);

};
#endif

class REFINE_BOX
{
 public:
  Point lo, hi;
};

void Read_Plotfile(char filename[], Qmesh_obj *mesh, int mode);
void Write_Plotfile(char filename[], Qmesh_obj *mesh, int mode);

/* journal file, in file, out file */
FILE *jou_f, *in_f, *out_f;
//jou_f: journal file for master process


/* MAIN HERE */

int main( int argc, char * argv[] ) {

  int FV_yes, c, i, j, k, l, m, n, naf, n0, n1, n2, n3, nrbox;
  int max_space_level;
  int invdistwgt;
  const int bdim = 300;
  char buff[bdim], fname[bdim], sname[bdim], func[bdim];
  double ff, ds, std, dx, dy, dz, d1, gscale, dsglobal;
  /* The dimensions here were changed for 2 dimensions */
  double root_lo[2], root_hi[2], lo[2], hi[2];
  double vec1[2], vec2[2], vec3[2];
  double rmt1[2][2], rmt2[2][2], rmt3[2][2], rmtv[4];//rmt?Rimmanian tensor vector?
  double left[2][2], right[2][2], lam[2];//lam?
  double vert[4][2];//vert?
  FILE *fp;
  Vector v1, v2, v3, v4, norm, area;
  Point p0, p1, p2, plo, phi;

  Qmesh_obj *fv_mesh;
  Linked_Node *hd;
  Linked_List **nhash;
  REFINE_BOX *rbox;

  in_f = stdin;
  out_f = stdout;
  // open journal file for master process
  if ((jou_f=fopen("Spacing.jou","w")) == NULL)
  {
    fprintf(stderr,"\nCouldn't open file journal file");
    fflush(stderr);
    exit(0);
  }

  // if no input file specified read from standard input
  if (--argc < 1)
  {
    fprintf(out_f,"\nNo input file specified!");
    fprintf(out_f,"\nUsing standard input!");
    fflush(out_f);
  } else
  {
    if ((in_f=fopen(argv[argc],"r")) == NULL)
    {
      fprintf(stderr,"\nCouldn't open file <%s>\n",argv[argc]);
      fprintf(stderr,"\n\nUsage: P_HUGG.XXX [batch_input_file]\n");
      exit(0);
    }
  }

  //print program info 
  fprintf(out_f,"\n======================================================================");
  fprintf(out_f,"\n|                      2D Spacing main routine                       |");
  fprintf(out_f,"\n======================================================================\n");
  fflush(out_f);

  printf("\nMESH SPACING Program:");
  // open spacing function file
  journal(in_f, out_f, jou_f, "#Enter name of output spacing file >",sname);
  journal(in_f, out_f, jou_f, "#Enter maximum level in output Quadtree >",max_space_level);

  // create adaption tensor objects
  // store in Quadtree
  Spacing_Obj *a_t;
  a_t = new Spacing_Obj();

  journal(in_f, out_f, jou_f, "#Is there a Fieldview file? [0 1] >",FV_yes);
  /* If there is no fieldview file */
  if (!FV_yes) {

    journal(in_f, out_f, jou_f, "#Is there an existing spacing file? [0 1] >",i);

    /* There is a spacing file */
    if(i) {

      journal(in_f, out_f, jou_f, "#Enter the name of the spacing file >",fname);
      // write adaptive tensor file
      /* Spacing file has .hdf5 appended to it */
      if (strstr(fname,".hdf5") != NULL) {

        a_t->Import_Spacing(fname,1,1,out_f);

      }
      /* Spacing file does not have .hdf5 appended to it */
      else {

        int mflag = 0;
        int nn, ntri, nquad;
//        int n0, n1, n2;
        char mname[bdim];
        double *x, *y;
        int (*tri_n)[3];
        int (*quad_n)[4];

        journal(in_f, out_f, jou_f, "#Is there a corresponding mesh2D file? [0 1] >",mflag);

        root_lo[0] = root_lo[1] = 1.0e20;
        root_hi[0] = root_hi[1] = -1.0e20;
        /* There is a corresponding mesh2D file */
        if (mflag) {

          journal(in_f, out_f, jou_f, "#Enter the name of the mesh2D file >",mname);

          // attempt to read elements from mesh file
          if ((fp=fopen(mname,"r")) == NULL)
          {
            fprintf(stderr,"\nCouldn't open mesh2D file <%s>\n",mname);
            exit(0);
          }

          /* Read number of points */
          fgets(buff,bdim,fp);
          fgets(buff,bdim,fp);
          sscanf(buff,"%d",&nn);
          fprintf(out_f,"\nNumber of points in file = %d",nn);
          x = new double[nn];
          y = new double[nn];
          /* Read coordinates, and find root lo's and hi's */
          for (n=0; n < nn; n++)
          {
            fgets(buff,bdim,fp);
            sscanf(buff,"%lf %lf",&x[n],&y[n]);
            root_lo[0] = MIN(root_lo[0],x[n]);
            root_lo[1] = MIN(root_lo[1],y[n]);
            root_hi[0] = MAX(root_hi[0],x[n]);
            root_hi[1] = MAX(root_hi[1],y[n]);
          }

          /* Read number o elementes of each kind */
          fgets(buff,bdim,fp);
          fgets(buff,bdim,fp);
          sscanf(buff,"%d %d",&ntri, &nquad);
          fprintf(out_f,"\n # of triangles = %d",ntri);
          fprintf(out_f,"\n # of quadrilaterals = %d",nquad);
          fflush(out_f);

          /* Read nodes of triangles */
          if (ntri > 0)
          {
            tri_n = new int[ntri][3];
            for (n=0; n < ntri; n++)
            {
              fgets(buff,bdim,fp);
              sscanf(buff,"%d %d %d", &n0, &n1, &n2);
              tri_n[n][0] = n0-1;
              tri_n[n][1] = n1-1;
              tri_n[n][2] = n2-1;
            }
          }
          /* Read nodes of quadrilaterals */
          if (nquad > 0)
          {
            quad_n = new int[nquad][4];
            for (n=0; n < nquad; n++)
            {
              fgets(buff,bdim,fp);
              sscanf(buff,"%d %d %d %d", &n0, &n1, &n2, &n3);
              quad_n[n][0] = n0-1;
              quad_n[n][1] = n1-1;
              quad_n[n][2] = n2-1;
              quad_n[n][3] = n3-1;
            }
          }

          fclose(fp);
        }

        // attempt to read ASCII spacing file
        if ((fp=fopen(fname,"r")) == NULL)
        {
          fprintf(stderr,"\nCouldn't open ASCII spacing file <%s>\n",fname);
          exit(0);
        }
        fgets(buff,bdim,fp);
        fgets(buff,bdim,fp);
        /* If mesh file exists */
        if (mflag)
          sscanf(buff,"%lf",&dsglobal);//dsglobal?
        /* If mesh file does not exist */
        else
          sscanf(buff,"%lf %lf %lf %lf %lf %lf %lf",&dsglobal,
                                                    &root_lo[0],&root_lo[1],
                                                    &root_hi[0],&root_hi[1]);

        // GOTTA FIX Spacing_Obj.h FILE!!!
        a_t->Initialize_Root(root_lo,root_hi,dsglobal,max_space_level);

        /* If mesh file exists */
        if (mflag) {

          fgets(buff,bdim,fp);
          fgets(buff,bdim,fp);
          sscanf(buff,"%d %d",&i, &j);
          /* Checking if number of elements per kind specified by mesh3D file
           * and Spacing file match */
          if (i != ntri || %j != nquad) {

            fprintf(stderr,"\nNumber of elements does not match mesh file!\n");
            exit(0);
          }

          // I NEED TO FIX THE Spacing_Obj.h ROUTINES TO BE 2D FRIENDLY
          // CHANGED RMTV FROM SIZE 6 TO 4, AND tv

          /* Go through triangles */
          for (i=0; i < ntri; i++) {

            /* Get nodes of tri */
            n0 = tri_n[i][0];
            n1 = tri_n[i][1];
            n2 = tri_n[i][2];
            /* Transfer from coordinate array to vert array */
            vert[0][0] = x[n0];
            vert[0][1] = y[n0];
            vert[0][2] = z[n0];
            vert[1][0] = x[n1];
            vert[1][1] = y[n1];
            vert[1][2] = z[n1];
            vert[2][0] = x[n2];
            vert[2][1] = y[n2];
            vert[2][2] = z[n2];
            /* Read line and store tensor */
            fgets(buff,bdim,fp);
            j=sscanf(buff,"%lf %lf %lf %lf",
                      &rmtv[0],&rmtv[1],&rmtv[2],&rmtv[3]);
            if (j==1)
              a_t->Store_Scalar(rmtv[0],3,vert);
            else if (j==4)
            {
              a_t->Full_Tensor(rmtv,rmt1); 
              a_t->Store_Tensor(rmt1,3,vert);
            } else
            {
              fprintf(stderr,"\nError reading Tri %d spacing data\n",i);
              exit(0);
            }
          }
          /* Get rid of tri_n array */
          if (ntri > 0) delete[] tri_n;

          /* Go through quadrilaterals */
          for (i=0; i < nquad; i++) {

            /* Get nodes of tri */
            n0 = quad_n[i][0];
            n1 = quad_n[i][1];
            n2 = quad_n[i][2];
            n3 = quad_n[i][3];
            /* Transfer from coordinate array to vert array */
            vert[0][0] = x[n0];
            vert[0][1] = y[n0];
            vert[0][2] = z[n0];
            vert[1][0] = x[n1];
            vert[1][1] = y[n1];
            vert[1][2] = z[n1];
            vert[2][0] = x[n2];
            vert[2][1] = y[n2];
            vert[2][2] = z[n2];
            vert[3][0] = x[n3];
            vert[3][1] = y[n3];
            vert[3][2] = z[n3];
            /* Read line and store tensor */
            fgets(buff,bdim,fp);
            j=sscanf(buff,"%lf %lf %lf %lf",
                      &rmtv[0],&rmtv[1],&rmtv[2],&rmtv[3]);
            if (j==1)
              a_t->Store_Scalar(rmtv[0],4,vert);
            else if (j==4)
            {
              a_t->Full_Tensor(rmtv,rmt1); 
              a_t->Store_Tensor(rmt1,4,vert);
            } else
            {
              fprintf(stderr,"\nError reading Quad %d spacing data\n",i);
              exit(0);
            }
          }
          /* Get rid of quad_n array */
          if (nquad > 0) delete[] quad_n;

          delete[] x;
          delete[] y;

        }

        /* If mesh file does not exist */
        else {

          int ns, nt;
          fgets(buff,bdim,fp);
          fgets(buff,bdim,fp);
          sscanf(buff,"%d",&ns);//Read number of scalars
          /* Read lo's and hi's and store scalars */
          for (i=0; i < ns; i++) {

            fgets(buff,bdim,fp);
            sscanf(buff,"%lf %lf %lf %lf %lf",&ds,&lo[0],&lo[1],&hi[0],&hi[1]);
            a_t->Store_Scalar(ds,lo,hi);
          }
          fgets(buff,bdim,fp);
          fgets(buff,bdim,fp);
          sscanf(buff,"%d",&nt);//Read number of tensors
          /* Read lo's and hi's and store tensors */
          for (i=0; i < nt; i++) {

            fgets(buff,bdim,fp);
            sscanf(buff,"%%lf %lf %lf %lf %lf %lf %lf %lf",
                      &rmt1[0][0],&rmt1[0][1],&rmt1[1][1],&rmt1[1][2],
                      &lo[0],&lo[1],&hi[0],&hi[1]);
            a_t->Store_Tensor(rmt1,lo,hi);
          }
        }
        fclose(fp);
      }
      fprintf(out_f,"\nNumber of entries in spacing file = %d",a_t->Number_of_Entries());
    } 
    /* There is no spacing file */
    else {

      /* Manually enter lo's and hi's and initilize root */
      journal(in_f, out_f, jou_f, "#Enter lower X corner of spacing domain >",root_lo[0]);
      journal(in_f, out_f, jou_f, "#Enter lower Y corner of spacing domain >",root_lo[1]);
      journal(in_f, out_f, jou_f, "#Enter upper X corner of spacing domain >",root_hi[0]);
      journal(in_f, out_f, jou_f, "#Enter upper Y corner of spacing domain >",root_hi[1]);
      dsglobal = (root_hi[0]-root_lo[0])/10.0;
      dsglobal = MAX(dsglobal,(root_hi[1]-root_lo[1])/10.0);
      a_t->Initialize_Root(root_lo,root_hi,dsglobal,max_space_level);
    }
  }
  /* There is a fieldview file */
  // NEED TO FIX amesh_obj.h FILE AS WELL
  else {
    /* Take name of fieldview file */
    journal(in_f, out_f, jou_f, "#Enter the name of Fieldview file >",fname);

    /* Read it */
    fv_mesh = new Qmesh_obj();
    // NEED TO FIX Qmesh AND amesh
    Read_Plotfile(fname,fv_mesh,0);

    /* Check if grid scale bigger than one, if yes multiply grid nodes by scale */
    journal(in_f, out_f, jou_f, "#Enter Fieldview file mesh scaling (multiplication) factor >",gscale);
    if (fabs(gscale-1.0) > 1.0e-12)
      for (n=0; n < fv_mesh->nn; n++)
        fv_mesh->node[n] *= gscale;// Is this correct?

    /* Check for lo's and hi's */
    root_lo[0] = root_lo[1] =  1.0e20;
    root_hi[0] = root_hi[1] = -1.0e20;
    for (n=0; n < fv_mesh->nn; n++)
    {
      root_lo[0] = MIN(root_lo[0],fv_mesh->node[n][0]);
      root_lo[1] = MIN(root_lo[1],fv_mesh->node[n][1]);
      root_hi[0] = MAX(root_hi[0],fv_mesh->node[n][0]);
      root_hi[1] = MAX(root_hi[1],fv_mesh->node[n][1]);
    }

    /* Make bounds of geometry greater than lowest and highest points and
     * initialize root */
    dsglobal = (root_hi[0]-root_lo[0])/10.0;
    dsglobal = MAX(dsglobal,(root_hi[1]-root_lo[1])/10.0);
    ds = dsglobal*1.0e-5;
    root_lo[0] -= ds;
    root_lo[1] -= ds;
    root_hi[0] += ds;
    root_hi[1] += ds;
    a_t->Initialize_Root(root_lo,root_hi,dsglobal,max_space_level);

    /* Read refinement box file */
    nrbox = 0;// number of refinement boxes?
    rbox = 0;// refinement box?
    fname[0]='\0';
    strcat(fname,"Spacing.rbox");//Is there always a Spacing.rbox file?
    if ((fp = fopen(fname,"r")) != NULL)
    {
      fprintf(out_f,"\nReading refinement boxes.");
      fflush(out_f);
      //* * * I MODIFIED THIS PART TO MAKE IT 2D FRIENDLY
      while(fgets(buff,bdim,fp) != NULL)
      {
        i = sscanf(buff,"%lf %lf %lf %lf",
                         &p0[0],&p0[1],&p1[0],&p1[1]);
        if (i == 4)
        {
          rbox = (REFINE_BOX*)realloc((void*)rbox,(nrbox+1)*sizeof(REFINE_BOX));
          plo[0] = MIN(p0[0],p1[0]);
          plo[1] = MIN(p0[1],p1[1]);
          phi[0] = MAX(p0[0],p1[0]);
          phi[1] = MAX(p0[1],p1[1]);
          rbox[nrbox].lo = plo;
          rbox[nrbox].hi = phi;
          nrbox++;
          fprintf(out_f,"\nRefinement box %d",nrbox);
          plo.print(stdout);
          phi.print(stdout);
        }
      }
      fclose(fp);
    }


    // tag nodes for inclusion in the process
    int *tag = new int[fv_mesh->nn];// there's a tag per node
   
    //
    // create node-to-node hash table
    //
    // AGAIN, NEED TO MODIFY Qmesh, amesh
    nhash = new Linked_List*[fv_mesh->nn];
    for (n=0; n < fv_mesh->nn; n++)
      nhash[n] = new Linked_List();
    // MAKING THIS PART 2D FRIENDLY
    for (c=0; c < fv_mesh->ntri; c++)
    {
      n0 = fv_mesh->tri_n[c][0];
      n1 = fv_mesh->tri_n[c][1];
      n2 = fv_mesh->tri_n[c][2];
      if (!nhash[n0]->In_list(n1)) nhash[n0]->Insert(n1);
      if (!nhash[n0]->In_list(n2)) nhash[n0]->Insert(n2);
      if (!nhash[n1]->In_list(n0)) nhash[n1]->Insert(n0);
      if (!nhash[n1]->In_list(n2)) nhash[n1]->Insert(n2);
      if (!nhash[n2]->In_list(n0)) nhash[n2]->Insert(n0);
      if (!nhash[n2]->In_list(n1)) nhash[n2]->Insert(n1);
    }
    for (c=0; c < fv_mesh->nquad; c++)
    {
      n0 = fv_mesh->quad_n[c][0];
      n1 = fv_mesh->quad_n[c][1];
      n2 = fv_mesh->quad_n[c][2];
      n3 = fv_mesh->quad_n[c][3];
      if (!nhash[n0]->In_list(n1)) nhash[n0]->Insert(n1);
      if (!nhash[n0]->In_list(n3)) nhash[n0]->Insert(n3);
      if (!nhash[n1]->In_list(n0)) nhash[n1]->Insert(n0);
      if (!nhash[n1]->In_list(n2)) nhash[n1]->Insert(n2);
      if (!nhash[n2]->In_list(n1)) nhash[n2]->Insert(n1);
      if (!nhash[n2]->In_list(n3)) nhash[n2]->Insert(n3);
      if (!nhash[n3]->In_list(n0)) nhash[n3]->Insert(n0);
      if (!nhash[n3]->In_list(n2)) nhash[n3]->Insert(n2);
    }

    double *f, *wgt, (*rmt)[6];
    double e1[3], e2[3], e3[3], h1, h2, h3;
    Vector *grad, cgrad;
    f = new double[fv_mesh->nn];
    wgt = new double[fv_mesh->nn];
    grad = new Vector[fv_mesh->nn];
    rmt = new double[fv_mesh->nn][6];
    Point cg;//center of gravity?

    double power, dsmn, dsmx, dfmin, dfmax, dfmean, dfsd;

    dsmx = 0.0;//ds max
    dsmn = 1.0e20;//ds min

    for (n=0; n < fv_mesh->nn; n++)
    {
      rmt[n][0] = dsglobal;
      // MAY NEED TO MAKE THIS 2D FRIENDLY
      rmt[n][1]=rmt[n][2]=rmt[n][3]=rmt[n][4]=rmt[n][5]=0.0;
    }

    // define function pointers
    double (*pressure)(double Q[]);
    double (*total_pressure)(double Q[]);
    double (*velmag)(double Q[]);
    double (*mach)(double Q[]);
    double (*density)(double Q[]);
    double (*temperature)(double Q[]);
    pressure = 0;
    total_pressure = 0;
    density = 0;
    temperature = 0;
    velmag = 0;
    mach = 0;

    /* Selecting velocity from regime, may be used as adaptation function */
    if (strstr(fv_mesh->Q_name[0],"Pressure") != NULL &&
        strstr(fv_mesh->Q_name[1],"X_Velocity") != NULL &&
        strstr(fv_mesh->Q_name[2],"Y_Velocity") != NULL)
      velmag = &velmag_from_regime_0;
    else if (strstr(fv_mesh->Q_name[0],"Density") != NULL &&
             strstr(fv_mesh->Q_name[1],"X_Momentum") != NULL &&
             strstr(fv_mesh->Q_name[2],"Y_Momentum") != NULL)
      velmag = &velmag_from_regime_1;
    else if (strstr(fv_mesh->Q_name[1],"X_Velocity") != NULL &&
             strstr(fv_mesh->Q_name[2],"Y_Velocity") != NULL)
      velmag = &velmag_from_regime_2;
    else if (strstr(fv_mesh->Q_name[0],"U-velocity") != NULL &&
             strstr(fv_mesh->Q_name[1],"V-velocity") != NULL)
      velmag = &velmag_from_FALCON;

    /* Selecting pressure from regime, may be used as adaptation function */
    if (strstr(fv_mesh->Q_name[0],"Pressure") != NULL &&
        strstr(fv_mesh->Q_name[1],"X_Velocity") != NULL &&
        strstr(fv_mesh->Q_name[2],"Y_Velocity") != NULL)
      pressure = &pressure_from_regime_0;
    if (strstr(fv_mesh->Q_name[0],"Density") != NULL &&
        strstr(fv_mesh->Q_name[1],"X_Momentum") != NULL &&
        strstr(fv_mesh->Q_name[2],"Y_Momentum") != NULL &&
        strstr(fv_mesh->Q_name[4],"TotalEnergy") != NULL)
      pressure = &pressure_from_regime_1;
    if (strstr(fv_mesh->Q_name[0],"Density") != NULL &&
        strstr(fv_mesh->Q_name[1],"X_Velocity") != NULL &&
        strstr(fv_mesh->Q_name[2],"Y_Velocity") != NULL &&
        strstr(fv_mesh->Q_name[4],"Temperature") != NULL)
      pressure = &pressure_from_regime_6;
    if (strstr(fv_mesh->Q_name[0],"U-velocity") != NULL &&
        strstr(fv_mesh->Q_name[1],"V-velocity") != NULL &&
        strstr(fv_mesh->Q_name[3],"Temperature") != NULL &&
        strstr(fv_mesh->Q_name[4],"Density") != NULL)
      pressure = &pressure_from_FALCON;

    /* Selecting total pressure from regime, may be used as adaptation function */
    if (strstr(fv_mesh->Q_name[0],"Pressure") != NULL &&
        strstr(fv_mesh->Q_name[1],"X_Velocity") != NULL &&
        strstr(fv_mesh->Q_name[2],"Y_Velocity") != NULL)
      total_pressure = &total_pressure_from_regime_0;

    /* Selecting mach from regime, may be used as adaptation function */
    if (strstr(fv_mesh->Q_name[0],"Density") != NULL &&
        strstr(fv_mesh->Q_name[1],"X_Momentum") != NULL &&
        strstr(fv_mesh->Q_name[2],"Y_Momentum") != NULL &&
        strstr(fv_mesh->Q_name[4],"TotalEnergy") != NULL)
      mach = &mach_from_regime_1;
    if (strstr(fv_mesh->Q_name[0],"Density") != NULL &&
        strstr(fv_mesh->Q_name[1],"X_Velocity") != NULL &&
        strstr(fv_mesh->Q_name[2],"Y_Velocity") != NULL &&
        strstr(fv_mesh->Q_name[4],"Pressure") != NULL)
      mach = &mach_from_regime_2;
    if (strstr(fv_mesh->Q_name[0],"U-velocity") != NULL &&
        strstr(fv_mesh->Q_name[1],"V-velocity") != NULL &&
        strstr(fv_mesh->Q_name[3],"Temperature") != NULL &&
        strstr(fv_mesh->Q_name[4],"Density") != NULL)
      mach = &mach_from_FALCON;

    int compute = 0;
    naf = 1;//adaptation function number
    while (naf > 0)
    {
      /* Presenting adaptation function options */
      if (velmag != 0)
        fprintf(out_f,"\nAdaptation function 1 - Velocity magnitude");
      if (pressure != 0)
        fprintf(out_f,"\nAdaptation function 2 - Pressure");
      if (mach != 0)
        fprintf(out_f,"\nAdaptation function 3 - Mach number");
      if (velmag != 0)
      {
        fprintf(out_f,"\nAdaptation function 4 - Vorticity magnitude");
        fprintf(out_f,"\nAdaptation function 5 - X vorticity magnitude");
        fprintf(out_f,"\nAdaptation function 6 - Y vorticity magnitude");
        fprintf(out_f,"\nAdaptation function 7 - Z vorticity magnitude (NA)");
        fprintf(out_f,"\nAdaptation function 8 - Helicity");
      }
      if (total_pressure != 0 || (pressure != 0 && mach != 0))
        fprintf(out_f,"\nAdaptation function 9 - Total Pressure");

      for (n=0; n < fv_mesh->nvars; n++)
        fprintf(out_f,"\nAdaptation function %d - %s",n+10,fv_mesh->Q_name[n]);

      /* Selecting adaptation function */
      journal(in_f, out_f, jou_f, "#Enter adaptation function [0 to exit] >",naf);

      /* Presenting selection */
      compute=1;
      switch (naf)
      {
        case 0:
          compute=0;
          break;
        case 1:
          if (velmag == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to determine how to compute velocity magnitude.");
            continue;
          }
          sprintf(func,"Velocity Magnitudes");
          fprintf(out_f,"\nAdaptation function is velocity magnitude");
          /* Go through variables calculating velocity magnitude at nodes */
          for (n=0; n < fv_mesh->nn; n++)
            f[n] = velmag(fv_mesh->Q[n]);
          break;
        case 2:
          if (pressure == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to compute pressure with solution provided.");
            continue;
          }
          sprintf(func,"Pressure");
          fprintf(out_f,"\nAdaptation function is pressure.");
          /* Go through variables calculating pressure at nodes */
          for (n=0; n < fv_mesh->nn; n++)
            f[n] = pressure(fv_mesh->Q[n]);
          break;
        case 3:
          if (mach == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to compute Mach number with solution provided.");
            continue;
          }
          sprintf(func,"Mach_number");
          fprintf(out_f,"\nAdaptation function is Mach number");

          for (n=0; n < fv_mesh->nn; n++)
            f[n] = mach(fv_mesh->Q[n]);
          break;
        case 4:
          if (velmag == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to determine how to compute vorticity magnitude.");
            continue;
          }
          sprintf(func,"vorticity magnitude");
          fprintf(out_f,"\nAdaptation function is vorticity magnitude");
          fv_mesh->vorticity_helicity(f,0);
          break;
        case 5:
          if (velmag == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to determine how to compute X vorticity magnitude.");
            continue;
          }
          sprintf(func,"X vorticity magnitude");
          fprintf(out_f,"\nAdaptation function is X_vorticity magnitude");
          fv_mesh->vorticity_helicity(f,1);
          break;
        case 6:
          if (velmag == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to determine how to compute Y vorticity magnitude.");
            continue;
          }
          sprintf(func,"Y vorticity magnitude");
          fprintf(out_f,"\nAdaptation function is Y_vorticity magnitude");
          fv_mesh->vorticity_helicity(f,2);
          break;
        case 7:
          if (velmag == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to determine how to compute Z vorticity magnitude.");
            continue;
          }
          sprintf(func,"Z vorticity magnitude");
          fprintf(out_f,"\nAdaptation function is Z_vorticity magnitude");
          fv_mesh->vorticity_helicity(f,3);
          break;
        case 8:
          if (velmag == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to determine how to compute helicity");
            continue;
          }
          sprintf(func,"Helicity");
          fprintf(out_f,"\nAdaptation function is helicity.");
          fv_mesh->vorticity_helicity(f,4);
          break;
        case 9:
          if (total_pressure != 0)
          {
            for (n=0; n < fv_mesh->nn; n++)
              f[n] = total_pressure(fv_mesh->Q[n]);
          } else if (pressure == 0 || mach == 0)
          {
            fprintf(out_f,"\nSPACING: Unable to determine how to compute total pressure");
            continue;
          } else
          {
            // ASSUMES gamma = 1.4
            for (n=0; n < fv_mesh->nn; n++)
            {
              double p = pressure(fv_mesh->Q[n]);
              double m = mach(fv_mesh->Q[n]);
              f[n] = p*pow((1.0+m*m/5.0),3.5);
            }
          }
          sprintf(func,"Total Pressure");
          fprintf(out_f,"\nAdaptation function is Total Pressure.");
          break;
        default:
          if (naf > 9 && naf <= 9+fv_mesh->nvars)
          {
            sprintf(func,"%s",fv_mesh->Q_name[naf-10]);
            fprintf(out_f,"\nAdaptation function is %s",fv_mesh->Q_name[naf-10]);
            for (n=0; n < fv_mesh->nn; n++)
              f[n] = fv_mesh->Q[n][naf-10];
          }
          break;
      }

      fflush(out_f);

      /* Exit 'while' if not computing anything */
      if (compute==0)
        break;

      /* Getting ready to compute gradient vectors */
      for (n=0; n < fv_mesh->nn; n++)
        tag[n] = 1;

      // VISCOUS ELEMENTS?
      // Tagging nodes in viscous elements
//      journal(in_f, out_f, jou_f, "#Include viscous (prism) nodes in the process? [0 1] >",i);
//
//      if (i==0)
//        for (c=0; c < fv_mesh->npri; c++)
//          for (i=0; i < 6; i++)
//            tag[fv_mesh->pri_n[c][i]] = 0;

      journal(in_f, out_f, jou_f, "#Enter length scale exponent [>= 1.0] >", power);
      journal(in_f, out_f, jou_f, "#Enter nodal gradient weighting [0 - volume, 1 - inverse distance] >", invdistwgt);
      invdistwgt = MAX(0,MIN(1,invdistwgt));

      dfmin =  1.0e20;
      dfmax = -1.0e20;
      dfmean = 0.0;

      //MAKING CODE 2D FRIENDLY
      for (n=0; n < fv_mesh->nn; n++)
      {
        grad[n] = Vector(0.0,0.0);
        wgt[n] = 0.0;
      }

      /* Getting grads for triangles */
      for (c=0; c < fv_mesh->ntri; c++)
      {
        n0 = fv_mesh->tri_n[c][0];
        n1 = fv_mesh->tri_n[c][1];
        n2 = fv_mesh->tri_n[c][2];
        p0 = fv_mesh->node[n0];
        p1 = fv_mesh->node[n1];
        p2 = fv_mesh->node[n2];

        //cgrad = tetrahedral_gradient(p0,p1,p2,p3,
        //        f[n0],f[n1],f[n2],f[n3]);
        cgrad = triangle_gradient(p0,p1,p2,
                       f[n0],f[n1],f[n2]);//function doesn't exist yet

        /* Inverse gradient */
        if (invdistwgt)
        {
          cg = (p0+p1+p2)/3.0;
          ds = 1.0/distance(p0,cg);
          grad[n0] += cgrad*ds;
          wgt[n0] += ds;
          ds = 1.0/distance(p1,cg);
          grad[n1] += cgrad*ds;
          wgt[n1] += ds;
          ds = 1.0/distance(p2,cg);
          grad[n2] += cgrad*ds;
          wgt[n2] += ds;
        } else
        /* Area (Volume) */
        {
          //double vol = tetrahedral_volume(p0,p1,p2,p3);
          double vol = triangle_area(p0,p1,p2);
          cgrad *= vol;
          grad[n0] += cgrad;
          wgt[n0] += vol;
          grad[n1] += cgrad;
          wgt[n1] += vol;
          grad[n2] += cgrad;
          wgt[n2] += vol;
        }
      }

      /* Getting grads for quadrilaterals */
      for (c=0; c < fv_mesh->nquad; c++)
      {
        n0 = fv_mesh->quad_n[c][0];
        n1 = fv_mesh->quad_n[c][1];
        n2 = fv_mesh->quad_n[c][2];
        n3 = fv_mesh->quad_n[c][3];
        p0 = fv_mesh->node[n0];
        p1 = fv_mesh->node[n1];
        p2 = fv_mesh->node[n2];
        p3 = fv_mesh->node[n3];

        cgrad = quad_gradient(p0,p1,p2,p3,
                f[n0],f[n1],f[n2],f[n3]);//function doesn't exist yet

        if (invdistwgt)
        {
          cg = (p0+p1+p2+p3)/4.0;
          ds = 1.0/distance(p0,cg);
          grad[n0] += cgrad*ds;
          wgt[n0] += ds;
          ds = 1.0/distance(p1,cg);
          grad[n1] += cgrad*ds;
          wgt[n1] += ds;
          ds = 1.0/distance(p2,cg);
          grad[n2] += cgrad*ds;
          wgt[n2] += ds;
          ds = 1.0/distance(p3,cg);
          grad[n3] += cgrad*ds;
          wgt[n3] += ds;
        } else
          /* Area (Volume) */
        {
          //double vol = pyramid_volume(p0,p1,p2,p3,p4);
          double vol = quad_area(p0,p1,p2,p3);
          cgrad *= vol;
          grad[n0] += cgrad;
          wgt[n0] += vol;
          grad[n1] += cgrad;
          wgt[n1] += vol;
          grad[n2] += cgrad;
          wgt[n2] += vol;
          grad[n3] += cgrad;
          wgt[n3] += vol;
        }
      }


      /* Dividing by weighting */
      for (n=0; n < fv_mesh->nn; n++)
        grad[n] /= wgt[n];

      /* Calculating dfmin, dfmax, and dfmean */
      k=0;
      // Go through all nodes
      for (n=0; n < fv_mesh->nn; n++)
      {
        j=0;// set j to zero
        // if number of refinement boxes greater than zero
        if (nrbox > 0)
        {
          // as long as there are refinement boxes and j is not zero
          for (i=0; i < nrbox && !j; i++)
            /* if fv_mesh is inside rbox */ // MAKING 2D FRIENDLY
            if (fv_mesh->node[n][0] >= rbox[i].lo[0] && fv_mesh->node[n][0] <= rbox[i].hi[0] &&
                fv_mesh->node[n][1] >= rbox[i].lo[1] && fv_mesh->node[n][1] <= rbox[i].hi[1]) j=1;
        } else
          j=1;

        // if j is zero or node is tagged( as boundary node )
        if (!j || !tag[n])
          continue;

        // getting k itself
        hd = nhash[n]->head;// get first node
        while (hd)
        {
          m = hd->data;// get other node
          v1 = Vector(fv_mesh->node[n],fv_mesh->node[m]);// make vector out of two nodes
          d1 = v1.magnitude();// get magnitude of vector
          v1.normalize();// normalize vector
          ff = fabs(grad[n] * v1)*pow(d1,power);//|grad(n) * e| * d1^(power) -> tensor?
          dfmin = MIN(dfmin,ff);
          dfmax = MAX(dfmax,ff);
          dfmean += ff;
          k++;
          hd = hd->next;
        }
      }
      /* Getting dfmean */
      dfmean /= MAX(1,k);

      /* Getting dfsd */
      dfsd = 0.0;
      for (n=0; n < fv_mesh->nn; n++)
      {
        j=0;
        if (nrbox > 0)
        {
          for (i=0; i < nrbox && !j; i++)
            /* if fv_mesh is inside rbox */ // MAKING 2D FRIENDLY
            if (fv_mesh->node[n][0] >= rbox[i].lo[0] && fv_mesh->node[n][0] <= rbox[i].hi[0] &&
                fv_mesh->node[n][1] >= rbox[i].lo[1] && fv_mesh->node[n][1] <= rbox[i].hi[1]) j=1;
        } else
          j=1;
        if (!j || !tag[n])
          continue;

        hd = nhash[n]->head;
        while (hd)
        {
          m = hd->data;
          v1 = Vector(fv_mesh->node[n],fv_mesh->node[m]);
          d1 = v1.magnitude();
          v1.normalize();
          ff = fabs(grad[n] * v1)*pow(d1,power) - dfmean;
          dfsd += ff*ff;
          hd = hd->next;
        }
      }
      dfsd = sqrt(dfsd/MAX(1,k));

      /* Printing out statistics of adaptation number */
      fprintf(out_f,"\nStatistics for adaption function %d - %s:",naf,func);
      fprintf(out_f,"\n  # of edges= %d",k);
      fprintf(out_f,"\n  Min       = %g",dfmin);
      fprintf(out_f,"\n  Mean      = %g",dfmean);
      fprintf(out_f,"\n  Max       = %g",dfmax);
      fprintf(out_f,"\n  STD       = %g\n",dfsd);

      /* Getting threshold (limit) */
      journal(in_f, out_f, jou_f, "#Enter threshold (+ve) or # of STD above mean (-ve) >", std);
      double limit;
      if (std > 1.0e-12)
        limit = std;
      else
        limit = dfmean + dfsd*fabs(std);
      fprintf(out_f,"\n  Threshold = %g\n",limit);

      if (limit > dfmax)
        continue;

      for (n=0; n < fv_mesh->nn; n++) {

        j=0;
        if (nrbox > 0)
        {
          /* if fv_mesh is inside rbox */
          for (i=0; i < nrbox && !j; i++)
            if (fv_mesh->node[n][0] >= rbox[i].lo[0] && fv_mesh->node[n][0] <= rbox[i].hi[0] &&
                fv_mesh->node[n][1] >= rbox[i].lo[1] && fv_mesh->node[n][1] <= rbox[i].hi[1]) j=1;
        } else
          j=1;
        if (!j || !tag[n])
          continue;

        // CHANGE THIS PART AFTER REVIEWING RIEMMANIAN TENSORS
        // compute orthogonal vectors using gradient as prime
        /* v1: gradient of analytical function, first direction
         * ff: magnitudes of vectors
         * hs: directions
         * h#: desired spacing
         * The other vectors get computed as orthogonals to v1
         * rmt: Riemmanian tensor! */
        v1 = grad[n];
        if (v1.magnitude() < 1.0e-10)
          v1 = Vector(1.0,0.0,0.0);
        v1.normalize();
        v3 = Vector(1.0,1.0,1.0);
        if (fabs(v1*v3) < 1.0e-10)
          v3 = Vector(0.0,1.0,0.0);
        v2 = v1 % v3;
        v2.normalize();
        v3 = v1 % v2;
        v3.normalize();
        ff = fabs(v1*grad[n]);
        ds = pow( MAX( 0.0, limit ) / MAX( 1.0e-10, ff ), 1.0 / power );
        h1 = MIN(dsglobal,ds);
        ff = fabs(v2*grad[n]);
        ds = pow( MAX( 0.0, limit ) / MAX( 1.0e-10, ff ), 1.0 / power );
        h2 = MIN(dsglobal,ds);
        ff = fabs(v3*grad[n]);
        ds = pow( MAX( 0.0, limit ) / MAX( 1.0e-10, ff ), 1.0 / power );
        h3 = MIN(dsglobal,ds);

        if ( fabs( h1 - h2 ) < 1.0e-12 &&
             fabs( h1 - h3 ) < 1.0e-12 &&
             fabs( h2 - h3 ) < 1.0e-12 )
        {
          // store as uniform tensor
          rmt1[0][0] = 1.0/h1/h1;
          rmt1[0][1] = 0.0;
          rmt1[0][2] = 0.0;
          rmt1[1][0] = 0.0;
          rmt1[1][1] = 1.0/h2/h2;
          rmt1[1][2] = 0.0;
          rmt1[2][0] = 0.0;
          rmt1[2][1] = 0.0;
          rmt1[2][2] = 1.0/h3/h3;
        } else
        {
          e1[0]=v1[0]; e1[1]=v1[1]; e1[2]=v1[2];
          e2[0]=v2[0]; e2[1]=v2[1]; e2[2]=v2[2];
          e3[0]=v3[0]; e3[1]=v3[1]; e3[2]=v3[2];
          a_t->Compute_Riemannian_Metric(e1,e2,e3,h1,h2,h3,rmt1);
        }

        // create full tensor of existing tensor components
        if (fabs(rmt[n][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n][0],rmt2);
        else
          a_t->Full_Tensor(rmt[n],rmt2); 

        if (a_t->Compare_Tensors(rmt1,rmt2) > 0.05)
        {
          a_t->Combine_Tensors(rmt1,rmt2,rmt3);
          a_t->Decompose_Tensor(rmt3,left,right,lam);//left,right,lam?
          h1 = sqrt(1.0/lam[0]);
          h2 = sqrt(1.0/lam[1]);
          h3 = sqrt(1.0/lam[2]);
          if ( fabs( h1 - h2 ) < 1.0e-12 &&
               fabs( h1 - h3 ) < 1.0e-12 &&
               fabs( h2 - h3 ) < 1.0e-12 )
          {
            // store as a scalar
            rmt[n][0] = h1;
            rmt[n][1] = 0.0;
            rmt[n][2] = 0.0;
            rmt[n][3] = 0.0;
            rmt[n][4] = 0.0;
            rmt[n][5] = 0.0;
          } else
          {
            // store 6 components of symmetric tensor
            rmt[n][0]=rmt3[0][0];
            rmt[n][1]=rmt3[0][1];
            rmt[n][2]=rmt3[0][2];
            rmt[n][3]=rmt3[1][1];
            rmt[n][4]=rmt3[1][2];
            rmt[n][5]=rmt3[2][2];
          }
        }
      }
    }
    delete[] f;
    delete[] wgt;
    delete[] grad;
    delete[] tag;

    int mode;
    journal(in_f, out_f, jou_f, "#Store scalars, tensors or mixed? [0 1 2] >",mode);

    int e_based;
    journal(in_f, out_f, jou_f, "#Store at nodes [0] or elements? [1] >",e_based);

    // CHANGING CODE AGAIN
    i=j=0;
    /* Store at elements */
    if (e_based)
    {
      /* Working on triangles */
      for (c=0; c < fv_mesh->ntri; c++)
      {
        n0 = fv_mesh->tri_n[c][0];
        n1 = fv_mesh->tri_n[c][1];
        n2 = fv_mesh->tri_n[c][2];
        vert[0][0] = fv_mesh->node[n0][0];
        vert[0][1] = fv_mesh->node[n0][1];
        vert[1][0] = fv_mesh->node[n1][0];
        vert[1][1] = fv_mesh->node[n1][1];
        vert[2][0] = fv_mesh->node[n2][0];
        vert[2][1] = fv_mesh->node[n2][1];

        // NOT TOUCHING THIS PART YET
        // combine tensors from all nodes
        if (fabs(rmt[n0][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n0][0],rmt1);
        else
          a_t->Full_Tensor(rmt[n0],rmt1); 

        if (fabs(rmt[n1][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n1][0],rmt2);
        else
          a_t->Full_Tensor(rmt[n1],rmt2); 

        a_t->Combine_Tensors(rmt1,rmt2,rmt3);

        if (fabs(rmt[n2][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n2][0],rmt1);
        else
          a_t->Full_Tensor(rmt[n2],rmt1); 

        a_t->Combine_Tensors(rmt1,rmt3,rmt2);

        if (fabs(rmt[n3][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n3][0],rmt1);
        else
          a_t->Full_Tensor(rmt[n3],rmt1); 

        a_t->Combine_Tensors(rmt1,rmt2,rmt3);

        a_t->Decompose_Tensor(rmt3,left,right,lam);
        h1 = sqrt(1.0/lam[0]);
        h2 = sqrt(1.0/lam[1]);
        h3 = sqrt(1.0/lam[2]);
        if (h1 < dsglobal || h2 < dsglobal || h3 < dsglobal)
        {
          if (mode == 0 || (mode == 2 && MAX(h1,MAX(h2,h3))/MIN(h1,MIN(h2,h3)) > 1.5))
          {
            ds = MIN(h1,MIN(h2,h3));
            if (ds < dsglobal)
            a_t->Store_Scalar(ds,4,vert);
            i++;
          } else
          {
            a_t->Store_Tensor(rmt3,4,vert);
            j++;
          }
        }

      }

      /* Working on quadrilaterals */
      for (c=0; c < fv_mesh->nquad; c++)
      {
        n0 = fv_mesh->quad_n[c][0];
        n1 = fv_mesh->quad_n[c][1];
        n2 = fv_mesh->quad_n[c][2];
        n3 = fv_mesh->quad_n[c][3];
        vert[0][0] = fv_mesh->node[n0][0];
        vert[0][1] = fv_mesh->node[n0][1];
        vert[1][0] = fv_mesh->node[n1][0];
        vert[1][1] = fv_mesh->node[n1][1];
        vert[2][0] = fv_mesh->node[n2][0];
        vert[2][1] = fv_mesh->node[n2][1];
        vert[3][0] = fv_mesh->node[n3][0];
        vert[3][1] = fv_mesh->node[n3][1];

        // NOT TOUCHING THIS PART YET
        // combine tensors from all nodes
        if (fabs(rmt[n0][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n0][0],rmt1);
        else
          a_t->Full_Tensor(rmt[n0],rmt1); 
        if (fabs(rmt[n1][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n1][0],rmt2);
        else
          a_t->Full_Tensor(rmt[n1],rmt2); 
        a_t->Combine_Tensors(rmt1,rmt2,rmt3);
        if (fabs(rmt[n2][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n2][0],rmt1);
        else
          a_t->Full_Tensor(rmt[n2],rmt1); 
        a_t->Combine_Tensors(rmt1,rmt3,rmt2);
        if (fabs(rmt[n3][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n3][0],rmt1);
        else
          a_t->Full_Tensor(rmt[n3],rmt1); 
        a_t->Combine_Tensors(rmt1,rmt2,rmt3);
        if (fabs(rmt[n4][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n4][0],rmt1);
        else
          a_t->Full_Tensor(rmt[n4],rmt1); 
        a_t->Combine_Tensors(rmt1,rmt3,rmt2);

        a_t->Decompose_Tensor(rmt2,left,right,lam);
        h1 = sqrt(1.0/lam[0]);
        h2 = sqrt(1.0/lam[1]);
        h3 = sqrt(1.0/lam[2]);
        if (h1 < dsglobal || h2 < dsglobal || h3 < dsglobal)
        {
          if (mode == 0 || (mode == 2 && MAX(h1,MAX(h2,h3))/MIN(h1,MIN(h2,h3)) < 1.5))
          {
            ds = MIN(h1,MIN(h2,h3));
            if (ds < dsglobal)
            a_t->Store_Scalar(ds,5,vert);
            i++;
          } else
          {
            a_t->Store_Tensor(rmt3,5,vert);
            j++;
          }
        }
      }

    }

    // BACK TO CHANGING
    /* Store at nodes */
    else
    {
      for (n=0; n < fv_mesh->nn; n++)
      {
        dx = 0.0;
        dy = 0.0;
        //dz = 0.0;
        ds = 0.0;
        hd = nhash[n]->head;
        k=0;
        while (hd)
        {
          m = hd->data;
          dx += fabs(fv_mesh->node[n][0]-fv_mesh->node[m][0]);
          dy += fabs(fv_mesh->node[n][1]-fv_mesh->node[m][1]);
          //dz += fabs(fv_mesh->node[n][2]-fv_mesh->node[m][2]);
          hd = hd->next;
          k++;
        }
        dx /= k;
        dy /= k;
        //dz /= k;
        dx *= 0.5;
        dy *= 0.5;
        //dz *= 0.5;

        lo[0] = fv_mesh->node[n][0] - dx;
        lo[1] = fv_mesh->node[n][1] - dy;
        //lo[2] = fv_mesh->node[n][2] - dz;
        hi[0] = fv_mesh->node[n][0] + dx;
        hi[1] = fv_mesh->node[n][1] + dy;
        //hi[2] = fv_mesh->node[n][2] + dz;

        // NOT CHANGING THE REST OF THIS CHUNK YET TIL KNOWING MORE ABOUT
        // TENSORS
        if (fabs(rmt[n][1]) < 1.0e-12)
          a_t->Scalar_To_Tensor(rmt[n][0],rmt2);
        else
          a_t->Full_Tensor(rmt[n],rmt2); 

        a_t->Decompose_Tensor(rmt2,left,right,lam);
        h1 = sqrt(1.0/lam[0]);
        h2 = sqrt(1.0/lam[1]);
        h3 = sqrt(1.0/lam[2]);

        if (mode == 1 || (mode == 2 && MAX(h1,MAX(h2,h3))/MIN(h1,MIN(h2,h3)) > 1.5))
        {
          if (h1 > dsglobal && h2 > dsglobal && h3 > dsglobal)
            continue;
          if (fabs(rmt[n][1]) < 1.0e-12)
            a_t->Scalar_To_Tensor(rmt[n][0],rmt2);
          else
            a_t->Full_Tensor(rmt[n],rmt2); 
          a_t->Store_Tensor(rmt2,lo,hi);
          j++;
        } else
        {
          ds = MIN(h1,MIN(h2,h3));
          if (ds > dsglobal)
            continue;
          a_t->Store_Scalar(ds,lo,hi);
          i++;
        }
      }
    }
    fprintf(out_f,"\nNodes have been processed, # of entries = %d",a_t->Number_of_Entries());
    fprintf(out_f,"\n   # of scalars stored = %d",i);
    fprintf(out_f,"\n   # of tensors stored = %d",j);
    fflush(out_f);

    // delete node to node hash table
    for (n=0; n < fv_mesh->nn; n++)
      delete nhash[n];
    delete nhash;

    // NOT CHANGING PLOT FILE WRITING YET, IT'S GOT 3D STUFF
    // write plot file with spacing parameters.
    // replace solution data with spacing data
    for (i=0; i < fv_mesh->nvars; i++)
      free(fv_mesh->Q_name[i]);
    free(fv_mesh->Q_name);
    fv_mesh->nvars = 3;
    fv_mesh->Q_name = (char**)malloc(fv_mesh->nvars*sizeof(char*));
    for (i=0; i < fv_mesh->nvars; i++)
    {
      fv_mesh->Q_name[i] = (char*)malloc(80*sizeof(char));
      switch (i)
      {
        case 0:
          sprintf(fv_mesh->Q_name[i],"X_Spacing");
          break;
        case 1:
          sprintf(fv_mesh->Q_name[i],"Y_Spacing");
          break;
        case 2:
          sprintf(fv_mesh->Q_name[i],"Z_Spacing");
          break;
      }
    }
    e1[0] = 1.0; e1[1] = 0.0; e1[2] = 0.0;
    e2[0] = 0.0; e2[1] = 1.0; e2[2] = 0.0;
    e3[0] = 0.0; e3[1] = 0.0; e3[2] = 1.0;
    for (n=0; n < fv_mesh->nn; n++)
    {
      fv_mesh->Q[n] = (double*)realloc((void*)fv_mesh->Q[n],fv_mesh->nvars*sizeof(double));;
      if (fabs(rmt[n][1]) < 1.0e-12)
        a_t->Scalar_To_Tensor(rmt[n][0],rmt2);
      else
        a_t->Full_Tensor(rmt[n],rmt2); 
      fv_mesh->Q[n][0] = 1.0/a_t->Metric_Length(e1,rmt2);
      fv_mesh->Q[n][1] = 1.0/a_t->Metric_Length(e2,rmt2);
      fv_mesh->Q[n][2] = 1.0/a_t->Metric_Length(e3,rmt2);
    }

    sprintf(fname,"%s.fv",sname);
    Write_Plotfile(fname,fv_mesh,0);

    delete[] rmt;

    if (nrbox > 0)
      free(rbox);


    delete fv_mesh;
  }

  do
  {
    journal(in_f, out_f, jou_f, "#Enter an auxilliary scalar/tensor box? [0 1] >",n);
    if (n)
    {
      journal(in_f, out_f, jou_f, "#Enter lower X coordinate >",lo[0]);
      journal(in_f, out_f, jou_f, "#Enter lower Y coordinate >",lo[1]);
      journal(in_f, out_f, jou_f, "#Enter lower Z coordinate >",lo[2]);
      journal(in_f, out_f, jou_f, "#Enter upper X coordinate >",hi[0]);
      journal(in_f, out_f, jou_f, "#Enter upper Y coordinate >",hi[1]);
      journal(in_f, out_f, jou_f, "#Enter upper Z coordinate >",hi[2]);
      journal(in_f, out_f, jou_f, "#Enter scalar [1] or tensor [2] >",i);

      if (i==1)
      {
        journal(in_f, out_f, jou_f, "#Enter scalar spacing size >",ds);
        a_t->Store_Scalar(ds,lo,hi);
      } else
      {
        journal(in_f, out_f, jou_f, "#Enter X spacing size >",dx);
        journal(in_f, out_f, jou_f, "#Enter Y spacing size >",dy);
        journal(in_f, out_f, jou_f, "#Enter Z spacing size >",dz);
        vec1[0] = 1.0;
        vec1[1] = 0.0;
        vec1[2] = 0.0;
        vec2[0] = 0.0;
        vec2[1] = 1.0;
        vec2[2] = 0.0;
        vec3[0] = 0.0;
        vec3[1] = 0.0;
        vec3[2] = 1.0;

        // recompute current tensor with modified eigenvalues
        a_t->Compute_Riemannian_Metric(vec1, vec2, vec3, dx, dy, dz, rmt1);
        a_t->Store_Tensor(rmt1,lo,hi);
      }
    }
  } while (n);

  // write adaptive tensor file
  a_t->Export_Spacing(sname,out_f);

  fprintf(out_f,"\nTensor file written.\n");
  fflush(out_f);

  //a_t->Populate_Octree();

  //do
  //{
  //  journal(in_f, out_f, jou_f, "#Optimize spacing field? [0 1 2] >",i);
  //  if (i > 0 && i < 3)
  //  {
  //    // Optimize Spacing Field
  //    a_t->Optimize_Field(i);
  //    // re-write adaptive tensor file
  //    a_t->Export_Spacing(sname,out_f);
  //  }
  //} while (i > 0 && i < 3);

  delete a_t;

  fclose(jou_f);

  return 0;

/***************** HAVE GOTTEN TO THIS POINT ***********************/

}


void Qmesh_obj::2Dvorticity_helicity(double f[], int mode) {

  int c, i, n, n0, n1, n2;
  Point p0, p1, p2, cg;
  double *wgt;
  double id0, id1, id2;
  double u0, u1, u2;
  double v0, v1, v2;
  double w0, w1, w2;
  Vector *ugrad, *vgrad;
  Vector cgrad;
  bool momentum;
  int ui, vi, di;

  momentum = false;
  di = -1;
  /* 4 total variables(?) */
  for( i=0; i < 4 && !momentum; i++ ) {

    /* strstr(?) */
    if( strstr( Q_name[i],"Density" ) != NULL ) di = i;//density index
    if( strstr( Q_name[i],"Momentum" ) != NULL )
      momentum = true;
  }
  if( momentum && di < 0 ) {

    fprintf( stderr,"\nVORTICITY: Unable to determine how to compute velocity!\n" );
    fflush( stderr );
    abort();
  }
  ui = vi = -1;
  for( i = 0 ; i < 4; i++ ) {

    if (strstr(Q_name[i],"X_Momentum") != NULL ||
        strstr(Q_name[i],"X_Velocity") != NULL ||
        strstr(Q_name[i],"U-velocity") != NULL)
      ui = i;
    if (strstr(Q_name[i],"Y_Momentum") != NULL ||
        strstr(Q_name[i],"Y_Velocity") != NULL ||
        strstr(Q_name[i],"V-velocity") != NULL)
      vi = i;
  }

  ugrad = new Vector[nn];
  vgrad = new Vector[nn];
  wgt= new double[nn];

  for (n=0; n < nn; n++) {

    /* Vector has only two components */
    ugrad[n] = vgrad[n] = Vector(0.0,0.0);
    wgt[n]=0.0;
  }

  for( c = 0; c < ntri; c++ ) {

    n0 = tri_n[c][0];
    n1 = tri_n[c][1];
    n2 = tri_n[c][2];
    p0 = node[n0];
    p1 = node[n1];
    p2 = node[n2];
    cg = ( p0 + p1 + p2 ) / 3.0;
    id0 = 1.0 / distance(p0,cg);
    id1 = 1.0 / distance(p1,cg);
    id2 = 1.0 / distance(p2,cg);
   
    if( momentum ) {

      u0 = Q[n0][ui]/Q[n0][di];
      v0 = Q[n0][vi]/Q[n0][di];
      u1 = Q[n1][ui]/Q[n1][di];
      v1 = Q[n1][vi]/Q[n1][di];
      u2 = Q[n2][ui]/Q[n2][di];
      v2 = Q[n2][vi]/Q[n2][di];

    } 
    else {

      u0 = Q[n0][ui];
      v0 = Q[n0][vi];
      u1 = Q[n1][ui];
      v1 = Q[n1][vi];
      u2 = Q[n2][ui];
      v2 = Q[n2][vi];
    }

    /* u gradients */
    cgrad = triangle_gradient( p0, p1, p2, u0, u1, u2 );
    ugrad[n0] += cgrad*id0;
    wgt[n0] += id0;
    ugrad[n1] += cgrad*id1;
    wgt[n1] += id1;
    ugrad[n2] += cgrad*id2;
    wgt[n2] += id2;

    /* v gradients */
    cgrad = triangle_gradient( p0, p1, p2, v0, v1, v2 );
    vgrad[n0] += cgrad*id0;
    wgt[n0] += id0;
    vgrad[n1] += cgrad*id1;
    wgt[n1] += id1;
    vgrad[n2] += cgrad*id2;
    wgt[n2] += id2;

  }

  double vx, vy;
  for( n = 0; n < nn; n++ ) {

    ugrad[n] /= wgt[n];
    vgrad[n] /= wgt[n];
    vx = wgrad[n][1]-vgrad[n][2];
    vy = ugrad[n][2]-wgrad[n][0];
    /* GOT A LITTLE DOUBT HERE */
    vz = vgrad[n][0]-ugrad[n][1];
    switch (mode)
    {
      case 0:
        f[n] = sqrt(vx*vx+vy*vy);
        break;
      case 1:
        f[n] = vx;
        break;
      case 2:
        f[n] = vy;
        break;
      case 3:
        if (momentum)
          f[n] = (Q[n][ui]*vx + Q[n][vi]*vy)/Q[n][di];
        else
          f[n] = (Q[n][ui]*vx + Q[n][vi]*vy);
        break;
    }
  }

  delete[] ugrad;
  delete[] vgrad;
  delete[] wgt;
}

void Read_Plotfile(char filename[], Qmesh_obj *mesh, int mode)
{
  mode = -1;
  Plotfile( filename,
            mode,
            mesh->nn,
            &(mesh->node),
            mesh->nb,
            &(mesh->b_name),
            mesh->nvars,
            &(mesh->Q_name),
            &(mesh->Q),
            &(mesh->nt),
            &(mesh->t_n),
            &(mesh->nq),
            &(mesh->q_n),
            mesh->ntet,
            &(mesh->tet_n),
            mesh->npyr,
            &(mesh->pyr_n),
            mesh->npri,
            &(mesh->pri_n),
            mesh->nhex,
            &(mesh->hex_n) );

}

void Write_Plotfile(char filename[], Qmesh_obj *mesh, int mode)
{
  mode = 1;
  Plotfile( filename,
            mode,
            mesh->nn,
            &(mesh->node),
            mesh->nb,
            &(mesh->b_name),
            mesh->nvars,
            &(mesh->Q_name),
            &(mesh->Q),
            &(mesh->nt),
            &(mesh->t_n),
            &(mesh->nq),
            &(mesh->q_n),
            mesh->ntet,
            &(mesh->tet_n),
            mesh->npyr,
            &(mesh->pyr_n),
            mesh->npri,
            &(mesh->pri_n),
            mesh->nhex,
            &(mesh->hex_n) );

}

