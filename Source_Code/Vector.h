#include <stdio.h>
#include <math.h>
#include "Space.h"
#include "Point.h"

#ifndef Vector_h
#define Vector_h
class Vector
{
 double vec[SPACE];

 public:
  /* Constructors 2D and 3D vectors, takes magnitudes */
  #if SPACE == 2
  inline Vector(double dx=0.0, double dy=0.0)
  { vec[0] = dx; vec[1] = dy; }
  #else
  inline Vector(double dx=0.0, double dy=0.0, double dz=0.0)
  { vec[0] = dx; vec[1] = dy; vec[2] = dz; }
  #endif

  /* Constructor, takes point objects. Defined later */
  inline Vector(const Point &from, const Point &to);

  /* Print to file functions */
  #if SPACE == 2
  void print(FILE *outf)
  { fprintf(outf,"\nVector (x,y)= (%.12g,%.12g)",vec[0],vec[1]); }
  #else
  void print(FILE *outf)
  { fprintf(outf,"\nVector (x,y,z)= (%.12g,%.12g,%.12g)",vec[0],vec[1],vec[2]); }
  #endif

  /* Defined later */
  inline Vector operator + ( const Vector &v);
  inline Vector operator - ( const Vector &v);
  inline Vector &operator += ( const Vector &v);
  inline Vector &operator -= ( const Vector &v);
  inline Vector &operator += ( const double &s);
  inline Vector &operator -= ( const double &s);
  inline Vector &operator *= ( const double &s);
  inline Vector &operator /= ( const double &s);

  /* Dot product vector-vector */
  #if SPACE == 2
  inline double operator * ( const Vector &v)
  { return double(vec[0]*v.vec[0]+vec[1]*v.vec[1]); }
  #else
  inline double operator * ( const Vector &v)
  { return double(vec[0]*v.vec[0]+vec[1]*v.vec[1]+vec[2]*v.vec[2]); }
  #endif

  /* Mod operator, defined later */
  #if SPACE == 2
  inline double operator % ( const Vector &v);
  #else
  inline Vector operator % ( const Vector &v);
  #endif

  /* Defined later */
  inline Vector operator * ( const double &s);
  inline Vector operator / ( const double &s);
  inline double operator () (int i) const;//Returns double
  inline double &operator () (int i);//FORTRAN MODE. Returns reference to double, used at left of =
  inline double &operator [] (int i);//Returns reference to double, used at left of =
  inline double magnitude();
  inline void normalize();
  ~Vector(){};

};

inline Vector::Vector(const Point &from, const Point &to)
{
  for (int i = 0; i < SPACE; i++)
          {
            vec[i] = to(i) - from(i);
          }
}

inline double &Vector::operator () (int i)//i = 0, 1, 2
{ return vec[i]; }

inline double Vector::operator () (int i) const
{ return vec[i]; }

inline double &Vector::operator [] (int i)
{ return vec[i]; }

/* Vector cross product */
#if SPACE == 2
inline double Vector::operator % ( const Vector &v)
{
  return (vec[0]*v.vec[1]-vec[1]*v.vec[0]);
}
#else
inline Vector Vector::operator % ( const Vector &v)
{
  return Vector(vec[1]*v.vec[2]-vec[2]*v.vec[1],
                vec[2]*v.vec[0]-vec[0]*v.vec[2],
                vec[0]*v.vec[1]-vec[1]*v.vec[0]);
}
#endif

#if SPACE == 2
inline Vector Vector::operator +( const Vector &v)
{ return Vector(vec[0]+v.vec[0], vec[1]+v.vec[1]); }
inline Vector Vector::operator -( const Vector &v)
{ return Vector(vec[0]-v.vec[0], vec[1]-v.vec[1]); }
inline Vector Vector::operator *( const double &s)
{ return Vector(s*vec[0], s*vec[1]); }
inline Vector Vector::operator /( const double &s)
{ return Vector(vec[0]/s, vec[1]/s); }
inline double Vector::magnitude()
{ return double(sqrt(vec[0]*vec[0] + vec[1]*vec[1])); }
#else
inline Vector Vector::operator +( const Vector &v)
{ return Vector(vec[0]+v.vec[0], vec[1]+v.vec[1], vec[2]+v.vec[2]); }
inline Vector Vector::operator -( const Vector &v)
{ return Vector(vec[0]-v.vec[0], vec[1]-v.vec[1], vec[2]-v.vec[2]); }
inline Vector Vector::operator *( const double &s)
{ return Vector(s*vec[0], s*vec[1], s*vec[2]); }
inline Vector Vector::operator /( const double &s)
{ return Vector(vec[0]/s, vec[1]/s, vec[2]/s); }
inline double Vector::magnitude()
{ return double(sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2])); }
#endif

inline Vector &Vector::operator += (const Vector &v)
{
  for (int i = 0; i < SPACE; i++)
          {
            vec[i]+=v.vec[i];
          }
  return *this;
}

inline Vector &Vector::operator -= (const Vector &v)
{
  for (int i = 0; i < SPACE; i++)
          {
            vec[i]-=v.vec[i];
          }
  return *this;
}

inline Vector &Vector::operator += (const double &s)
{
  for (int i = 0; i < SPACE; i++)
          {
            vec[i]+=s;
          }
  return *this;
}

inline Vector &Vector::operator -= (const double &s)
{
  for (int i = 0; i < SPACE; i++)
          {
            vec[i]-=s;
          }
  return *this;
}

inline Vector &Vector::operator *= (const double &s)
{
  for (int i = 0; i < SPACE; i++)
          {
            vec[i]*=s;
          }
  return *this;
}

inline Vector &Vector::operator /= (const double &s)
{
  for (int i = 0; i < SPACE; i++)
          {
            vec[i]/=s;
          }
  return *this;
}

// we do not need to worry if mag less than machine eps
// if so, vector is 0 anyways, and if we divide nearly 0 by zero,
// we get a huge number and not a normalized vector like we want 
#if SPACE == 2
inline void Vector::normalize()
{
  double mag = sqrt(vec[0]*vec[0]+vec[1]*vec[1]);
  if (mag > 1.0e-20) { vec[0] = vec[0] / mag; vec[1] = vec[1] / mag; }
}
#else
inline void Vector::normalize()
{
  double mag = sqrt(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]);
  if (mag > 1.0e-20) { vec[0] = vec[0] / mag; vec[1] = vec[1] / mag; vec[2] = vec[2] / mag; }
}
#endif

#if SPACE == 3
inline double scalar_triple_product(Vector a, Vector b, Vector c)
{
  double d;
  d = a[0]*b[1]*c[2]+a[1]*b[2]*c[0]+a[2]*b[0]*c[1]-
      a[2]*b[1]*c[0]-a[1]*b[0]*c[2]-a[0]*b[2]*c[1];
  return(d);
}
#endif

#endif
