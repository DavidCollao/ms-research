#include <stdio.h>
#include <stdlib.h>
#include "Linked_List.h"

#ifndef amesh_obj_h
#define amesh_obj_h

/* This class is used only to store meshes */
class amesh_obj
{
 public:
   /* Declaration of stuff */
  int nb, nn, ntet, npyr, npri, nhex;
  Point *node;
  int *nt, *nq;
  int ***t_n;
  int ***q_n;
  int **tet_n;
  int **pyr_n;
  int **pri_n;
  int **hex_n;

  /* Constructor */
  amesh_obj() { 
    nb=nn=ntet=npyr=npri=nhex=0; node=0;
    tet_n=0; pyr_n=0; pri_n=0; hex_n=0; nt=0; nq=0;
    t_n=0; q_n=0;
  }

  /* Destructor */
  ~amesh_obj() {
    int i;

    /* Get rid of pointers to triangles and quads */
    if (nb > 0) {
      for (i=0; i < nb; i++) {
        if (nt[i] != 0)
          free(t_n[i]);
        if (nq[i] != 0)
          free(q_n[i]);
      }
      free(nt);
      free(t_n);
      free(nq);
      free(q_n);
    }
    /* Get rid of tets */
    if (tet_n != 0)
    {
      for (i=0; i < ntet; i++)
        free(tet_n[i]);
      free(tet_n);
    }
    /* Get rid of pyramids */
    if (pyr_n != 0)
    {
      for (i=0; i < npyr; i++)
        free(pyr_n[i]);
      free(pyr_n);
    }
    /* Get rid of prizms */
    if (pri_n != 0)
    {
      for (i=0; i < npri; i++)
        free(pri_n[i]);
      free(pri_n);
    }
    /* Get rid of hexes */
    if (hex_n != 0)
    {
      for (i=0; i < nhex; i++)
        free(hex_n[i]);
      free(hex_n);
    }
    free(node);
    nb=nn=ntet=npyr=npri=nhex=0;
    node=0;
    tet_n=0;
    pyr_n=0;
    pri_n=0;
    hex_n=0;
  }

};
#endif

