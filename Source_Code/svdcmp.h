#include <stdio.h>

#ifndef SVDCMP_h
#define SVDCMP_h

int svdcmp(double **a,
           int m,
           int n,
           double w[],
           double **v );

void svbksb(double **u,
            double w[],
            double **v,
            int m,
            int n,
            double b[],
            double x[] );

#endif
